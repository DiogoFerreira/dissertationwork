import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas import datetime
from numpy import genfromtxt


# Read data file
def read_file_slice_1(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S")).loc['2018-11-26 00:00:00':'2018-12-02 23:00:00']


# Read data file
def read_file_slice_2(filename):
    max_connected_users = pd.read_csv(filename, index_col=[0], header=None, names=['TIME_KEY', 'X']).T.squeeze()
    max_connected_users.index = pd.to_datetime(max_connected_users.index, format='%Y-%m-%d %H:%M:%S.%f')
    max_connected_users = max_connected_users.resample('H').ffill().fillna(0)
    return max_connected_users[-24*7:]


def calculate_number_sessions_above_threshold(series, threshold):
	index = np.where(series>=threshold)[0]
	return np.sum(series[index]-threshold[index]), len(index)


# max_users_network = 2000

total_percentage_above_threshold = []
min_users_per_slice = 10

for max_users_network in range(min_users_per_slice*2, 4500, 100):

	slice_1_filename = "counts.csv"
	slice_2_filename = "kpi_data.csv"

	df1 = read_file_slice_1(slice_1_filename)
	df2 = read_file_slice_2(slice_2_filename)

	predictions_1 = genfromtxt('slice_1_predictions.csv', delimiter=',')
	predictions_2 = genfromtxt('slice_2_predictions.csv', delimiter=',')

	# plt.plot(predictions_1)
	# plt.plot(df1.values)
	# plt.show()

	# plt.plot(predictions_2)
	# plt.plot(df2.values)
	# plt.show()


	threshold = []

	for p1, p2 in zip(predictions_1, predictions_2):
		# Some sessions must always be available for each slice
		if p1 <= min_users_per_slice and p2 <= min_users_per_slice:
			threshold.append(max_users_network//2)
		elif p1 <= min_users_per_slice:
			threshold.append(max_users_network-min_users_per_slice)
		elif p2 <= min_users_per_slice:
			threshold.append(min_users_per_slice)
		else:
			
			# If the network can handle with both slices, divide them equally according to their weight
			if (p1+p2)<=max_users_network:
				thr_2 = round(p2 + (max_users_network-(p1+p2))*(p2/(p1+p2)))
			else:
				thr_2 = round(p2 - ((p1+p2)-max_users_network)*(p2/(p1+p2)))
			
			if thr_2<min_users_per_slice:
				threshold.append(min_users_per_slice)
			elif max_users_network-thr_2<min_users_per_slice:
				threshold.append(max_users_network-min_users_per_slice)
			else:
				threshold.append(thr_2)

	# plt.plot(predictions_1)
	# plt.plot([max_users_network-thr for thr in threshold])
	# plt.show()


	# plt.plot(predictions_2)
	# plt.plot(threshold)
	# plt.show()

	sessions_above_threshold_1, hours_above_threshold_1 = calculate_number_sessions_above_threshold(df1.values, np.array([max_users_network-thr for thr in threshold]).reshape(len(threshold), 1))
	sessions_above_threshold_2, hours_above_threshold_2 = calculate_number_sessions_above_threshold(df2.values, np.array(threshold))

	sessions_lost_1 = sessions_above_threshold_1/(np.sum(df1.values)+np.sum(df2.values))
	sessions_lost_2 = sessions_above_threshold_2/(np.sum(df1.values)+np.sum(df2.values))

	# print(sessions_lost_1)
	# print(sessions_lost_2)
	# print(sessions_lost_1+sessions_lost_2)

	total_percentage_above_threshold.append(sessions_lost_1+sessions_lost_2)



np.savetxt("smart_policies.csv", np.asarray(total_percentage_above_threshold), delimiter=",")