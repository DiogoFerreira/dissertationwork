
import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, LSTM
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return np.asarray([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# make one forecast with a NN,
def forecast_lstm(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag, 1)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]
    

# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# evaluate the model
def make_lstm_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test.values, forecasts))
    try:
        mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    except Exception as e:
        mape = 0.0
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse, mape


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, dashes=[4, 2], label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.ylim(0, 750)
    plt.xlim(1650, 1995)
    plt.subplots_adjust(left=0.04, right=0.99, top=0.97, bottom=0.05)
    plt.show()


# plot the forecasts in the context of the original dataset
def plot_all(series, forecasts1, forecasts2, forecasts3, forecasts4, n_test, title, xlabel, ylabel):
    fig = plt.figure()
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values", marker="o", markersize=8)
    plt.plot(range(len(series)-n_test, len(series)), forecasts1, linestyle=':', label="Predictor #1 values", marker="^", markersize=4)
    plt.plot(range(len(series)-n_test, len(series)), forecasts2, linestyle=':', label="Predictor #2 values", marker="<", markersize=4)
    plt.plot(range(len(series)-n_test, len(series)), forecasts3, linestyle=':', label="Predictor #3 values", marker=">", markersize=4)
    plt.plot(range(len(series)-n_test, len(series)), forecasts4, linestyle=':', label="Predictor ensemble values", marker="v", markersize=8)
    # show the plot
    plt.title(title, fontsize=30)
    plt.xlabel(xlabel, fontsize=30)
    plt.ylabel(ylabel, fontsize=30)
    plt.legend(prop={'size': 25})
    plt.ylim(0, 650)
    plt.xlim(1900, 1924)
    plt.subplots_adjust(left=0.06, right=0.99, top=0.95, bottom=0.09)
    plt.tick_params(labelsize = 20)
    plt.show()
    fig.savefig("test.png")


def plot_test_data(series, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values)
    # show the plot
    plt.title(title, fontsize=30)
    plt.xlabel(xlabel, fontsize=30)
    plt.ylabel(ylabel, fontsize=30)
    plt.ylim(0, 750)
    plt.xlim(1650, 1995)
    plt.subplots_adjust(left=0.06, right=0.99, top=0.95, bottom=0.09)
    plt.tick_params(labelsize = 20)
    #plt.show()


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded




def get_max_values(values):
    return np.array([np.amax(value) for value in values])


def get_min_values(values):
    return np.array([np.amin(value) for value in values])


def get_average_values(values):
    return np.array([np.average(value) for value in values])


def get_std_values(values):
    return np.array([np.std(value) for value in values])


def get_above_mean_values(values):
    return np.array([np.where(value > np.average(value))[0].size for value in values])


def get_below_mean_values(values):
    return np.array([np.where(value < np.average(value))[0].size for value in values])


def get_median_values(values):
    return np.array([np.median(value) for value in values])


def get_quantile_values(values, a):
    return np.array([np.quantile(value, a) for value in values])


def get_sum_values(values):
    return np.array([np.sum(value) for value in values])


def get_range_count_values(values, min, max):
    return np.array([np.sum((value >= min) & (value < max)) for value in values])


def get_index_of_max_values(values):
    return np.array([np.argmax(value) for value in values])


def get_index_of_min_values(values):
    return np.array([np.argmin(value) for value in values])


# Parameters
file = "counts.csv"
differentiation = True
second_differentiation = False

normalize = True

# Lag to differentiate the time series
differentiation_lag = 1

# Lag for second differentiation of the time series
second_differentiation_lag = 1

# Number of points available to predict the next ones
n_lag = 12

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2

# Batch size
batch_size = 1

model_number = 3


df = read_file(file)

# Analysis of traffic and choose_series
series_values = get_series_values(df)
original_series_values = series_values

if differentiation:
    series_values = differentiate(series_values, differentiation_lag)
    if second_differentiation:
        second_series_values = series_values
        series_values = differentiate(series_values, second_differentiation_lag)
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
    else:
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
else:
    original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
    week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
    hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
else:
    scaler = None
    scaled_values = series_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)


# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values

work_day_feature = False
week_day_feature = True
hour_feature = False

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    number_inputs += 24

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1])
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

# design network
input_tensor = Input(shape=(X.shape[1],))
output = Dense(200, activation='relu')(input_tensor)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(y.shape[1])(output)
model = Model(inputs=input_tensor, outputs=output)
model.compile(loss='mean_squared_error', optimizer='adam')

model.load_weights('FFNNmodelWeights2.h5')

forecasts = make_nn_forecasts(model, batch_size, test, number_inputs, n_seq)


if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)


if second_differentiation and differentiation:
    forecasts = invert_difference(forecasts, second_series_values.values[second_differentiation_lag-len(test):], second_differentiation_lag)
    forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)
elif differentiation:
    forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)


forecasts[forecasts<0] = 0

error = evaluate_forecasts(original_test, forecasts, number_inputs, n_seq, True)

#plot_forecasts(original_series_values, forecasts, len(validation), "Feed-Forward NN model prediction", "Sample number", "Number of sessions per hour")

n_lag = 168
series_values = original_series_values
original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)

# transform into supervised learning problem X, y
supervised = series_to_supervised(series_values.values.reshape(len(series_values), 1), n_lag, n_seq)
supervised_values = supervised.values

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)

week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

week_max_value = get_max_values(supervised_values[:, :n_lag])
week_min_value = get_min_values(supervised_values[:, :n_lag])
week_average_value = get_average_values(supervised_values[:, :n_lag])
week_std_value = get_std_values(supervised_values[:, :n_lag])
week_above_mean_value = get_above_mean_values(supervised_values[:, :n_lag])
week_below_mean_value = get_below_mean_values(supervised_values[:, :n_lag])
week_median_value = get_median_values(supervised_values[:, :n_lag])
week_sum_value = get_sum_values(supervised_values[:, :n_lag])
week_quantile_value_25 = get_quantile_values(supervised_values[:, :n_lag], 0.25)
week_quantile_value_50 = get_quantile_values(supervised_values[:, :n_lag], 0.50)
week_quantile_value_75 = get_quantile_values(supervised_values[:, :n_lag], 0.75)
week_range_0_100_value = get_range_count_values(supervised_values[:, :n_lag], 0, 100)
week_range_100_300_value = get_range_count_values(supervised_values[:, :n_lag], 100, 300)
week_range_300_600_value = get_range_count_values(supervised_values[:, :n_lag], 300, 600)
week_range_600_700_value = get_range_count_values(supervised_values[:, :n_lag], 600, 700)
week_range_700_9999_value = get_range_count_values(supervised_values[:, :n_lag], 700, 9999)
week_index_max_values = get_index_of_max_values(supervised_values[:, :n_lag])
week_index_min_values = get_index_of_min_values(supervised_values[:, :n_lag])
last_week_value = supervised_values[:, n_lag-168]


day_max_value = get_max_values(supervised_values[:, n_lag-24:n_lag])
day_min_value = get_min_values(supervised_values[:, n_lag-24:n_lag])
day_average_value = get_average_values(supervised_values[:, n_lag-24:n_lag])
day_std_value = get_std_values(supervised_values[:, n_lag-24:n_lag])
day_above_mean_value = get_above_mean_values(supervised_values[:, n_lag-24:n_lag])
day_below_mean_value = get_below_mean_values(supervised_values[:, n_lag-24:n_lag])
day_median_value = get_median_values(supervised_values[:, n_lag-24:n_lag])
day_sum_value = get_sum_values(supervised_values[:, n_lag-24:n_lag])
day_quantile_value_25 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.25)
day_quantile_value_50 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.50)
day_quantile_value_75 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.75)
day_range_0_100_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 0, 100)
day_range_100_300_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 100, 300)
day_range_300_600_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 300, 600)
day_range_600_700_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 600, 700)
day_range_700_9999_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 700, 9999)
day_index_max_values = get_index_of_max_values(supervised_values[:, n_lag-24:n_lag])
day_index_min_values = get_index_of_min_values(supervised_values[:, n_lag-24:n_lag])
last_day_value = supervised_values[:, n_lag-24]

last_hour_value = supervised_values[:, n_lag-1]

features = np.c_ [last_hour_value, last_day_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
        week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value, day_quantile_value_25, day_quantile_value_50, day_quantile_value_75,
        week_quantile_value_25, week_quantile_value_50, week_quantile_value_75, day_sum_value, week_sum_value, 
        week_index_max_values, week_index_min_values, day_index_min_values, day_index_max_values]

number_inputs = features.shape[1]

if normalize:
    # rescale values
    scaler2 = StandardScaler()
    scaled_features = scaler2.fit_transform(features)
    scaled_labels = scaler2.fit_transform(supervised_values[:, -n_seq:])
else:
    scaler2 = None
    scaled_features = features
    scaled_labels = supervised_values[:, -n_seq:]


train, validation, test = train_validation_test_split(np.c_ [scaled_features, scaled_labels], 0, validation_hours, test_hours)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1])
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

rmse = 1000
while rmse>30:
    rfr = RandomForestRegressor()
    rfr = rfr.fit(X, y)
    forecasts_features = rfr.predict(test_X)
    forecasts_features = forecasts_features.reshape(len(forecasts_features), 1)
    if normalize:
        forecasts_features = invert_scaler(forecasts_features, scaler2)
    else:
        forecasts_features = np.asarray(forecasts_features)

    forecasts_features[forecasts_features<0] = 0
    print("Random Forest")
    rmse, mape = evaluate_forecasts(original_test, forecasts_features, n_lag, n_seq, True)
#plot_forecasts(original_series_values, forecasts_features, len(validation), "Random Forest Regressor model prediction", "Sample number", "Number of sessions per hour")


series_values = original_series_values
n_lag = 12

original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
else:
    scaler = None
    scaled_values = series_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)

supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values

work_day_feature = True
week_day_feature = False
hour_feature = False

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    number_inputs += 24

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)

# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1], 1)
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1], 1)
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1], 1)

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

# design network
input_tensor = Input(shape=(X.shape[1],X.shape[2]))
x = LSTM(200, activation='tanh', recurrent_dropout=0.2)(input_tensor)
x = Dropout(0.2)(x)
output = Dense(y.shape[1])(x)
model = Model(inputs=input_tensor, outputs=output)
model.compile(loss='mean_squared_error', optimizer='adam')

model.load_weights('LSTM_model_weights1.h5')

forecasts_lstm = make_lstm_forecasts(model, batch_size, test, number_inputs, n_seq)

if normalize:
    forecasts_lstm = invert_scaler(forecasts_lstm, scaler)
else:
    forecasts_lstm = np.asarray(forecasts_lstm)

forecasts_lstm[forecasts_lstm<0] = 0

error = evaluate_forecasts(original_test, forecasts_lstm, number_inputs, n_seq, True)

#plot_forecasts(original_series_values, forecasts_lstm, len(validation), "Recurrent NN model prediction", "Sample number", "Number of sessions per hour")

all_forecasts = (forecasts+forecasts_features+forecasts_lstm)/3


evaluate_forecasts(original_test, all_forecasts, n_lag, n_seq, True)
#plot_forecasts(original_series_values, all_forecasts, len(test), "Ensemble voting of three best models prediction", "Sample number", "Number of sessions per hour")

# plot_test_data(original_series_values, len(test), "Title", "Sample number", "Number of sessions per hour")

# plot_all(original_series_values, forecasts, forecasts_features, forecasts_lstm, all_forecasts, len(test), "Predictions of the three predictors and ensemble prediction", "Sample number", "Number of sessions per hour")
start_off = -168-24*6
end_off = -24*6
print(len(test[start_off:end_off]))
plot_forecasts(original_series_values[:end_off], all_forecasts[start_off:end_off], len(test[start_off:end_off]), "Predictions of the three predictors and ensemble prediction", "Sample Number", "Number of sessions per hour")

np.savetxt("slice_1_predictions.csv", all_forecasts[start_off:end_off], delimiter=",")
