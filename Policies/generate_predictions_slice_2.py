import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from sklearn import svm
from numpy import array
import matplotlib.pyplot as plt
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, LSTM
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    max_connected_users = pd.read_csv(filename, index_col=[0], header=None, names=['TIME_KEY', 'X']).T.squeeze()
    max_connected_users.index = pd.to_datetime(max_connected_users.index, format='%Y-%m-%d %H:%M:%S.%f')
    max_connected_users = max_connected_users.resample('H').ffill().fillna(0)
    return max_connected_users


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return np.asarray([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# make one forecast with a NN,
def forecast_lstm(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag, 1)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]
    

# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# evaluate the model
def make_lstm_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test.values, forecasts))
    try:
        mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    except Exception as e:
        mape = 0.0
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse, mape


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, dashes=[4, 2], label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# plot the forecasts in the context of the original dataset
def plot_all(series, forecasts1, forecasts2, forecasts3, forecasts4, n_test, title, xlabel, ylabel):
    fig = plt.figure()
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values", marker="o", markersize=8)
    plt.plot(range(len(series)-n_test, len(series)), forecasts1, linestyle=':', label="Predictor #1 values", marker="^", markersize=4)
    plt.plot(range(len(series)-n_test, len(series)), forecasts2, linestyle=':', label="Predictor #2 values", marker="<", markersize=4)
    plt.plot(range(len(series)-n_test, len(series)), forecasts3, linestyle=':', label="Predictor #3 values", marker=">", markersize=4)
    plt.plot(range(len(series)-n_test, len(series)), forecasts4, linestyle=':', label="Predictor ensemble values", marker="v", markersize=8)
    # show the plot
    plt.title(title, fontsize=30)
    plt.xlabel(xlabel, fontsize=30)
    plt.ylabel(ylabel, fontsize=30)
    plt.legend(prop={'size': 25})
    plt.ylim(0, 650)
    plt.xlim(1900, 1924)
    plt.subplots_adjust(left=0.06, right=0.99, top=0.95, bottom=0.09)
    plt.tick_params(labelsize = 20)
    plt.show()
    fig.savefig("test.png")


def plot_test_data(series, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values)
    # show the plot
    plt.title(title, fontsize=30)
    plt.xlabel(xlabel, fontsize=30)
    plt.ylabel(ylabel, fontsize=30)
    plt.ylim(0, 750)
    plt.xlim(1650, 1995)
    plt.subplots_adjust(left=0.06, right=0.99, top=0.95, bottom=0.09)
    plt.tick_params(labelsize = 20)
    #plt.show()


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


def get_max_values(values):
    return np.array([np.amax(value) for value in values])


def get_min_values(values):
    return np.array([np.amin(value) for value in values])


def get_average_values(values):
    return np.array([np.average(value) for value in values])


def get_std_values(values):
    return np.array([np.std(value) for value in values])


def get_above_mean_values(values):
    return np.array([np.where(value > np.average(value))[0].size for value in values])


def get_below_mean_values(values):
    return np.array([np.where(value < np.average(value))[0].size for value in values])


def get_median_values(values):
    return np.array([np.median(value) for value in values])


def get_quantile_values(values, a):
    return np.array([np.quantile(value, a) for value in values])


def get_sum_values(values):
    return np.array([np.sum(value) for value in values])


def get_range_count_values(values, min, max):
    return np.array([np.sum((value >= min) & (value < max)) for value in values])


def get_index_of_max_values(values):
    return np.array([np.argmax(value) for value in values])


def get_index_of_min_values(values):
    return np.array([np.argmin(value) for value in values])


# Parameters
file = "kpi_data.csv"

second_differentiation = False

normalize = True

# Lag for second differentiation of the time series
second_differentiation_lag = 1

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7

# Number of hours to include in the vest set
test_hours = 24*7

# Batch size
batch_size = 1

differentiation, differentiation_lag = True, 1
n_lag = 12
week_day_feature, work_day_feature, hour_feature = False, True, False


df = read_file(file)

# Analysis of traffic and choose_series
series_values = df
original_series_values = series_values

if differentiation:
    series_values = differentiate(series_values, differentiation_lag)
    if second_differentiation:
        second_series_values = series_values
        series_values = differentiate(series_values, second_differentiation_lag)
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
    else:
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
else:
    original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
    week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
    hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
else:
    scaler = None
    scaled_values = series_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)


# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    number_inputs += 24

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1])
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

train = np.vstack((train, validation))

original_train = np.hstack((original_train, original_validation))

# design network
input_tensor = Input(shape=(X.shape[1],))
output = Dense(200, activation='relu')(input_tensor)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(y.shape[1])(output)
model = Model(inputs=input_tensor, outputs=output)
model.compile(loss='mean_squared_error', optimizer='adam')
model.load_weights('FFNNmodel_final.h5')


forecasts = make_nn_forecasts(model, batch_size, test, number_inputs, n_seq)

if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

if second_differentiation and differentiation:
    forecasts = invert_difference(forecasts, second_series_values.values[second_differentiation_lag-len(test):], second_differentiation_lag)
    forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)
elif differentiation:
    forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)

zero_values_test = np.where(original_test.values == 0)[0]
forecasts[forecasts<0] = 0
forecasts[zero_values_test] = 0
error, mape = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
plot_forecasts(original_series_values, forecasts, len(test), "Feed-Forward NN model prediction", "Sample number", "Number of sessions per hour")

differentiation, differentiation_lag = True, 1
n_lag = 168
week_day_feature, work_day_feature, hour_feature = False, False, False



df = read_file(file)

# Analysis of traffic and choose_series
series_values = df
original_series_values = series_values

if differentiation:
    series_values = differentiate(series_values, differentiation_lag)
    if second_differentiation:
        second_series_values = series_values
        series_values = differentiate(series_values, second_differentiation_lag)
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
    else:
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
else:
    original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
    week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
    hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
else:
    scaler = None
    scaled_values = series_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)


# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    number_inputs += 24

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1], 1)
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1], 1)
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1], 1)

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

train = np.vstack((train, validation))

original_train = np.hstack((original_train, original_validation))

stateful = False
# design network
input_tensor = Input(batch_shape=(batch_size,X.shape[1],X.shape[2]))
output = LSTM(20, activation='tanh', stateful=stateful, return_sequences=True)(input_tensor)
output = Dropout(0.2)(output)
output = LSTM(20, activation='tanh', stateful=stateful)(output)
output = Dropout(0.2)(output)   
output = Dense(y.shape[1])(output)
model = Model(inputs=input_tensor, outputs=output)
model.compile(loss='mse', optimizer='adam')
model.load_weights('LSTMmodel_final.h5')

zero_values_train = np.where(original_train == 0)[0]
zero_values_validation = np.where(original_test.values == 0)[0]


forecasts_lstm = make_lstm_forecasts(model, batch_size, test, number_inputs, n_seq)

if normalize:
    forecasts_lstm = invert_scaler(forecasts_lstm, scaler)
else:
    forecasts_lstm = np.asarray(forecasts_lstm)

if second_differentiation and differentiation:
    forecasts_lstm = invert_difference(forecasts_lstm, second_series_values.values[second_differentiation_lag-len(test):], second_differentiation_lag)
    forecasts_lstm = invert_difference(forecasts_lstm, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)
elif differentiation:
    forecasts_lstm = invert_difference(forecasts_lstm, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)

forecasts_lstm[forecasts_lstm<0] = 0
forecasts_lstm[zero_values_validation] = 0
error, mape = evaluate_forecasts(original_test, forecasts_lstm, n_lag, n_seq, True)
plot_forecasts(original_test, forecasts_lstm, len(forecasts_lstm), "LSTM model prediction", "Sample number", "Number of sessions per hour")

forecasts_two = (forecasts + forecasts_lstm)/2
error, mape = evaluate_forecasts(original_test, forecasts_two, n_lag, n_seq, True)
plot_forecasts(original_test, forecasts_two, len(forecasts_lstm), "Ensemble predictions of the feed-forward and lstm network", "Sample number", "Number of sessions per hour")

print(len(forecasts))
np.savetxt("slice_2_predictions.csv", forecasts_two, delimiter=",")