import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas import datetime
from numpy import genfromtxt


# Read data file
def read_file_slice_1(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S")).loc['2018-11-26 00:00:00':'2018-12-02 23:00:00']


# Read data file
def read_file_slice_2(filename):
    max_connected_users = pd.read_csv(filename, index_col=[0], header=None, names=['TIME_KEY', 'X']).T.squeeze()
    max_connected_users.index = pd.to_datetime(max_connected_users.index, format='%Y-%m-%d %H:%M:%S.%f')
    max_connected_users = max_connected_users.resample('H').ffill().fillna(0)
    return max_connected_users[-24*7:]


def calculate_number_sessions_in_congestion(series, threshold):
	index = np.where(series>threshold)[0]
	if np.isscalar(threshold):
		return np.sum(series[index]-threshold)
	else:
		return np.sum(series[index]-threshold[index])


slice_1_filename = "counts.csv"
slice_2_filename = "kpi_data.csv"

values_1 = read_file_slice_1(slice_1_filename).values
values_2 = read_file_slice_2(slice_2_filename).values

values_1 = np.reshape(values_1, len(values_1))
values_2 = np.reshape(values_2, len(values_2))
values_3 = values_1+values_2
plt.plot(values_1)
plt.plot(values_2)
plt.plot(values_3)
plt.show()

predictions_1 = genfromtxt('slice_1_predictions.csv', delimiter=',')
predictions_2 = genfromtxt('slice_2_predictions.csv', delimiter=',')
predictions_3 = predictions_1+predictions_2

congestion_threshold_1 = 0.8*max(values_1)
congestion_threshold_2 = 0.8*max(values_2)
congestion_threshold_3 = 0.8*max(values_3)


print("No congestion management")
normal_congestion_sessions_1 = calculate_number_sessions_in_congestion(values_1, congestion_threshold_1)
print(normal_congestion_sessions_1)
plt.plot(values_1, label="Number of sessions in the vehicular network")
plt.plot(congestion_threshold_1*np.ones((len(values_1), 1)), label="Congestion threshold")
plt.title("Number of sessions and congestion threshold for the vehicular slice with no congestion management")
plt.xlabel("Hour number in a week")
plt.ylabel("Number of sessions per hour")
plt.legend()
plt.show()

normal_congestion_sessions_2 = calculate_number_sessions_in_congestion(values_2, congestion_threshold_2)
print(normal_congestion_sessions_2)
plt.plot(values_2)
plt.plot(congestion_threshold_2*np.ones((len(values_2), 1)))
plt.show()

normal_congestion_sessions_3 = calculate_number_sessions_in_congestion(values_3, congestion_threshold_3)
print(normal_congestion_sessions_3)
plt.plot(values_3)
plt.plot(congestion_threshold_3*np.ones((len(values_3), 1)))
plt.show()


print("Reactive congestion management")
delayed_values_1 = values_1[:-1]
delayed_values_1 = np.insert(delayed_values_1, 0, 0)
delayed_values_2 = values_2[:-1]
delayed_values_2 = np.insert(delayed_values_2, 0, 0)
delayed_values_3 = values_3[:-1]
delayed_values_3 = np.insert(delayed_values_3, 0, 0)

delayed_values_with_threshold_1 = np.maximum(congestion_threshold_1, delayed_values_1)
delayed_values_with_threshold_2 = np.maximum(congestion_threshold_2, delayed_values_2)
delayed_values_with_threshold_3 = np.maximum(congestion_threshold_3, delayed_values_3)

normal_congestion_sessions_1 = calculate_number_sessions_in_congestion(values_1, delayed_values_with_threshold_1)
print(normal_congestion_sessions_1)
plt.plot(values_1, label="Number of sessions in the vehicular network")
plt.plot(delayed_values_with_threshold_1, label="Congestion threshold")
plt.title("Number of sessions and congestion threshold for the vehicular slice with reactive congestion management")
plt.xlabel("Hour number in a week")
plt.ylabel("Number of sessions per hour")
plt.legend()
plt.show()

normal_congestion_sessions_2 = calculate_number_sessions_in_congestion(values_2, delayed_values_with_threshold_2)
print(normal_congestion_sessions_2)
plt.plot(values_2)
plt.plot(delayed_values_with_threshold_2)
plt.show()

normal_congestion_sessions_3 = calculate_number_sessions_in_congestion(values_3, delayed_values_with_threshold_3)
print(normal_congestion_sessions_3)
plt.plot(values_3)
plt.plot(delayed_values_with_threshold_3)
plt.show()


print("Pro-active congestion management")
prediction_with_threshold_1 = np.maximum(congestion_threshold_1, predictions_1)
prediction_with_threshold_2 = np.maximum(congestion_threshold_2, predictions_2)
prediction_with_threshold_3 = np.maximum(congestion_threshold_3, predictions_3)

predicted_congestion_sessions_1 = calculate_number_sessions_in_congestion(values_1, prediction_with_threshold_1)
print(predicted_congestion_sessions_1)
plt.plot(values_1, label="Number of sessions in the vehicular network")
plt.plot(prediction_with_threshold_1, label="Congestion threshold")
plt.title("Number of sessions and congestion threshold for the vehicular slice with pro-active confestion management")
plt.xlabel("Hour number in a week")
plt.ylabel("Number of sessions per hour")
plt.legend()
plt.show()

predicted_congestion_sessions_2 = calculate_number_sessions_in_congestion(values_2, prediction_with_threshold_2)
print(predicted_congestion_sessions_2)
plt.plot(values_2)
plt.plot(prediction_with_threshold_2)
plt.show()

predicted_congestion_sessions_3 = calculate_number_sessions_in_congestion(values_3, prediction_with_threshold_3)
print(predicted_congestion_sessions_3)
plt.plot(values_3)
plt.plot(prediction_with_threshold_3)
plt.show()


