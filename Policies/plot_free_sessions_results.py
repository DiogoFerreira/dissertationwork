from numpy import genfromtxt
import matplotlib.pyplot as plt

naive_policies = genfromtxt('naive_policies_free_sessions.csv', delimiter=',')
best_policies = genfromtxt('best_policies_free_sessions.csv', delimiter=',')
smart_policies = genfromtxt('smart_policies_free_sessions.csv', delimiter=',')

plt.title("Ratio of the free sessions in the network divided by all the current sessions for different available number of sessions")
plt.xlabel("Ratio of the number of sessions available by the maximum number of sessions needed in the network")
plt.ylabel("Ratio of the free sessions/current sessions")
plt.plot([x/4344 for x in range(20, 9000, 100)], naive_policies, label="Fixed threshold algorithm")
plt.plot([x/4344 for x in range(20, 9000, 100)], smart_policies, label="Dynamic threshold algorithm with predicted values")
plt.plot([x/4344 for x in range(20, 9000, 100)], best_policies, label="Dynamic threshold algorithm with real values")
plt.legend()
plt.show()

