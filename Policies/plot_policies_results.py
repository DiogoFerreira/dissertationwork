from numpy import genfromtxt
import matplotlib.pyplot as plt

naive_policies = genfromtxt('naive_policies.csv', delimiter=',')
best_policies = genfromtxt('best_policies.csv', delimiter=',')
smart_policies = genfromtxt('smart_policies.csv', delimiter=',')

plt.title("Percentage of sessions lost with different policies for different available number of sessions")
plt.xlabel("Ratio of the number of sessions available by the maximum number of sessions needed in the network")
plt.ylabel("Percentage of sessions lost")
plt.plot([x/4344 for x in range(20, 4500, 100)], naive_policies, label="Fixed threshold algorithm")
plt.plot([x/4344 for x in range(20, 4500, 100)], smart_policies, label="Dynamic threshold algorithm with predicted values")
plt.plot([x/4344 for x in range(20, 4500, 100)], best_policies, label="Dynamic threshold algorithm with real values")
plt.legend()
plt.show()

