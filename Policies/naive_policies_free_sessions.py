import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas import datetime


# Read data file
def read_file_slice_1(filename):
    all_data = pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))
    return all_data.loc['2018-09-18':'2018-11-26 00:00:00'], all_data.loc['2018-11-26 00:00:00':'2018-12-02 23:00:00']


# Read data file
def read_file_slice_2(filename):
    max_connected_users = pd.read_csv(filename, index_col=[0], header=None, names=['TIME_KEY', 'X']).T.squeeze()
    max_connected_users.index = pd.to_datetime(max_connected_users.index, format='%Y-%m-%d %H:%M:%S.%f')
    max_connected_users = max_connected_users.resample('H').ffill().fillna(0)
    return max_connected_users[:-24*7], max_connected_users[-24*7:]


def calculate_number_free_sessions_below_threshold(series, threshold):
	index = np.where(series<threshold)[0]	
	return np.sum(threshold-series[index]), len(index)


total_percentage_above_threshold = []
min_users_per_slice = 10

for max_users_network in range(min_users_per_slice*2, 9000, 100):

	#max_users_network = 6000
	
	slice_1_filename = "counts.csv"
	slice_2_filename = "kpi_data.csv"

	train_s1, test_s1 = read_file_slice_1(slice_1_filename)
	train_s2, test_s2 = read_file_slice_2(slice_2_filename)

	free_sessions_1_list = []
	free_sessions_2_list = []

	thres_range = range(min_users_per_slice, max_users_network-min_users_per_slice+1, 10)

	for t in thres_range:
		threshold_1 = max_users_network-t
		threshold_2 = t

		free_sessions_below_threshold_1, hours_below_threshold_1 = calculate_number_free_sessions_below_threshold(train_s1.values, threshold_1)
		free_sessions_below_threshold_2, hours_below_threshold_2 = calculate_number_free_sessions_below_threshold(train_s2.values, threshold_2)
		free_sessions_1 = free_sessions_below_threshold_1/(np.sum(train_s1.values)+np.sum(train_s2.values))
		free_sessions_2 = free_sessions_below_threshold_2/(np.sum(train_s1.values)+np.sum(train_s2.values))
		free_sessions_1_list.append(free_sessions_1)
		free_sessions_2_list.append(free_sessions_2)

	sum_free_sessions_list = [sum(x) for x in zip(free_sessions_1_list, free_sessions_2_list)]


	# plt.plot(thres_range, free_sessions_1_list, label="Vehicular communication sessions")
	# plt.plot(thres_range, free_sessions_2_list, label="Mobile communication sessions")
	# plt.plot(thres_range, sum_free_sessions_list, label="Vehicular and Mobile communication sessions")
	# plt.title("Ratio of the free sessions in the network divided by all the current sessions")
	# plt.xlabel("Maximum number of sessions in the mobile communication slice")
	# plt.ylabel("Ratio of the free sessions/current sessions")
	# plt.legend()
	# plt.show()


	lost_sessions = min(sum_free_sessions_list)
	idx = sum_free_sessions_list.index(lost_sessions)


	threshold_2 = thres_range[idx]
	threshold_1 = max_users_network-threshold_2

	#threshold_1_line = threshold_1*np.ones((len(test_s1), 1))
	#plt.plot(test_s1.values)
	#plt.plot(threshold_1_line)
	#plt.show()

	#threshold_2_line = threshold_2*np.ones((len(test_s2), 1))
	#plt.plot(test_s2.values)
	#plt.plot(threshold_2_line)
	#plt.show()

	free_sessions_below_threshold_1, hours_above_threshold_1 = calculate_number_free_sessions_below_threshold(test_s1.values, threshold_1)
	free_sessions_below_threshold_2, hours_above_threshold_2 = calculate_number_free_sessions_below_threshold(test_s2.values, threshold_2)

	percentage_above_threshold_1 = free_sessions_below_threshold_1/(np.sum(test_s1.values)+np.sum(test_s2.values))
	percentage_above_threshold_2 = free_sessions_below_threshold_2/(np.sum(test_s1.values)+np.sum(test_s2.values))


	# print(percentage_above_threshold_1)
	# print(percentage_above_threshold_2)
	# print(percentage_above_threshold_1+percentage_above_threshold_2)

	# np.savetxt("slice_1_real.csv", test_s1.values, delimiter=",")
	# np.savetxt("slice_2_real.csv", test_s2.values, delimiter=",")

	total_percentage_above_threshold.append(percentage_above_threshold_1+percentage_above_threshold_2)
	#exit()
	

# plt.title("Percentage of sessions lost with different policies for different available number of sessions")
# plt.xlabel("Ratio of the number of sessions available by the maximum number of sessions needed in the network")
# plt.ylabel("Percentage of sessions lost")
# plt.plot([x/4344 for x in range(0, 4500, 10)], total_percentage_above_threshold, label="Naive policies")
# plt.show()

np.savetxt("naive_policies_free_sessions.csv", np.asarray(total_percentage_above_threshold), delimiter=",")
