import pandas as pd
from matplotlib import pyplot

one_neuron = pd.read_csv("one_neuron.txt", skipinitialspace=True, sep=" ")
two_neurons = pd.read_csv("two_neurons.txt", skipinitialspace=True, sep=" ")
three_neurons = pd.read_csv("three_neurons.txt", skipinitialspace=True, sep=" ")
five_neurons = pd.read_csv("five_neurons.txt", skipinitialspace=True, sep=" ")


fig, axes = pyplot.subplots(nrows=2, ncols=2)

axes[0, 0].plot(one_neuron.loc[:, "test"], color='blue', label='Test data')
axes[0, 0].plot(one_neuron.loc[:, "train"], color='orange', label='Training data')
axes[0, 0].set_title("One neuron")
axes[0, 0].legend()

axes[0, 1].plot(two_neurons.loc[:, "test"], color='blue', label='Test data')
axes[0, 1].plot(two_neurons.loc[:, "train"], color='orange', label='Training data')
axes[0, 1].set_title("Two neuron")
axes[0, 1].legend()

axes[1, 0].plot(three_neurons.loc[:, "test"], color='blue', label='Test data')
axes[1, 0].plot(three_neurons.loc[:, "train"], color='orange', label='Training data')
axes[1, 0].set_title("Three neuron")
axes[1, 0].legend()

axes[1, 1].plot(five_neurons.loc[:, "test"], color='blue', label='Test data')
axes[1, 1].plot(five_neurons.loc[:, "train"], color='orange', label='Training data')
axes[1, 1].set_title("Four neurons")
axes[1, 1].legend()

fig.text(0.5, 0.04, 'Number of epochs', ha='center')
fig.text(0.04, 0.5, 'Root Mean Squared Error', va='center', rotation='vertical')
pyplot.show()

pyplot.plot(one_neuron.loc[:, "test"], color='blue', label='One neuron')
pyplot.plot(two_neurons.loc[:, "test"], color='orange', label='Two neurons')
pyplot.plot(three_neurons.loc[:, "test"], color='green', label='Three neurons')
pyplot.plot(five_neurons.loc[:, "test"], color='red', label='Four neurons')
pyplot.legend()
pyplot.show()
