import csv
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import glob
import time
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, AdaBoostRegressor, BaggingRegressor
from sklearn.svm import LinearSVR
from sklearn.linear_model import Ridge, SGDRegressor, Lasso, ElasticNet, LinearRegression


timestamp = np.zeros((0,0))
values = np.zeros((0,0))

def datetime_to_number(date):
	return time.mktime(date.timetuple())

def number_to_datetime(number):
	return datetime.fromtimestamp(time.mktime(time.localtime(number)))

def show_metrics(regressor, values, predictions):
	# The coefficients
	#print('Coefficients: \n', regressor.coef_)
	# The mean squared error
	print("Mean squared error: %.2f"
      % mean_squared_error(values, predictions))
	# Explained variance score: 1 is perfect prediction
	print('Variance score: %.2f' % r2_score(values, predictions))

# 1 - Input data
#for file in glob.glob("./LSTM Based Traffic Flow Prediction with Missing Data/*.csv"):
with open("./train14719.csv") as f:
    reader = csv.reader(f, delimiter=',')
    next(reader)
    data = np.array([(datetime.strptime(col1, "%Y-%m-%d %H:%M:%S"), int(col2)) for col1, col2 in reader])
    timestamp = np.append(timestamp, data[:, 0])
    values = np.append(values, data[:, 1])

plt.plot(timestamp, values)
plt.show()

# 2 - Data processing

func = np.vectorize(datetime_to_number)
timestamp_number = func(timestamp)
timestamp_number = timestamp_number.reshape(-1,1)

def applyRegressionModel(regressor, X, Y, test_X, test_Y):
	regressor = regressor()
	regressor.fit(X, Y)
	predictions = regressor.predict(test_X)
	show_metrics(regressor, Y, predictions)
	return predictions


predictionsLinReg = applyRegressionModel(LinearRegression, timestamp_number, values, timestamp_number, values)
predictionsDecTree = applyRegressionModel(DecisionTreeRegressor, timestamp_number, values, timestamp_number, values)
predictionsRanFor = applyRegressionModel(RandomForestRegressor, timestamp_number, values, timestamp_number, values)
predictionsLinSVR = applyRegressionModel(LinearSVR, timestamp_number, values, timestamp_number, values)
predictionsSGDReg = applyRegressionModel(SGDRegressor, timestamp_number, values, timestamp_number, values)
predictionsRidge = applyRegressionModel(Ridge, timestamp_number, values, timestamp_number, values)
predictionsLasso = applyRegressionModel(Lasso, timestamp_number, values, timestamp_number, values)
predictionsElasticNet = applyRegressionModel(ElasticNet, timestamp_number, values, timestamp_number, values)
predictionsGradBoost = applyRegressionModel(GradientBoostingRegressor, timestamp_number, values, timestamp_number, values)
predictionsBagReg = applyRegressionModel(BaggingRegressor, timestamp_number, values, timestamp_number, values)
predictionsAdaBoost = applyRegressionModel(AdaBoostRegressor, timestamp_number, values, timestamp_number, values)

# Plot outputs
plt.subplot(341)
plt.scatter(timestamp, predictionsLinReg,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("Linear Regression")

plt.subplot(342)
plt.scatter(timestamp, predictionsDecTree, color='black')
plt.plot(timestamp, values, color="blue")
plt.title("Decision Tree Regression")

plt.subplot(343)
plt.scatter(timestamp, predictionsRanFor,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("Random Forest Regression")

plt.subplot(344)
plt.scatter(timestamp, predictionsLinSVR,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("SVR Regression")

plt.subplot(345)
plt.scatter(timestamp, predictionsSGDReg,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("SGD Regression")

plt.subplot(346)
plt.scatter(timestamp, predictionsRidge,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("Ridge Regression")

plt.subplot(347)
plt.scatter(timestamp, predictionsLasso,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("Lasso Regression")

plt.subplot(348)
plt.scatter(timestamp, predictionsElasticNet,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("ElasticNet Regression")

plt.subplot(349)
plt.scatter(timestamp, predictionsGradBoost,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("Gradient Boosting Regression")

plt.subplot(3,4,10)
plt.scatter(timestamp, predictionsBagReg,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("Bagging Regression")

plt.subplot(3,4,11)
plt.scatter(timestamp, predictionsAdaBoost,  color='black')
plt.plot(timestamp, values, color='blue')
plt.title("Ada Boost Regression")

# Train:
# Neural Network
# LSTM
# GRU
# Reinforcement learning


plt.show()

