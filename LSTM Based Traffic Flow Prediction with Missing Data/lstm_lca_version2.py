from pandas import read_csv
from pandas import datetime
from pandas import DataFrame
from pandas import concat
from pandas import Series
from statsmodels.tsa.stattools import adfuller
from matplotlib import pyplot
from numpy import log
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras import optimizers
from numpy import array
from math import sqrt


# date-time parsing function for loading the dataset
def parser(x):
    return datetime.strptime(x, "%Y-%m-%d %H:%M:%S")


# remove rows with zero values
def remove_zeros(x):
    return x[x > 0]


def check_is_stationary(series):
    # extract raw values
    raw_values = series.values

    # Augmented Dickey-Fuller test
    # If p-value is <0.05, the data is not stationary
    result = adfuller(raw_values)

    if result[1]>0.05:
        return True
    else:
        return False


# create a differenced series
def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return Series(diff)


# invert differenced forecast
def inverse_difference(last_ob, forecast):
    # invert first forecast
    inverted = list()
    inverted.append(forecast[0] + last_ob)
    # propagate difference forecast using inverted first value
    for i in range(1, len(forecast)):
        inverted.append(forecast[i] + inverted[i - 1])
    return inverted


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Prepare the data to feed the LSTM
def prepare_data(series, test_percentage, n_lag, n_seq):
    series_without_zeros = remove_zeros(series)
    
    # Choose a series
    chosen_series = series_without_zeros

    # extract raw values
    raw_values = chosen_series.values

    is_stationary = check_is_stationary(chosen_series)

    if is_stationary:
        # transform data to be stationary
        diff_series = difference(raw_values, 1)
        diff_values = diff_series.values
        diff_values = diff_values.reshape(len(diff_values), 1)
        
        raw_values = diff_values
    else:
        raw_values = raw_values.reshape(len(raw_values), 1)

    # rescale values to -1, 1
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaled_values = scaler.fit_transform(raw_values)
    scaled_values = scaled_values.reshape(len(scaled_values), 1)

    # transform into supervised learning problem X, y
    supervised = series_to_supervised(scaled_values, n_lag, n_seq)
    supervised_values = supervised.values

    n_test = int(test_percentage*len(supervised_values))

    # split into train and test sets
    train, test = supervised_values[0:-n_test], supervised_values[-n_test:]
    return chosen_series, scaler, train, test, is_stationary


# Create a 1 layer model of lstm
def lstm_model_1layer(x, y, z, output_label, n_neurons1, dropout=False, dropout_value=0.2):
    model = Sequential()
    model.add(LSTM(n_neurons1, batch_input_shape=(x, y, z), stateful=True, activation='relu'))
    if dropout:
        model.add(Dropout(dropout_value))
    model.add(Dense(output_label.shape[1]))
    return model


# Create a 2 layer model of lstm
def lstm_model_2layers(x, y, z, output_label, n_neurons1, n_neurons2, dropout1=False, dropout1Value=0.2, dropout2=False, dropout2Value=0.2):
    model = Sequential()
    model.add(LSTM(n_neurons1, batch_input_shape=(x, y, z), stateful=True, return_sequences=True))
    if dropout1:
        model.add(Dropout(dropout1_value))
    model.add(LSTM(n_neurons2, stateful=True))
    if dropout2:
        model.add(Dropout(dropout2_value))
    model.add(Dense(output_label.shape[1]))
    return model



# fit an LSTM network to training data
def fit_lstm(train, test, n_lag, n_seq, n_batch, nb_epoch, n_layers, n_neurons, series, scaler, differentiate, actual_test, actual_train, n_neurons2=1):
    # reshape training into [samples, timesteps, features]
    X, y = train[:, 0:n_lag], train[:, n_lag:]
    X = X.reshape(X.shape[0], 1, X.shape[1])

    # design network
    if n_layers == 1:
        model = lstm_model_1layer(n_batch, X.shape[1], X.shape[2], y, n_neurons)
    else:
        model = lstm_model_2layers(n_batch, X.shape[1], X.shape[2], y, n_neurons, n_neurons2)
    
    optimizer = optimizers.Adam(lr=0.01)
    model.compile(loss='mean_squared_error', optimizer=optimizer)
    
    # fit network
    train_rmse, test_rmse = list(), list()
    for i in range(nb_epoch):
        print("Epoch %d" % (i+1))
        model.fit(X, y, epochs=1, batch_size=n_batch, verbose=1, shuffle=False)
        model.reset_states()

        # evaluate model on train data
        # make forecasts
        forecasts = make_lstm_forecasts(model, n_batch, train, n_lag, n_seq)
        # inverse transform forecasts and train
        forecasts = inverse_transform(series, forecasts, scaler, len(train) + 2, differentiate)
        # evaluate forecasts
        train_rmse.append(evaluate_forecasts(actual_train, forecasts, n_lag, n_seq, False))
        model.reset_states()

        # evaluate model on test data
        forecasts = make_lstm_forecasts(model, n_batch, test, n_lag, n_seq)
        # inverse transform forecasts and test
        forecasts = inverse_transform(series, forecasts, scaler, len(test) + 2, differentiate)
        # evaluate forecasts
        test_rmse.append(evaluate_forecasts(actual_test, forecasts, n_lag, n_seq, False))
        model.reset_states()

    history = DataFrame()
    history['train'], history['test'] = train_rmse, test_rmse
    return model, history


# make one forecast with an LSTM,
def forecast_lstm(model, X, n_batch):
    # reshape input pattern to [samples, timesteps, features]
    X = X.reshape(1, 1, len(X))
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the persistence model
def make_lstm_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch)
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# inverse data transform on forecasts
def inverse_transform(series, forecasts, scaler, n_test, differentiate):
    inverted = list()
    for i in range(len(forecasts)):
        # create array from forecast
        forecast = array(forecasts[i])
        forecast = forecast.reshape(1, len(forecast))
        # invert scaling
        inv_scale = scaler.inverse_transform(forecast)
        inv_scale = inv_scale[0, :]

        if differentiate:
            # invert differencing
            index = len(series) - n_test + i - 1
            last_ob = series.values[index]
            inv_diff = inverse_difference(last_ob, inv_scale)
            # store
            inverted.append(inv_diff)
        else:
            inverted.append(inv_scale)

    return inverted


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    for i in range(n_seq):
        actual = [row[i] for row in test]
        predicted = [forecast[i] for forecast in forecasts]
        rmse = sqrt(mean_squared_error(actual, predicted))
        if debug:
            print('t+%d RMSE: %f' % ((i + 1), rmse))
        return rmse


# load dataset
series = read_csv('train13519.csv', parse_dates=[0], index_col=0, squeeze=True, date_parser=parser)
output_file = "output_history_2.txt"


# Parameters
n_lag = 1
n_seq = 1
test_percentage = 0.2

# Prepare the data
chosen_series, scaler, train, test,  differentiate = prepare_data(series, test_percentage, n_lag, n_seq)
n_test = len(test)
n_train = len(train)

actual_test = [row[n_lag:] for row in test]
actual_test = inverse_transform(chosen_series, actual_test, scaler, n_test + 2, differentiate)

actual_train = [row[n_lag:] for row in train]
actual_train = inverse_transform(chosen_series, actual_train, scaler, n_train + 2, differentiate)

# fit lstm model
n_epochs = 5
n_batch = 1
n_layers = 1
list_neurons = [1]

i=0


writer = open(output_file, 'w')

for n_neurons in list_neurons:
    writer.write("Training with %d neurons\n" % n_neurons)
    model, history = fit_lstm(train, test, n_lag, n_seq, n_batch, n_epochs, n_layers, n_neurons, chosen_series, scaler, differentiate, actual_test, actual_train, n_neurons)
    writer.write(history.to_string())


writer.close()

