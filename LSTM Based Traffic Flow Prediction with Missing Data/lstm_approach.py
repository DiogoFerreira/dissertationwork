from pandas import read_csv
from pandas import datetime
from pandas import DataFrame
from pandas import concat
from pandas import Series
from pandas.plotting import lag_plot
from pandas.plotting import autocorrelation_plot
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.ar_model import AR
from matplotlib import pyplot
from numpy import log
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from numpy import array
from math import sqrt


# date-time parsing function for loading the dataset
def parser(x):
    return datetime.strptime(x, "%Y-%m-%d %H:%M:%S")


# remove rows with zero values
def remove_zeros(x):
    return x[x > 0]


def plot_series(series1, series2):
    fig, axes = pyplot.subplots(nrows=1, ncols=2)

    # line plot
    series1.plot(ax=axes[0], title="Original data")
    axes[0].set_xlabel('date time')
    axes[0].set_ylabel('traffic volume')

    # line plot
    pyplot.plot(series2.values)
    axes[1].set_title("Zero values removed")
    axes[1].set_xlabel("Sample number")
    axes[1].set_ylabel('traffic volume')
    pyplot.show()


def compare_correlation(series1, series2):
    fig, axes = pyplot.subplots(nrows=3, ncols=2)

    # Compare correlation between two methods
    values = DataFrame(series1.values)
    dataframe = concat([values.shift(1), values], axis=1)
    dataframe.columns = ['t-1', 't+1']
    result = dataframe.corr()
    print(result)
    autocorrelation_plot(series1, ax=axes[0,0])
    lag_plot(series1, ax=axes[1,0])
    plot_acf(series1, lags=31, ax=axes[2,0])

    values = DataFrame(series2.values)
    dataframe = concat([values.shift(1), values], axis=1)
    dataframe.columns = ['t-1', 't+1']
    result = dataframe.corr()
    print(result)
    autocorrelation_plot(series2, ax=axes[0,1])
    lag_plot(series2, ax=axes[1,1])
    plot_acf(series2, lags=31, ax=axes[2,1])
    pyplot.show()


def check_is_stationary(series, log_diferentiation=False):
    # Histogram of series
    series.hist()
    pyplot.title("Histogram")
    pyplot.xlabel("Traffic Volume")
    pyplot.ylabel("Frequency")
    pyplot.show()

    # extract raw values
    raw_values = series.values

    # It is not a gaussian. Mean an variance are less meaningful
    # Calculate mean and variance
    split = len(raw_values) // 2
    X1, X2 = raw_values[0:split], raw_values[split:]
    mean1, mean2 = X1.mean(), X2.mean()
    var1, var2 = X1.var(), X2.var()
    print('mean1=%f, mean2=%f' % (mean1, mean2))
    print('variance1=%f, variance2=%f' % (var1, var2))

    # Try log differentation (in this case, it does not make a difference)
    if log_diferentiation:
        log_raw_values = log(raw_values)
        pyplot.hist(log_raw_values)
        pyplot.show()
        pyplot.plot(log_raw_values)
        pyplot.show()

        # Calculate mean and variance
        split = len(raw_values) // 2
        X1, X2 = raw_values[0:split], raw_values[split:]
        mean1, mean2 = X1.mean(), X2.mean()
        var1, var2 = X1.var(), X2.var()
        print('mean1=%f, mean2=%f' % (mean1, mean2))
        print('variance1=%f, variance2=%f' % (var1, var2))

    # Augmented Dickey-Fuller test
    # If p-value is <0.05, the data is not stationary
    result = adfuller(raw_values)
    print('ADF Statistic: %f' % result[0])
    print('p-value: %f' % result[1])
    print('Critical Values:')
    for key, value in result[4].items():
        print('\t%s: %.3f' % (key, value))

    if result[1]>0.05:
        return True
    else:
        return False


# create a differenced series
def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return Series(diff)


# invert differenced forecast
def inverse_difference(last_ob, forecast):
    # invert first forecast
    inverted = list()
    inverted.append(forecast[0] + last_ob)
    # propagate difference forecast using inverted first value
    for i in range(1, len(forecast)):
        inverted.append(forecast[i] + inverted[i - 1])
    return inverted


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Prepare the data to feed the LSTM
def prepare_data(series, test_percentage, n_lag, n_seq):
    series_without_zeros = remove_zeros(series)
    plot_series(series, series_without_zeros)

    compare_correlation(series, series_without_zeros)

    # Choose a series
    chosen_series = series_without_zeros

    # extract raw values
    raw_values = chosen_series.values

    is_stationary = check_is_stationary(chosen_series)

    if is_stationary:
        # transform data to be stationary
        diff_series = difference(raw_values, 1)
        diff_values = diff_series.values
        diff_values = diff_values.reshape(len(diff_values), 1)
        pyplot.plot(diff_values)
        pyplot.show()
        raw_values = diff_values
    else:
        raw_values = raw_values.reshape(len(raw_values), 1)

    # rescale values to -1, 1
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaled_values = scaler.fit_transform(raw_values)
    scaled_values = scaled_values.reshape(len(scaled_values), 1)

    # transform into supervised learning problem X, y
    supervised = series_to_supervised(scaled_values, n_lag, n_seq)
    supervised_values = supervised.values

    n_test = int(test_percentage*len(supervised_values))

    # split into train and test sets
    train, test = supervised_values[0:-n_test], supervised_values[-n_test:]
    return chosen_series, scaler, train, test, is_stationary


# make a persistence forecast
def persistence(last_ob, n_seq):
    return [last_ob for i in range(n_seq)]


# evaluate the persistence model
def make_persistence_forecasts(train, test, n_lag, n_seq):
    return [persistence(train[-1, 0:n_lag][-1], n_seq)]+[persistence(test[i, 0:n_lag][-1], n_seq) for i in range(len(test)-1)]


# fit an LSTM network to training data
def fit_lstm(train, test, n_lag, n_seq, n_batch, nb_epoch, n_neurons, series, scaler, differentiate, actual_test, actual_train):
    # reshape training into [samples, timesteps, features]
    X, y = train[:, 0:n_lag], train[:, n_lag:]
    X = X.reshape(X.shape[0], 1, X.shape[1])
    # design network
    model = Sequential()
    model.add(LSTM(n_neurons, batch_input_shape=(n_batch, X.shape[1], X.shape[2]), stateful=True))
    #model.add(Dropout(0.2))
    model.add(Dense(y.shape[1]))
    model.compile(loss='mean_squared_error', optimizer='adam')
    # fit network
    train_rmse, test_rmse = list(), list()
    for i in range(nb_epoch):
        print("Epoch %d" % (i+1))
        model.fit(X, y, epochs=1, batch_size=n_batch, verbose=0, shuffle=False)
        model.reset_states()

        # evaluate model on train data
        # make forecasts
        forecasts = make_lstm_forecasts(model, n_batch, train, n_lag, n_seq)
        # inverse transform forecasts and train
        forecasts = inverse_transform(series, forecasts, scaler, len(train) + 2, differentiate)
        # evaluate forecasts
        train_rmse.append(evaluate_forecasts(actual_train, forecasts, n_lag, n_seq, False))
        model.reset_states()

        # evaluate model on test data
        forecasts = make_lstm_forecasts(model, n_batch, test, n_lag, n_seq)
        # inverse transform forecasts and test
        forecasts = inverse_transform(series, forecasts, scaler, len(test) + 2, differentiate)
        # evaluate forecasts
        test_rmse.append(evaluate_forecasts(actual_test, forecasts, n_lag, n_seq, False))
        model.reset_states()

    history = DataFrame()
    history['train'], history['test'] = train_rmse, test_rmse
    return model, history


# make one forecast with an LSTM,
def forecast_lstm(model, X, n_batch):
    # reshape input pattern to [samples, timesteps, features]
    X = X.reshape(1, 1, len(X))
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the persistence model
def make_lstm_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch)
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# inverse data transform on forecasts
def inverse_transform(series, forecasts, scaler, n_test, differentiate):
    inverted = list()
    for i in range(len(forecasts)):
        # create array from forecast
        forecast = array(forecasts[i])
        forecast = forecast.reshape(1, len(forecast))
        # invert scaling
        inv_scale = scaler.inverse_transform(forecast)
        inv_scale = inv_scale[0, :]

        if differentiate:
            # invert differencing
            index = len(series) - n_test + i - 1
            last_ob = series.values[index]
            inv_diff = inverse_difference(last_ob, inv_scale)
            # store
            inverted.append(inv_diff)
        else:
            inverted.append(inv_scale)

    return inverted


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    for i in range(n_seq):
        actual = [row[i] for row in test]
        predicted = [forecast[i] for forecast in forecasts]
        rmse = sqrt(mean_squared_error(actual, predicted))
        if debug:
            print('t+%d RMSE: %f' % ((i + 1), rmse))
        return rmse


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    pyplot.plot(series.values, label="Real values")
    pyplot.plot(range(len(series)-n_test-1, len(series)-1), forecasts, label="Predicted values")
    # show the plot
    pyplot.title(title)
    pyplot.xlabel(xlabel)
    pyplot.ylabel(ylabel)
    pyplot.legend()
    pyplot.show()


# fit autoregression model
def fit_ar(train, n_lag):
    model = AR(train[:, n_lag - 1])
    model_fit = model.fit()
    return model_fit


# make autoregressive forecast
def make_ar_forecasts(train, test, window, n_lag, coef):
    # walk forward over time steps in test
    history = train[len(train) - window:, n_lag - 1]
    history = [history[i] for i in range(len(history))]
    predictions = list()
    for t in range(len(test)):
        length = len(history)
        lag = [history[i] for i in range(length - window, length)]
        yhat = coef[0]
        for d in range(window):
            yhat += coef[d + 1] * lag[window - d - 1]
        obs = test[t, n_lag - 1]
        predictions.append(yhat)
        history.append(obs)
        #print('predicted=%f, expected=%f' % (yhat, obs))
    return predictions, coef


# load dataset
series = read_csv('train13519.csv', parse_dates=[0], index_col=0, squeeze=True, date_parser=parser)
# summarize first few rows
print(series.head())

# Parameters
n_lag = 1
n_seq = 1
test_percentage = 0.2

# Prepare the data
chosen_series, scaler, train, test,  differentiate = prepare_data(series, test_percentage, n_lag, n_seq)
n_test = len(test)
n_train = len(train)

# Persistence Model (Naive Model)
# make forecasts
forecasts = make_persistence_forecasts(train, test, n_lag, n_seq)
forecasts = inverse_transform(chosen_series, forecasts, scaler, n_test + 2, differentiate)
actual_test = [row[n_lag:] for row in test]
actual_test = inverse_transform(chosen_series, actual_test, scaler, n_test + 2, differentiate)

actual_train = [row[n_lag:] for row in train]
actual_train = inverse_transform(chosen_series, actual_train, scaler, n_train + 2, differentiate)

# evaluate forecasts
evaluate_forecasts(actual_test, forecasts, n_lag, n_seq)
# plot forecasts
plot_forecasts(chosen_series, forecasts, n_test, "Persistence model prediction", "Sample number", "Traffic volume")


# Autoregression model
model_fit = fit_ar(train, n_lag)
window = model_fit.k_ar
coef = model_fit.params
print('Lag: %s' % window)
print('Coefficients: %s' % coef)
predictions, coef = make_ar_forecasts(train, test, window, n_lag, coef)
forecasts = inverse_transform(chosen_series, [[p] for p in predictions], scaler, n_test + 2, differentiate)
evaluate_forecasts(actual_test, forecasts, n_lag, n_seq)
plot_forecasts(chosen_series, forecasts, n_test, "Autoregression model prediction", "Sample number", "Traffic volume")


# fit lstm model
n_epochs = 20
n_batch = 1
list_neurons = [1, 2, 3, 5, 10, 20]

i=0

fig, axes = pyplot.subplots(nrows=3, ncols=2)
for n_neurons in list_neurons:
    print("Training with %d neurons" % n_neurons)

    model, history = fit_lstm(train, test, n_lag, n_seq, n_batch, n_epochs, n_neurons, chosen_series, scaler, differentiate, actual_test, actual_train)
    axes[i // 2, i % 2].plot(range(1, len(history['train'])+1), history['train'], color='blue', label="Train data")
    axes[i // 2, i % 2].plot(range(1, len(history['test'])+1), history['test'], color='orange', label="Test data")
    axes[i // 2, i % 2].set_title(str(n_neurons) + " neurons")

    axes[i // 2, i % 2].legend()
    print(history)

    i = i + 1

fig.text(0.5, 0.04, 'Number of epochs', ha='center')
fig.text(0.04, 0.5, 'Root Mean Squared Error', va='center', rotation='vertical')
pyplot.show()

n_neurons = 1
n_epochs = 2

# Make final forecast
model, history = fit_lstm(train, test, n_lag, n_seq, n_batch, n_epochs, n_neurons, chosen_series, scaler, differentiate, actual_test, actual_train)
# make forecasts
forecasts = make_lstm_forecasts(model, n_batch, test, n_lag, n_seq)
# inverse transform forecasts and test
forecasts = inverse_transform(chosen_series, forecasts, scaler, n_test + 2, differentiate)
# evaluate forecasts
evaluate_forecasts(actual_test, forecasts, n_lag, n_seq)
# plot forecasts
plot_forecasts(chosen_series, forecasts, n_test, "LSTM model prediction (with "+str(n_neurons)+" neurons and "+str(n_epochs)+" epochs)", "Sample number", "Traffic volume")

