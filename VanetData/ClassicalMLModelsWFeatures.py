import holidays
import numpy as np
import pandas as pd
from math import sqrt
from pandas import Series
import matplotlib.pyplot as plt
from xgboost import XGBRegressor
from sklearn import svm
from pandas import datetime, DataFrame, concat
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.linear_model import LinearRegression, SGDRegressor, Ridge, Lasso, ElasticNet
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, AdaBoostRegressor


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


def choose_series(df):
    return df.loc['2018-09-18':'2018-11-18']["values"]


def differentiate(series):
    return Series(np.diff(series))


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


def invert_difference(original_series, series):
    return Series([series[i]+original_series[i] for i in range(0, len(series))])


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test, forecasts))
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
    return rmse


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values t+1")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# Parameters
file = "counts.csv"
DEBUG = False
seconds_to_group = 900
test_percentage = 0.2
n_seq = 1
n_lag = 10
differentiation = False
normalize = True
week_day_feature = False
work_day_feature = True
hour_feature = True

df = read_file(file)

# Analysis of traffic and choose_series
original_series = choose_series(df)

chosen_series = original_series

if differentiation:
    chosen_series = differentiate(chosen_series)
    week_day_array = np.array([date_to_week_day(index) for index in original_series.index[n_lag + 1:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in original_series.index[n_lag + 1:]])
    hour_array = np.array([date_to_hour(index) for index in original_series.index[n_lag + 1:]])
else:
    week_day_array = np.array([date_to_week_day(index) for index in original_series.index[n_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in original_series.index[n_lag:]])
    hour_array = np.array([date_to_hour(index) for index in original_series.index[n_lag:]])


if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(chosen_series.values.reshape(len(chosen_series.values), 1))
else:
    scaler = None
    scaled_values = chosen_series.values

scaled_values = scaled_values.reshape(len(scaled_values), 1)


# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    number_inputs += 24


n_test = int(test_percentage*len(supervised_values))

# split into train and test sets
train, test = supervised_values[:-n_test], supervised_values[-n_test:]

if normalize:
    original_train = invert_scaler([row[number_inputs:] for row in train], scaler)
    original_test = invert_scaler([row[number_inputs:] for row in test], scaler)
else:
    original_train = [row[number_inputs:] for row in train]
    original_test = [row[number_inputs:] for row in test]


if differentiation:
    original_train = invert_difference(original_series[n_lag:], original_train)
    original_test = invert_difference(original_series[-n_test-1:], original_test)

#print(original_series[n_lag if not differentiation else n_lag+1:n_lag+10 if not differentiation else n_lag+1+10])
#print(original_train[:10])
#print(original_series[-n_test:-n_test+10])
#print(original_test[:10])

# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1])
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])


sgd = SGDRegressor()
sgd = sgd.fit(X, y)
forecasts = sgd.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test-1:], forecasts)
print(len(original_test))
print(len(forecasts))
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "Stochastic Gradient Descent Regressor model prediction", "Sample number", "Traffic volume")


svr = svm.SVR()
svr = svr.fit(X, y)
forecasts = svr.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "Support Vector Regressor model prediction", "Sample number", "Traffic volume")

lr = LinearRegression()
lr = lr.fit(X, y)
forecasts = lr.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "Linear Regression model prediction", "Sample number", "Traffic volume")

ridge = Ridge()
ridge = ridge.fit(X, y)
forecasts = lr.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "Ridge Regression model prediction", "Sample number", "Traffic volume")

lasso = Lasso()
lasso = lasso.fit(X, y)
forecasts = lasso.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "Lasso Regression model prediction", "Sample number", "Traffic volume")

elasticNet = ElasticNet()
elasticNet = elasticNet.fit(X, y)
forecasts = elasticNet.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "ElasticNet Regression model prediction", "Sample number", "Traffic volume")

rfr = RandomForestRegressor()
rfr = rfr.fit(X, y)
forecasts = rfr.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "Random Forest Regressor model prediction", "Sample number", "Traffic volume")

gbr = GradientBoostingRegressor()
gbr = gbr.fit(X, y)
forecasts = gbr.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "Gradient Boosting Regressor model prediction", "Sample number", "Traffic volume")

abr = AdaBoostRegressor()
abr = abr.fit(X, y)
forecasts = abr.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "AdaBoost Regressor model prediction", "Sample number", "Traffic volume")

xgb = XGBRegressor()
xgb = xgb.fit(X, y)
forecasts = xgb.predict(test_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "XGBoost model prediction", "Sample number", "Traffic volume")

