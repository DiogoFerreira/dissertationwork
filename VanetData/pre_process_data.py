import pandas as pd
from pandas import datetime
import matplotlib.pyplot as plt

# No data from 29/august to 13 September
# Many connections in 14/august
# Sessions with very large duration
# Sessions with negative duration


filename = "session_data.csv"

# For all files in pattern, read them and append them into an array
data = pd.read_csv(filename, sep=";", parse_dates=[0], usecols=["ts", "duration"],
                   date_parser=lambda x: datetime.strptime(x if '.' in x else x+'.0', "%Y-%m-%d %H:%M:%S.%f"))

data["duration"].fillna(0, inplace=True)

data["duration"] = data["duration"].apply(lambda duration: pd.Timedelta(seconds=duration))
data["start"] = data["ts"]
data.columns = ["start", "duration", "end"]
data["start"] = data["start"] - data["duration"]
print(data.head())
#print(data.loc[data.duration > pd.Timedelta(weeks=10)])
#print(data.loc[data.duration<pd.Timedelta(seconds=0)])
data = data[data.duration < pd.Timedelta(weeks=100)]
data = data[data.duration > pd.Timedelta(seconds=0)]
print(data["duration"].describe())
data["duration"].astype('timedelta64[h]').plot()
plt.title("Session duration")
plt.ylabel("Duration of session (hours)")
plt.show()

start_count = data["start"].value_counts()
end_count = data["end"].value_counts()
data2 = pd.concat([start_count, end_count], axis=1, keys=["start", "end"])
data2.fillna(0, inplace=True)

data2["diff"] = data2["start"] - data2["end"]
counts = data2["diff"].resample("1h").sum().fillna(0).cumsum()
counts.loc['2018-09-15':].plot()
plt.title("Number of sessions/hour since 15-09")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2018-09-18':'2018-09-30'].plot()
plt.title("Number of sessions/hour in September since 15-09")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2018-10-01':'2018-10-31'].plot()
plt.title("Number of sessions/hour in October")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2018-11-01':'2018-11-30'].plot()
plt.title("Number of sessions/hour in November")
plt.ylabel("Number of sessions/hour")
plt.show()

counts.to_csv('counts.csv', header=["values"])


