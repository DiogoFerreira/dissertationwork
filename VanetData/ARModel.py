import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat
from statsmodels.tsa.ar_model import AR
from numpy import array
from math import sqrt
from pandas import Series


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


def choose_series(df):
    return df.loc['2018-09-18':'2018-11-18']["values"]


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# fit autoregression model
def fit_ar(train, n_lag):
    model = AR(train[:, n_lag - 1])
    model_fit = model.fit()
    return model_fit


# make autoregressive forecast
def make_ar_forecasts(train, test, window, n_lag, coef):
    # walk forward over time steps in test
    history = train[len(train) - window:, n_lag - 1]
    history = [history[i] for i in range(len(history))]
    predictions = list()
    for t in range(len(test)):
        length = len(history)
        lag = [history[i] for i in range(length - window, length)]
        yhat = coef[0]
        for d in range(window):
            yhat += coef[d + 1] * lag[window - d - 1]
        obs = test[t, n_lag - 1]
        predictions.append(yhat)
        history.append(obs)
    return predictions, coef


# Invert data transform on forecasts
def invert_transform(forecasts, scaler):
    inverted = list()
    for i in range(len(forecasts)):
        # create array from forecast
        forecast = array(forecasts[i])
        forecast = forecast.reshape(1, len(forecast))
        # invert scaling
        inv_scale = scaler.inverse_transform(forecast)
        inv_scale = inv_scale[0, :]
        inverted.append(inv_scale.tolist())
    return inverted


def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test, forecasts))
    #mape = mean_absolute_percentage_error(test, forecasts)
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        #print('t+%d MAPE: %f' % (1, mape))
    return rmse


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values t+1")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


def differentiate(series):
    return Series(np.diff(series))


def invert_difference(original_series, series):
    return Series([series[i]+original_series[i] for i in range(0, len(series))])


# Parameters
file = "counts.csv"
DEBUG = False
seconds_to_group = 900
test_percentage = 0.2
n_seq = 1
n_lag = 1
differentiation = True
normalize = True


df = read_file(file)

# Analysis of traffic and choose_series
original_series = choose_series(df)
chosen_series = original_series

if differentiation:
    chosen_series = differentiate(chosen_series)

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(chosen_series.values.reshape(len(chosen_series.values), 1))
else:
    scaler = None
    scaled_values = chosen_series.values

scaled_values = scaled_values.reshape(len(scaled_values), 1)

# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values


n_test = int(test_percentage*len(supervised_values))

# split into train and test sets
train, test = supervised_values[:-n_test], supervised_values[-n_test:]

if normalize:
    original_train = invert_scaler([row[n_lag:] for row in train], scaler)
    original_test = invert_scaler([row[n_lag:] for row in test], scaler)
else:
    original_train = [row[n_lag:] for row in train]
    original_test = [row[n_lag:] for row in test]


if differentiation:
    original_train = invert_difference(original_series[n_lag:], original_train)
    original_test = invert_difference(original_series[-n_test-1:], original_test)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:n_lag], train[:, n_lag:]
X = X.reshape(X.shape[0], X.shape[1])
test_X, test_y = test[:, 0:n_lag], test[:, n_lag:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])


# Autoregression model
model_fit = fit_ar(train, n_lag)
window = model_fit.k_ar
coef = model_fit.params
if DEBUG:
    print('Lag: %s' % window)
    print('Coefficients: %s' % coef)

predictions, coef = make_ar_forecasts(train, test, window, n_lag, coef)
forecasts = [[p] for p in predictions]
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
plot_forecasts(original_series, forecasts, n_test, "Autoregression model prediction", "Sample number", "Traffic volume")

