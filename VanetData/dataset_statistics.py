import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pandas import Series
from pandas import datetime
from statsmodels.tsa.stattools import adfuller
import statsmodels.tsa.api as smt
import statsmodels.api as sm
import scipy.stats as scs
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


def choose_series(df):
    return df.loc['2018-09-18':'2018-11-20']["values"]


def test_stationarity(timeseries):

    print("Mean: " + str(timeseries.mean()))
    print("Std: " + str(timeseries.std()))

    # Determing rolling statistics
    rolmean = timeseries.rolling(window=12).mean()
    rolstd = timeseries.rolling(window=12).std()

    # Plot rolling statistics:
    orig = plt.plot(timeseries, color='blue', label='Original')
    mean = plt.plot(rolmean, color='red', label='Rolling Mean')
    std = plt.plot(rolstd, color='black', label='Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show(block=True)

    # Perform Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    values = timeseries.values.reshape(len(timeseries.values))
    dftest = adfuller(values, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic', 'p-value', '#Lags Used', 'Number of Observations Used'])
    for key, value in dftest[4].items():
        dfoutput['Critical Value (%s)' % key] = value
    print(dfoutput)
    print("Autocorrelation lag+1: "+str(timeseries.autocorr(1)))


def tsplot(y, lags=None, figsize=(10, 8), style='bmh'):
    if not isinstance(y, pd.Series):
        y = pd.Series(y)
    with plt.style.context(style):
        fig = plt.figure(figsize=figsize)
        # mpl.rcParams['font.family'] = 'Ubuntu Mono'
        layout = (3, 2)
        ts_ax = plt.subplot2grid(layout, (0, 0), colspan=2)
        acf_ax = plt.subplot2grid(layout, (1, 0))
        pacf_ax = plt.subplot2grid(layout, (1, 1))
        qq_ax = plt.subplot2grid(layout, (2, 0))
        pp_ax = plt.subplot2grid(layout, (2, 1))

        y.plot(ax=ts_ax)
        ts_ax.set_title('Time Series Analysis Plots')
        smt.graphics.plot_acf(y, lags=lags, ax=acf_ax, alpha=0.5)
        smt.graphics.plot_pacf(y, lags=lags, ax=pacf_ax, alpha=0.5)
        sm.qqplot(y, line='s', ax=qq_ax)
        qq_ax.set_title('QQ Plot')
        scs.probplot(y, sparams=(y.mean(), y.std()), plot=pp_ax)

        plt.tight_layout()
        plt.show()


def analyze_data(series):

    print("Original Data")
    test_stationarity(series)
    tsplot(series, 100)

    print("One Differentiation")
    diff_series = Series(np.diff(series))
    test_stationarity(diff_series)
    tsplot(diff_series, 100)

    print("Two Differentiations")
    diff_series = Series(np.diff(diff_series))
    test_stationarity(diff_series)
    tsplot(diff_series, 100)

    #print("Log of data")
    #log_series = Series(np.log(series))
    #test_stationarity(log_series)
    #tsplot(log_series, 100)

    #print("One differentiation of log of data")
    #diff_log_series = Series(np.diff(log_series))
    #test_stationarity(diff_log_series)
    #tsplot(diff_log_series, 100)

    print("Moving average of data")
    moving_avg = series.rolling(12).mean()
    ts_log_moving_avg_diff = series - moving_avg
    ts_log_moving_avg_diff.dropna(inplace=True)
    test_stationarity(ts_log_moving_avg_diff)
    tsplot(ts_log_moving_avg_diff, 100)

    print("Exponential weighted average of data")
    expwighted_avg = series.ewm(halflife=12).mean()
    ts_log_ewma_diff = series - expwighted_avg
    test_stationarity(ts_log_ewma_diff)
    tsplot(ts_log_ewma_diff, 100)

    print("Seasonal decomposition of data")
    decomposition = seasonal_decompose(series, freq=12)
    trend = decomposition.trend
    seasonal = decomposition.seasonal
    residual = decomposition.resid

    plt.subplot(411)
    plt.plot(series, label='Original')
    plt.legend(loc='best')
    plt.subplot(412)
    plt.plot(trend, label='Trend')
    plt.legend(loc='best')
    plt.subplot(413)
    plt.plot(seasonal, label='Seasonality')
    plt.legend(loc='best')
    plt.subplot(414)
    plt.plot(residual, label='Residuals')
    plt.legend(loc='best')
    plt.tight_layout()
    plt.show()

    ts_log_decompose = residual
    ts_log_decompose.dropna(inplace=True)
    test_stationarity(ts_log_decompose)
    tsplot(ts_log_decompose, 100)


# Parameters
file = "counts.csv"
DEBUG = True
test_percentage = 0.2

df = read_file(file)

chosen_series = choose_series(df)

raw_values = chosen_series.values.tolist()
analyze_data(chosen_series)
