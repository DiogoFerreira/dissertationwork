import glob
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt
from sklearn.metrics import mean_squared_error
from pandas import datetime


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


def choose_series(df):
    return df.loc['2018-09-18':'2018-11-18']["values"]


def make_persistence_forecast(train, test, n_steps):
    forecasts = []
    for i in range(n_steps):
        forecasts.append(train[-(i+1):] + test[:-(i+1)])
    return forecasts


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    for i in range(len(forecasts)):
        plt.plot(range(len(series)-n_test, len(series)), forecasts[i], label="Predicted values t+"+str(i+1))
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_steps, debug=True):
    for i in range(n_steps):
        predicted = forecasts[i]
        mse = mean_squared_error(test, predicted)
        rmse = sqrt(mse)
        #mape = mean_absolute_percentage_error(test, forecasts)
        if debug:
            print('t+%d RMSE: %f' % ((i + 1), rmse))
            #print('t+%d MAPE: %f' % ((i + 1), mape))
    return rmse


# Parameters
file = "counts.csv"
DEBUG = True
test_percentage = 0.2
n_steps = 1

df = read_file(file)

chosen_series = choose_series(df)

raw_values = chosen_series.values.tolist()
n_test = int(test_percentage*len(raw_values))
train, test = raw_values[:-n_test], raw_values[-n_test:]

forecasts = make_persistence_forecast(train, test, n_steps)
if DEBUG:
    plot_forecasts(chosen_series, forecasts, n_test, "Persistence model prediction", "Sample number", "Traffic volume")
evaluate_forecasts(test, forecasts, n_steps, True)
