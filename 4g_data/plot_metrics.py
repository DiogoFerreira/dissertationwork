import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_excel('DBN1_DATA_01_03_31_03.xlsx')
data["TIME_KEY"] = pd.to_datetime(data["TIME_KEY"].apply(lambda x:str(x)), format='%Y%m%d%H%M%S%f')

cellnames = data.CELLNAME.unique()

new_data = data.groupby(['TIME_KEY']).sum()
sorted_data_from_cell = new_data.sort_values("TIME_KEY")
max_connected_users = sorted_data_from_cell["M21576255"]
max_connected_users = max_connected_users.fillna(0)
max_connected_users.plot()
plt.title("Maximum connected users for all cells")
plt.ylabel("Number of users")
plt.xlabel("Timestamp")
plt.show()

new_data = data.groupby(['TIME_KEY']).sum()
sorted_data_from_cell = new_data.sort_values("TIME_KEY")
download_upload_data_volume = sorted_data_from_cell["M182496"]+sorted_data_from_cell["M182497"]
download_upload_data_volume = download_upload_data_volume.fillna(0)
download_upload_data_volume.plot()
plt.title("Upload+Download volume for all cells")
plt.ylabel("Number of users")
plt.xlabel("Timestamp")
plt.show()

new_data = data.groupby(['TIME_KEY']).sum()
sorted_data_from_cell = new_data.sort_values("TIME_KEY")
connected_users = sorted_data_from_cell["M187488"]
connected_users = connected_users.fillna(0)
connected_users.plot()
plt.title("Connected users for all cells")
plt.ylabel("Number of users")
plt.xlabel("Timestamp")
plt.show()

new_data = data.groupby(['TIME_KEY']).sum()
sorted_data_from_cell = new_data.sort_values("TIME_KEY")
avg_active_users = sorted_data_from_cell["M33606793"]
avg_active_users = avg_active_users.fillna(0)
avg_active_users.plot()
plt.title("Average Active users for all cells")
plt.ylabel("Number of users")
plt.xlabel("Timestamp")
plt.show()

new_data = data.groupby(['TIME_KEY']).sum()
sorted_data_from_cell = new_data.sort_values("TIME_KEY")
max_active_users = sorted_data_from_cell["M33606802"]
max_active_users = max_active_users.fillna(0)
max_active_users.plot()
plt.title("Maximum Active users for all cells")
plt.ylabel("Number of users")
plt.xlabel("Timestamp")
plt.show()


# Strong correlation of 0.88
print(np.corrcoef(download_upload_data_volume, max_connected_users))
# Strong correlation of 0.87
print(np.corrcoef(download_upload_data_volume, connected_users))
# Strong correlation of 0.78
print(np.corrcoef(download_upload_data_volume, avg_active_users))
# Strong correlation of 0.68
print(np.corrcoef(download_upload_data_volume, max_active_users))


download_upload_data_volume.plot()
(max_connected_users*30).plot()
plt.show()

max_connected_users.to_csv('kpi_data.csv')

for cell_name in cellnames:

	data_from_cell = data.loc[data["CELLNAME"] == cell_name]

	sorted_data_from_cell = data_from_cell.sort_values("TIME_KEY")
	sorted_data_from_cell.index = sorted_data_from_cell["TIME_KEY"]

	# connected_users = sorted_data_from_cell["M187488"]
	# connected_users = connected_users.fillna(0)
	# connected_users.plot()
	# plt.title("Connected users")
	# plt.ylabel("Number of users")
	# plt.xlabel("Timestamp")
	# plt.show()

	# average_active_users = sorted_data_from_cell["M33606793"]
	# average_active_users = average_active_users.fillna(0)
	# average_active_users.plot()
	# plt.title("Average Active users")
	# plt.ylabel("Number of users")
	# plt.xlabel("Timestamp")
	# plt.show()

	# max_active_users = sorted_data_from_cell["M33606802"]
	# max_active_users = max_active_users.fillna(0)
	# max_active_users.plot()
	# plt.title("Maximum Active users")
	# plt.ylabel("Number of users")
	# plt.xlabel("Timestamp")
	# plt.show()

	f1 = plt.figure()
	max_connected_users = sorted_data_from_cell["M21576255"]
	max_connected_users = max_connected_users.fillna(0)
	max_connected_users.plot()
	plt.title("Maximum connected users for cell " + cell_name)
	plt.ylabel("Number of users")
	plt.xlabel("Timestamp")
	#plt.show()

	f2 = plt.figure()
	download_data_volume = sorted_data_from_cell["M182496"]
	download_data_volume = download_data_volume.fillna(0)
	download_data_volume.plot()
	plt.title("Download volume for cell " + cell_name)
	plt.ylabel("Download data volume (Mb)")
	plt.xlabel("Timestamp")
#	plt.show()

	f3 = plt.figure()
	upload_data_volume = sorted_data_from_cell["M182497"]
	upload_data_volume = upload_data_volume.fillna(0)
	upload_data_volume.plot()
	plt.title("Upload volume for cell " + cell_name)
	plt.ylabel("Upload data volume (Mb)")
	plt.xlabel("Timestamp")
# 	plt.show()

