import os
import pandas as pd
from matplotlib import pyplot

for model_number in range(40):

	file = "ffnn_predictions_features_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	idxmin = predictions.loc[:, "test"].idxmin()
	rmse = predictions.loc[idxmin, "test"]

	file = "ffnn_predictions_mape_features_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	mape = predictions.loc[idxmin, "test"]

	file = "ffnn_predictions_ppm20_features_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	ppm20 = predictions.loc[idxmin, "test"]

	file = "ffnn_predictions_ppm50_features_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	ppm50 = predictions.loc[idxmin, "test"]

	file = "ffnn_predictions_ppm80_features_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	ppm80 = predictions.loc[idxmin, "test"]

	file = "ffnn_predictions_jpm20_features_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	jpm20 = predictions.loc[idxmin, "test"]

	file = "ffnn_predictions_jpm50_features_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	jpm50 = predictions.loc[idxmin, "test"]

	file = "ffnn_predictions_jpm80_features_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	jpm80 = predictions.loc[idxmin, "test"]


	filename = "aggregated_results.csv"
	if os.path.exists(filename):
	    append_write = 'a' # append if already exists
	else:
	    append_write = 'w' # make a new file if not

	with open(filename, append_write) as f:
	    f.write("%d,%f,%f,%f,%f,%f,%f,%f, %f\n" % (model_number, rmse, mape, ppm20, ppm50, ppm80, jpm20, jpm50, jpm80))


