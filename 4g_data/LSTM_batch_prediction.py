import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, LSTM
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    max_connected_users = pd.read_csv(filename, index_col=[0], header=None, names=['TIME_KEY', 'X']).T.squeeze()
    max_connected_users.index = pd.to_datetime(max_connected_users.index, format='%Y-%m-%d %H:%M:%S.%f')
    return max_connected_users


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return np.asarray([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag, 1)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    denominator = y_pred
    denominator[denominator==0] = 999999
    return np.mean(np.abs((y_true - y_pred) / denominator)) * 100


def find_week_threshold(test, start_offset=0, end_offset=0):
    start_break = ((start_offset+23)//24)*24-(start_offset)

    first_window = test[:start_break]

    if end_offset == 0:
        inner_windows = test[start_break:]
        last_window = test[:0]
    else:
        inner_windows = test[start_break:-end_offset]
        last_window = test[-end_offset:]

    if len(first_window) != 0:
        first_window_max = np.max(first_window, axis=0, keepdims=True)
    else:
        first_window_max = np.zeros((0,))

    inner_windows_max = np.max(inner_windows.reshape(len(inner_windows)//24, 24), axis=1, keepdims=True)
    if len(last_window) != 0:
        last_window_max = np.max(last_window, axis=0, keepdims=True)
    else:
        last_window_max = np.zeros((0,))

    first_window_max_values = np.repeat(first_window_max, start_break, axis=0).flatten()
    inner_window_max_values = np.repeat(inner_windows_max, 24, axis=1).flatten()
    last_window_max_values = np.repeat(last_window_max, end_offset, axis=0).flatten()

    weeks_threshold = np.hstack((first_window_max_values, inner_window_max_values, last_window_max_values))
    return weeks_threshold


def peak_performance_metric(test, forecasts, sigma=0.8, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)*sigma

    test_above_sigma = test>weeks_threshold
    test_above_sigma_indices = np.where(test_above_sigma)

    # plt.plot(test)
    # plt.plot(weeks_threshold)
    # plt.plot(test_above_sigma*test, 'bo')
    # plt.show()

    if len(test_above_sigma_indices[0])>0:
        return sqrt(mean_squared_error(test[test_above_sigma_indices], forecasts[test_above_sigma_indices]))
    else:
        return 0.0


def jump_performance_metric(test, forecasts, sigma=0.2, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)*sigma

    week_difference = np.absolute(np.diff(test, 1))
    week_difference = np.insert(week_difference, 0, 0)

    jumps_above_sigma = week_difference>weeks_threshold
    jumps_above_sigma_indices = np.where(jumps_above_sigma)

    # plt.plot(test)
    # plt.plot(weeks_threshold)
    # plt.plot(jumps_above_sigma*test, 'bo')
    # plt.show()

    if len(jumps_above_sigma_indices[0])>0:
        return sqrt(mean_squared_error(test[jumps_above_sigma_indices], forecasts[jumps_above_sigma_indices]))
    else:
        return 0.0


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True, initial_offset=0, final_offset=0):
    rmse = sqrt(mean_squared_error(test, forecasts))

    try:
        mape = mean_absolute_percentage_error(test, np.resize(forecasts, (len(forecasts),)))
    except Exception as e:
        mape = 0.0
    
    peak_pm_20 = peak_performance_metric(test, forecasts, 0.2, initial_offset, final_offset)
    peak_pm_50 = peak_performance_metric(test, forecasts, 0.5, initial_offset, final_offset)
    peak_pm_80 = peak_performance_metric(test, forecasts, 0.8, initial_offset, final_offset)
    jumps_pm_20 = jump_performance_metric(test, forecasts, 0.2, initial_offset, final_offset)
    jumps_pm_50 = jump_performance_metric(test, forecasts, 0.5, initial_offset, final_offset)
    jumps_pm_80 = jump_performance_metric(test, forecasts, 0.8, initial_offset, final_offset)

    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
        print('t+%d PPM (0.2): %f' % (1, peak_pm_20))
        print('t+%d PPM (0.5): %f' % (1, peak_pm_50))
        print('t+%d PPM (0.8): %f' % (1, peak_pm_80))
        print('t+%d JPM (0.2): %f' % (1, jumps_pm_20))
        print('t+%d JPM (0.5): %f' % (1, jumps_pm_50))
        print('t+%d JPM (0.8): %f' % (1, jumps_pm_80))
        print()

    return rmse, mape, peak_pm_20, peak_pm_50, peak_pm_80, jumps_pm_20, jumps_pm_50, jumps_pm_80


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values t+1")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


def get_model(model_number, batch_size, shape_x, shape_y, stateful):
    input_tensor = Input(batch_shape=(batch_size,shape_x,shape_y))

    if (model_number//3) % 8 == 0:
        output = LSTM(20, activation='tanh', stateful=stateful)(input_tensor)
        output = Dropout(0.2)(output)
    elif (model_number//3) % 8 == 1:
        output = LSTM(200, activation='tanh', stateful=stateful)(input_tensor)
        output = Dropout(0.2)(output)
    elif (model_number//3) % 8 == 2:
        output = LSTM(20, activation='tanh', stateful=stateful)(input_tensor)
        output = Dropout(0.2)(output)
        output = LSTM(20, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
    elif (model_number//3) % 8 == 3:
        output = LSTM(200, activation='tanh', stateful=stateful)(input_tensor)
        output = Dropout(0.2)(output)
        output = LSTM(200, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
    elif (model_number//3) % 8 == 4:
        output = LSTM(20, activation='tanh', stateful=stateful)(input_tensor)
        output = Dropout(0.2)(output)
        output = LSTM(20, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
        output = LSTM(20, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
    elif (model_number//3) % 8 == 5:
        output = LSTM(200, activation='tanh', stateful=stateful)(input_tensor)
        output = Dropout(0.2)(output)
        output = LSTM(200, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
        output = LSTM(200, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
    elif (model_number//3) % 8 == 6:
        output = LSTM(20, activation='tanh', stateful=stateful)(input_tensor)
        output = Dropout(0.2)(output)
        output = LSTM(20, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
        output = LSTM(20, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
        output = LSTM(20, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
    elif (model_number//3) % 8 == 7:
        output = LSTM(200, activation='tanh', stateful=stateful)(input_tensor)
        output = Dropout(0.2)(output)
        output = LSTM(200, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
        output = LSTM(200, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
        output = LSTM(200, activation='tanh', stateful=stateful)(output)
        output = Dropout(0.2)(output)
    
    output = Dense(y.shape[1])(output)
    model = Model(inputs=input_tensor, outputs=output)
    return model


def get_first_diff(i):
    return True, 1


def get_lag(i):
    if i%3 == 0:
        return 12
    elif i%3 == 1:
        return 24
    elif i%3 == 2:
        return 168


def get_features(i):
    return False, False, False


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


# Parameters
file = 'kpi_data.csv'
second_differentiation = False

normalize = True

# Lag for second differentiation of the time series
second_differentiation_lag = 1

# Number of next points to predict
n_seq = 1

validation_hours = 24*7

test_hours = 24*7

# Number of epochs to train the model
n_epochs = 500

# Batch size
batch_size = 1

stateful = False


for model_number in range(0, 24):
    differentiation, differentiation_lag = get_first_diff(model_number)
    n_lag = get_lag(model_number)
    week_day_feature, work_day_feature, hour_feature = get_features(model_number)

    df = read_file(file)

    # Analysis of traffic and choose_series
    series_values = df
    original_series_values = series_values

    if differentiation:
        series_values = differentiate(series_values, differentiation_lag)
        if second_differentiation:
            second_series_values = series_values
            series_values = differentiate(series_values, second_differentiation_lag)
            original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
            week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
            work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
            hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        else:
            original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
            week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
            work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag:]])
            hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
    else:
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

    if normalize:
        # rescale values
        scaler = StandardScaler()
        scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
    else:
        scaler = None
        scaled_values = series_values.values


    scaled_values = scaled_values.reshape(len(scaled_values), 1)


    # transform into supervised learning problem X, y
    supervised = series_to_supervised(scaled_values, n_lag, n_seq)
    supervised_values = supervised.values

    number_inputs = n_lag
    if work_day_feature:
        supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
        number_inputs += 1

    if week_day_feature:
        supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
        number_inputs += 7

    if hour_feature:
        supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
        number_inputs += 24

    train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)


    # reshape training into [samples, timesteps, features]
    X, y = train[:, 0:number_inputs], train[:, number_inputs:]
    X = X.reshape(X.shape[0], X.shape[1], 1)
    validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
    validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1], 1)

    # design network
    model = get_model(model_number, batch_size, X.shape[1], X.shape[2], stateful)
    model.compile(loss='mse', optimizer='adam')

    train_rmse, test_rmse = list(), list()
    train_mape, test_mape = list(), list()

    train_ppm_20, test_ppm_20 = list(), list()
    train_ppm_50, test_ppm_50 = list(), list()
    train_ppm_80, test_ppm_80 = list(), list()

    train_jpm_20, test_jpm_20 = list(), list()
    train_jpm_50, test_jpm_50 = list(), list()
    train_jpm_80, test_jpm_80 = list(), list()

    lowest_error = 9999
    best_model = None

    zero_values_train = np.where(original_train.values == 0)[0]
    zero_values_validation = np.where(original_validation.values == 0)[0]

    for i in range(n_epochs):
        print("Epoch "+str(i+1))

        history = model.fit(X, y, epochs=1, batch_size=batch_size, verbose=0, shuffle=False)
        forecasts = make_nn_forecasts(model, batch_size, train, number_inputs, n_seq)
        
        if normalize:
            forecasts = invert_scaler(forecasts, scaler)
        else:
            forecasts = np.asarray(forecasts)

        forecasts = np.resize(forecasts, (len(forecasts),))
        
        if second_differentiation and differentiation:
            forecasts = invert_difference(forecasts, second_series_values.values[n_lag:-len(test)-len(validation)], second_differentiation_lag)
            forecasts = invert_difference(forecasts, original_series_values.values[second_differentiation_lag+n_lag:-len(test)-len(validation)], differentiation_lag)
        elif differentiation:
            forecasts = invert_difference(forecasts, original_series_values.values[n_lag:-len(test)-len(validation)], differentiation_lag)
        
        
        forecasts[forecasts<0] = 0
        forecasts[zero_values_train] = 0
        error, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(original_train.values, forecasts, n_lag, n_seq, True, differentiation_lag+n_lag+12 if differentiation else n_lag+12, 18)
        train_rmse.append(error)
        train_mape.append(mape)
        train_ppm_20.append(ppm_20)
        train_ppm_50.append(ppm_50)
        train_ppm_80.append(ppm_80)
        train_jpm_20.append(jpm_20)
        train_jpm_50.append(jpm_50)
        train_jpm_80.append(jpm_80)

        forecasts = make_nn_forecasts(model, batch_size, validation, number_inputs, n_seq)
        if normalize:
            forecasts = invert_scaler(forecasts, scaler)
        else:
            forecasts = np.asarray(forecasts)

        forecasts = np.resize(forecasts, (len(forecasts),))

        if second_differentiation and differentiation:
            forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
            forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
        elif differentiation:
            forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

        forecasts[forecasts<0] = 0
        forecasts[zero_values_validation] = 0
        error, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(original_validation.values, forecasts, n_lag, n_seq, True, 4, 4)
        test_rmse.append(error)
        test_mape.append(mape)
        test_ppm_20.append(ppm_20)
        test_ppm_50.append(ppm_50)
        test_ppm_80.append(ppm_80)
        test_jpm_20.append(jpm_20)
        test_jpm_50.append(jpm_50)
        test_jpm_80.append(jpm_80)
        
        if error < lowest_error:
            lowest_error = error
            best_model = model
            best_model.save_weights("models/LSTMmodel"+str(model_number)+".h5")

    history = DataFrame()
    history_mape = DataFrame()
    history_ppm_20 = DataFrame()
    history_ppm_50 = DataFrame()
    history_ppm_80 = DataFrame()
    history_jpm_20 = DataFrame()
    history_jpm_50 = DataFrame()
    history_jpm_80 = DataFrame()

    history['train'], history['test'] = train_rmse, test_rmse
    history_mape['train'], history_mape['test'] = train_mape, test_mape
    history_ppm_20['train'], history_ppm_20['test'] = train_ppm_20, test_ppm_20
    history_ppm_50['train'], history_ppm_50['test'] = train_ppm_50, test_ppm_50
    history_ppm_80['train'], history_ppm_80['test'] = train_ppm_80, test_ppm_80
    history_jpm_20['train'], history_jpm_20['test'] = train_jpm_20, test_jpm_20
    history_jpm_50['train'], history_jpm_50['test'] = train_jpm_50, test_jpm_50
    history_jpm_80['train'], history_jpm_80['test'] = train_jpm_80, test_jpm_80


    writer = open("lstm_predictions/lstm_predictions_"+str(model_number)+".csv", 'w')
    writer.write(history.to_string()+"\n")
    writer.close()

    writer = open("lstm_predictions/lstm_predictions_mape_"+str(model_number)+".csv", 'w')
    writer.write(history_mape.to_string()+"\n")
    writer.close()

    writer = open("lstm_predictions/lstm_predictions_ppm20_"+str(model_number)+".csv", 'w')
    writer.write(history_ppm_20.to_string()+"\n")
    writer.close()

    writer = open("lstm_predictions/lstm_predictions_ppm50_"+str(model_number)+".csv", 'w')
    writer.write(history_ppm_50.to_string()+"\n")
    writer.close()

    writer = open("lstm_predictions/lstm_predictions_ppm80_"+str(model_number)+".csv", 'w')
    writer.write(history_ppm_80.to_string()+"\n")
    writer.close()

    writer = open("lstm_predictions/lstm_predictions_jpm20_"+str(model_number)+".csv", 'w')
    writer.write(history_jpm_20.to_string()+"\n")
    writer.close()

    writer = open("lstm_predictions/lstm_predictions_jpm50_"+str(model_number)+".csv", 'w')
    writer.write(history_jpm_50.to_string()+"\n")
    writer.close()

    writer = open("lstm_predictions/lstm_predictions_jpm80_"+str(model_number)+".csv", 'w')
    writer.write(history_jpm_80.to_string()+"\n")
    writer.close()

    # plt.title("RMSE error")
    # plt.plot(train_rmse, "b", label="Training error")
    # plt.plot(test_rmse, "orange", label="Test error")
    # plt.legend()
    # plt.show()

    # forecasts = make_nn_forecasts(best_model, batch_size, validation, number_inputs, n_seq)

    # if normalize:
    #     forecasts = invert_scaler(forecasts, scaler)
    # else:
    #     forecasts = np.asarray(forecasts)

    # if second_differentiation and differentiation:
    #     forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
    #     forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    # elif differentiation:
    #     forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    # forecasts[forecasts<0] = 0
    # forecasts[zero_values_validation] = 0
    # error, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(original_validation.values, forecasts, n_lag, n_seq, True, differentiation_lag+4 if differentiation else 4, 4)
    
    # plot_forecasts(original_validation, forecasts, len(forecasts), "LSTM model prediction", "Sample number", "Number of sessions per hour")



