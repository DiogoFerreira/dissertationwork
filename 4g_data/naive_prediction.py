import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from math import sqrt
from pandas import datetime
from sklearn.metrics import mean_squared_error


# Read data file
def read_file(filename):
    max_connected_users = pd.read_csv(filename, index_col=[0], header=None, names=['TIME_KEY', 'X']).T.squeeze()
    max_connected_users.index = pd.to_datetime(max_connected_users.index, format='%Y-%m-%d %H:%M:%S.%f')
    return max_connected_users


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# Make a forecast of the next value be equal to the previous one
def make_persistence_forecast(train, test, n_steps=1, n_lag=1):
    return [train[-(i+n_lag):] + test[:-(i+n_lag)]for i in range(n_steps)]


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    for i in range(len(forecasts)):
        plt.plot(range(len(series)-n_test, len(series)), forecasts[i], label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# Calculate the mean absolute precentage error
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    denominator = y_pred
    denominator[denominator==0] = 999999
    return np.mean(np.abs((y_true - y_pred) / denominator)) * 100


def find_week_threshold(test, start_offset=0, end_offset=0):
    start_break = ((start_offset+23)//24)*24-(start_offset)

    first_window = test[:start_break]

    if end_offset == 0:
        inner_windows = test[start_break:]
        last_window = test[:0]
    else:
        inner_windows = test[start_break:-end_offset]
        last_window = test[-end_offset:]

    if len(first_window) != 0:
        first_window_max = np.max(first_window, axis=0, keepdims=True)
    else:
        first_window_max = np.zeros((0,))

    inner_windows_max = np.max(inner_windows.reshape(len(inner_windows)//24, 24), axis=1, keepdims=True)
    if len(last_window) != 0:
        last_window_max = np.max(last_window, axis=0, keepdims=True)
    else:
        last_window_max = np.zeros((0,))

    first_window_max_values = np.repeat(first_window_max, start_break, axis=0).flatten()
    inner_window_max_values = np.repeat(inner_windows_max, 24, axis=1).flatten()
    last_window_max_values = np.repeat(last_window_max, end_offset, axis=0).flatten()

    weeks_threshold = np.hstack((first_window_max_values, inner_window_max_values, last_window_max_values))
    return weeks_threshold


def peak_performance_metric(test, forecasts, sigma=0.8, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)*sigma

    test_above_sigma = test>weeks_threshold
    test_above_sigma_indices = np.where(test_above_sigma)

    # plt.plot(test)
    # plt.plot(weeks_threshold)
    # plt.plot(test_above_sigma*test, 'bo')
    # plt.show()

    if len(test_above_sigma_indices[0])>0:
        return sqrt(mean_squared_error(test[test_above_sigma_indices], forecasts[test_above_sigma_indices]))
    else:
        return 0.0


def jump_performance_metric(test, forecasts, sigma=0.2, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)*sigma

    week_difference = np.absolute(np.diff(test, 1))
    week_difference = np.insert(week_difference, 0, 0)

    jumps_above_sigma = week_difference>weeks_threshold
    jumps_above_sigma_indices = np.where(jumps_above_sigma)

    # plt.plot(test)
    # plt.plot(weeks_threshold)
    # plt.plot(jumps_above_sigma*test, 'bo')
    # plt.show()

    if len(jumps_above_sigma_indices[0])>0:
        return sqrt(mean_squared_error(test[jumps_above_sigma_indices], forecasts[jumps_above_sigma_indices]))
    else:
        return 0.0


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True, initial_offset=0, final_offset=0):
    rmse = sqrt(mean_squared_error(test, forecasts))

    try:
        mape = mean_absolute_percentage_error(test, np.resize(forecasts, (len(forecasts),)))
    except Exception as e:
        mape = 0.0
    
    peak_pm_20 = peak_performance_metric(test, forecasts, 0.2, initial_offset, final_offset)
    peak_pm_50 = peak_performance_metric(test, forecasts, 0.5, initial_offset, final_offset)
    peak_pm_80 = peak_performance_metric(test, forecasts, 0.8, initial_offset, final_offset)
    jumps_pm_20 = jump_performance_metric(test, forecasts, 0.2, initial_offset, final_offset)
    jumps_pm_50 = jump_performance_metric(test, forecasts, 0.5, initial_offset, final_offset)
    jumps_pm_80 = jump_performance_metric(test, forecasts, 0.8, initial_offset, final_offset)

    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
        print('t+%d PPM (0.2): %f' % (1, peak_pm_20))
        print('t+%d PPM (0.5): %f' % (1, peak_pm_50))
        print('t+%d PPM (0.8): %f' % (1, peak_pm_80))
        print('t+%d JPM (0.2): %f' % (1, jumps_pm_20))
        print('t+%d JPM (0.5): %f' % (1, jumps_pm_50))
        print('t+%d JPM (0.8): %f' % (1, jumps_pm_80))
        print()

    return rmse, mape, peak_pm_20, peak_pm_50, peak_pm_80, jumps_pm_20, jumps_pm_50, jumps_pm_80


# Parameters
file = "kpi_data.csv"
DEBUG = True
n_steps = 1

# Number of hours to include in the validation set
validation_hours = 24*7

# Number of hours to include in the vest set
test_hours = 24*7

# Number of hours to lag from the current prediction
n_lag = 24*7

df = read_file(file)
series_values = df

train_series, validation_series, _ = train_validation_test_split(series_values, 0, validation_hours, test_hours)

train_values = train_series.values.tolist()
validation_values = validation_series.values.tolist()

forecasts = make_persistence_forecast(train_values, validation_values, n_steps, n_lag)

full_series = train_series.append(validation_series)

if DEBUG:
    plot_forecasts(full_series, forecasts, len(validation_values), "Persistence (naive) model prediction", "Sample number (one per hour)", "Number of active sessions per hour")

forecasts = np.asarray(forecasts)[0]
validation_values = np.asarray(validation_values)
error, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(validation_values, forecasts, n_lag, n_steps, True, 4, 4)

