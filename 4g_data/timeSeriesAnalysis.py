import glob
import pylab
import numpy as np
import pandas as pd
import scipy.stats as scs
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.tsa.api as smt
from pandas.plotting import lag_plot
from pandas import Series, datetime, DataFrame
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    max_connected_users = pd.read_csv(filename, index_col=[0], header=None, names=['TIME_KEY', 'X']).T.squeeze()
    max_connected_users.index = pd.to_datetime(max_connected_users.index, format='%Y-%m-%d %H:%M:%S.%f')
    return max_connected_users


def test_stationarity(timeseries, window=24):
    print("Mean: " + str(timeseries.mean()))
    print("Std: " + str(timeseries.std()))

    # Determing rolling statistics
    rolmean = timeseries.rolling(window=window).mean()
    rolstd = timeseries.rolling(window=window).std()

    # Plot rolling statistics:
    fig5 = plt.figure()
    orig = plt.plot(timeseries, color='blue', label='Original data')
    mean = plt.plot(rolmean, color='red', label='Rolling Mean')
    std = plt.plot(rolstd, color='black', label='Rolling Std')
    plt.legend(loc='best')
    plt.ylabel("Number of sessions per hour")
    plt.xlabel("Timestamp")
    plt.title('Rolling Mean & Standard Deviation of time-series data')

    # Perform Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    values = timeseries.values.reshape(len(timeseries.values))
    dftest = adfuller(values, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic', 'p-value', '#Lags Used', 'Number of Observations Used'])
    for key, value in dftest[4].items():
        dfoutput['Critical Value (%s)' % key] = value
    print(dfoutput)
    print("Autocorrelation lag+1: "+str(timeseries.autocorr(1)))
    print("Autocorrelation lag day: "+str(timeseries.autocorr(24)))
    print("Autocorrelation lag week: "+str(timeseries.autocorr(24*7)))
    print()
    print()



def tsplot(y, lags=None, figsize=(10, 8), style='bmh'):
    if not isinstance(y, pd.Series):
        y = pd.Series(y)
    with plt.style.context(style):

        smt.graphics.plot_acf(y, lags=lags, alpha=0.5)
        plt.title("Autocorrelation function")
        plt.xlabel("Number of lags (1 hour per lag)")
        plt.ylabel("Autocorrelation")
        plt.show()

        smt.graphics.plot_pacf(y, lags=lags, alpha=0.5)
        plt.title("Partial Autocorrelation function")
        plt.xlabel("Number of lags (1 hour per lag)")
        plt.ylabel("Partial Autocorrelation")
        plt.show()

        y.hist(bins = 20)
        plt.title("Time-series data histogram")
        plt.xlabel("Number of sessions per hour")
        plt.ylabel("Frequency")
        plt.show()

        
        fig = plt.figure(figsize=figsize)
        
        layout = (3, 2)
        ts_ax = plt.subplot2grid(layout, (0, 0), colspan=2)
        acf_ax = plt.subplot2grid(layout, (1, 0))
        pacf_ax = plt.subplot2grid(layout, (1, 1))
        qq_ax = plt.subplot2grid(layout, (2, 0))
        pp_ax = plt.subplot2grid(layout, (2, 1))

        y.plot(ax=ts_ax)
        ts_ax.set_title('Time Series Analysis Plots')
        smt.graphics.plot_acf(y, lags=lags, ax=acf_ax, alpha=0.5)
        smt.graphics.plot_pacf(y, lags=lags, ax=pacf_ax, alpha=0.5)
        sm.qqplot(y, line='s', ax=qq_ax)
        qq_ax.set_title('QQ Plot')
        scs.probplot(y, sparams=(y.mean(), y.std()), plot=pp_ax)

        plt.tight_layout()

        fig2 = plt.figure()
        y.hist()

        decomposition = seasonal_decompose(y, freq=168, model="additive")
        trend = decomposition.trend
        seasonal = decomposition.seasonal
        residual = decomposition.resid

        fig4 = plt.figure()
        fig4.text(0.5, 0.98, 'Seasonal decomposition with additive model and frequency of one week', ha='center')
        fig4.text(0.5, 0.02, 'Timestamp', ha='center')
        fig4.text(0.04, 0.5, 'Number of sessions per hour', va='center', rotation='vertical')
        plt.subplot(411)
        plt.plot(y, label='Original')
        plt.legend(loc='best')
        plt.subplot(412)
        plt.plot(trend, label='Trend')
        plt.legend(loc='best')
        plt.subplot(413)
        plt.plot(seasonal, label='Seasonality')
        plt.legend(loc='best')
        plt.subplot(414)
        plt.plot(residual, label='Residuals')
        plt.legend(loc='best')
        plt.tight_layout()
        
        fig3 = plt.figure()
        lag_plot(y)
        plt.show()


def analyze_data(series):
    print("Original Data")
    series.plot()
    plt.title("Maximum connected users for all cells")
    plt.xlabel("Timestamp")
    plt.ylabel("Number of users")
    plt.show()
    test_stationarity(series, window=24)
    tsplot(series, 350)



# Parameters
DEBUG = True
file = 'kpi_data.csv'
df = read_file(file)
analyze_data(df)
