import glob
import pandas as pd
from math import sqrt
from pandas import DataFrame
import matplotlib.pyplot as plt
from numpy import log, array
from pandas import Series
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, GRU
from pandas import datetime, concat
from pandas.plotting import lag_plot
from statsmodels.tsa.ar_model import AR
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from statsmodels.tsa.stattools import adfuller
from pandas.plotting import autocorrelation_plot
from statsmodels.graphics.tsaplots import plot_acf
from sklearn.neural_network import MLPRegressor


# date-time parsing function for loading the dataset
def parser(x):
    return datetime.strptime(x, "%Y/%m/%dT%H:%M:%S")

# Read data file
def read_file(file_pattern):
    # For all files in pattern, read them and append them into an array
    l = [pd.read_csv(filename, sep="|", header=[0], skipinitialspace=True, parse_dates=[0], index_col=0, date_parser=parser).dropna(axis=1, how='all') for filename in sorted(glob.glob(file_pattern))]
    # Concatenate all time frames
    df = pd.concat(l, axis=0)
    # Add the values with the same timestamp in different files
    df = df.groupby('Date').sum()

    # Convert Bytes to Mbps
    df['Bytes'] = df['Bytes'].apply(lambda x: 8*x/1000000)
    df = df.rename(columns={"Bytes": "Mbps"})
    # Convert packets to Kpps
    df['Packets'] = df['Packets'].apply(lambda x: x/1000)
    df = df.rename(columns={"Packets": "KPackets"})

    # Convert to Japan Timestamp
    df.index = df.index.tz_localize('UTC').tz_convert('Asia/Tokyo')
    return df


# Perform traffic analysis and choose time series data
# Prints the traffic and the packets with different time-frames
def perform_traffic_analysis_and_choose_series(df, debug=True):

    if debug:
        print("Total number of Megabits: " + str(df['Mbps'].sum()))
        print("Total number of packets: " + str(1000*df['KPackets'].sum()))

    five_second_lag = df.resample('5S').sum()
    five_second_lag['Mbps'] = five_second_lag['Mbps'].apply(lambda x: x/5)
    five_second_lag['KPackets'] = five_second_lag['KPackets'].apply(lambda x: x/5)

    thirty_second_lag = df.resample('30S').sum()
    thirty_second_lag['Mbps'] = thirty_second_lag['Mbps'].apply(lambda x: x/30)
    thirty_second_lag['KPackets'] = thirty_second_lag['KPackets'].apply(lambda x: x/30)

    if debug:
        mbps_plot = df.plot(y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        kpps_plot = df.plot(y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        five_second_lag.plot(ax=mbps_plot, y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        five_second_lag.plot(ax=kpps_plot, y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        thirty_second_lag.plot(ax=mbps_plot, y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        thirty_second_lag.plot(ax=kpps_plot, y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        mbps_plot.set_title("Traffic volume analysis")
        kpps_plot.set_title("Packet number")
        mbps_plot.legend(["1 second between samples", "5 seconds between samples", "30 seconds between samples"])
        kpps_plot.legend(["1 second between samples", "5 seconds between samples", "30 seconds between samples"])

        plt.show()

    return five_second_lag['Mbps']

# Check the correlation of a series
def check_correlation(series):
    fig, axes = plt.subplots(nrows=3)
    # Check correlation
    dataframe = concat([series.shift(1), series], axis=1)
    dataframe.columns = ['t-1', 't+1']
    result = dataframe.corr()
    print(result)
    autocorrelation_plot(series, ax=axes[0])
    lag_plot(series, ax=axes[1])
    plot_acf(series, lags=31, ax=axes[2])
    plt.show()


# Run a series of tests to check if the data is stationary
def stationary_tests(raw_values):

    plt.plot(raw_values)
    plt.show()

    plt.hist(raw_values)
    plt.title("Histogram")
    plt.xlabel("Traffic Volume")
    plt.ylabel("Frequency")
    plt.show()

    split = len(raw_values) // 2
    X1, X2 = raw_values[0:split], raw_values[split:]
    mean1, mean2 = X1.mean(), X2.mean()
    var1, var2 = X1.var(), X2.var()
    print('mean1=%f, mean2=%f' % (mean1, mean2))
    print('variance1=%f, variance2=%f' % (var1, var2))

    # Augmented Dickey-Fuller test
    # If p-value is <=0.05, the data is stationary
    result = adfuller(raw_values)
    print('ADF Statistic: %f' % result[0])
    print('p-value: %f' % result[1])
    print('Critical Values:')
    for key, value in result[4].items():
        print('\t%s: %.3f' % (key, value))

    if result[1]<=0.05:
        return True
    else:
        return False


# Perform log differentiation
def log_differentiate(series):
    return log(series.values)
    

# create a differenced series
def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return Series(diff)


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Transform data to be a supervised problem
def prepare_data(series, n_lag, n_seq, test_percentage, differentiate=True):

    values = series.values
    values = values.reshape(len(values), 1)
    # transform into supervised learning problem X, y
    supervised = series_to_supervised(values, n_lag, n_seq)
    supervised_values = supervised.values

    n_test = int(test_percentage*len(supervised_values))
    return 0, supervised_values, n_test


# make a persistence forecast
def persistence(last_ob, n_seq):
    return [last_ob for i in range(n_seq)]


# evaluate the persistence model
def make_persistence_forecasts(train, test, n_lag, n_seq):
    return [persistence(train[-1, 0:n_lag][-1], n_seq)]+[persistence(test[i, 0:n_lag][-1], n_seq) for i in range(len(test)-1)]


# fit autoregression model
def fit_ar(train, n_lag):
    model = AR(train[:, n_lag - 1])
    model_fit = model.fit()
    return model_fit


# make autoregressive forecast
def make_ar_forecasts(train, test, window, n_lag, coef):
    # walk forward over time steps in test
    history = train[len(train) - window:, n_lag - 1]
    history = [history[i] for i in range(len(history))]
    predictions = list()
    for t in range(len(test)):
        length = len(history)
        lag = [history[i] for i in range(length - window, length)]
        yhat = coef[0]
        for d in range(window):
            yhat += coef[d + 1] * lag[window - d - 1]
        obs = test[t, n_lag - 1]
        predictions.append(yhat)
        history.append(obs)
    return predictions, coef

def make_arima_forecasts(train, test, n_lag=5, differentiation=1, window_size=1):
    history = [x for x in train]
    predictions = list()
    for t in range(len(test)):
        model = ARIMA(history, order=(n_lag,differentiation,window_size))
        model_fit = model.fit(disp=0)
        output = model_fit.forecast()
        yhat = output[0]
        predictions.append(yhat)
        obs = test[t]
        history.append(obs)
    return predictions


# inverse data transform on forecasts
def inverse_transform(series, forecasts, scaler, n_test, differentiate):
    inverted = list()
    for i in range(len(forecasts)):
        # create array from forecast
        forecast = array(forecasts[i])
        forecast = forecast.reshape(1, len(forecast))
        # invert scaling
        inv_scale = scaler.inverse_transform(forecast)
        inv_scale = inv_scale[0, :]

        if differentiate:
            # invert differencing
            index = len(series) - n_test + i - 1
            last_ob = series.values[index]
            inv_diff = inverse_difference(last_ob, inv_scale)
            # store
            inverted.append(inv_diff)
        else:
            inverted.append(inv_scale)

    return inverted


# invert differenced forecast
def inverse_difference(last_ob, forecast):
    # invert first forecast
    inverted = list()
    inverted.append(forecast[0] + last_ob)
    # propagate difference forecast using inverted first value
    for i in range(1, len(forecast)):
        inverted.append(forecast[i] + inverted[i - 1])
    return inverted


# make one forecast with an LSTM,
def forecast_lstm(model, X, n_batch, n_lag):
    # reshape input pattern to [samples, timesteps, features]
    X = X.reshape(1, n_lag, 1)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the model
def make_lstm_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch, n_lag)
        # store the forecast
        forecasts.append(forecast)
    return forecasts

# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # store the forecast
        forecasts.append(forecast)
    return forecasts

# fit an GRU network to training data
def fit_gru(train, test, n_lag, n_seq, n_batch, nb_epoch, n_neurons, n_layers, series, scaler, differentiate, actual_test, actual_train, stateful=True):
    # reshape training into [samples, timesteps, features]
    X, y = train[:, 0:n_lag], train[:, n_lag:]
    #X = X.reshape(X.shape[0], 1, X.shape[1])
    X = X.reshape(X.shape[0], X.shape[1], 1)

    # design network
    model = Sequential()
    if n_layers == 1:
        model.add(GRU(n_neurons, batch_input_shape=(n_batch, X.shape[1], X.shape[2]), stateful=stateful))
    else:
        model.add(GRU(n_neurons, batch_input_shape=(n_batch, X.shape[1], X.shape[2]), stateful=stateful, return_sequences=True))
        for i in range(0,n_layers-2):
            model.add(GRU(n_neurons, stateful=stateful, return_sequences=True))
        model.add(GRU(n_neurons, stateful=stateful))
    #model.add(Dropout(0.2))
    model.add(Dense(y.shape[1]))
    model.compile(loss='mean_squared_error', optimizer='adam')

    # fit network
    train_rmse, test_rmse = list(), list()
    for i in range(nb_epoch):
        print("Epoch %d" % (i+1))
        model.fit(X, y, epochs=1, batch_size=n_batch, verbose=1, shuffle=False)

        if stateful:
            model.reset_states()

        # evaluate model on train data
        # make forecasts
        forecasts = make_lstm_forecasts(model, n_batch, train, n_lag, n_seq)
        # inverse transform forecasts and train
        forecasts = inverse_transform(series, forecasts, scaler, len(train) + 2, differentiate)
        # evaluate forecasts
        train_rmse.append(evaluate_forecasts(actual_train, forecasts, n_lag, n_seq, False))

        if stateful:
            model.reset_states()

        # evaluate model on test data
        forecasts = make_lstm_forecasts(model, n_batch, test, n_lag, n_seq)
        # inverse transform forecasts and test
        forecasts = inverse_transform(series, forecasts, scaler, len(test) + 2, differentiate)
        # evaluate forecasts
        test_rmse.append(evaluate_forecasts(actual_test, forecasts, n_lag, n_seq, False))
        print(train_rmse[-1])
        print(test_rmse[-1])
        if stateful:
            model.reset_states()

    history = DataFrame()
    history['train'], history['test'] = train_rmse, test_rmse
    return model, history

# fit an LSTM network to training data
def fit_lstm(train, test, n_lag, n_seq, n_batch, nb_epoch, n_neurons, n_layers, series, scaler, differentiate, actual_test, actual_train, stateful=True):
    # reshape training into [samples, timesteps, features]
    X, y = train[:, 0:n_lag], train[:, n_lag:]
    #X = X.reshape(X.shape[0], 1, X.shape[1])
    X = X.reshape(X.shape[0], X.shape[1], 1)

    # design network
    model = Sequential()
    if n_layers == 1:
        model.add(LSTM(n_neurons, batch_input_shape=(n_batch, X.shape[1], X.shape[2]), stateful=stateful))
    else:
        model.add(LSTM(n_neurons, batch_input_shape=(n_batch, X.shape[1], X.shape[2]), stateful=stateful, return_sequences=True))
        for i in range(0,n_layers-2):
            model.add(LSTM(n_neurons, stateful=stateful, return_sequences=True))
        model.add(LSTM(n_neurons, stateful=stateful))
    #model.add(Dropout(0.2))
    model.add(Dense(y.shape[1]))
    model.compile(loss='mean_squared_error', optimizer='adam')

    # fit network
    train_rmse, test_rmse = list(), list()
    for i in range(nb_epoch):
        print("Epoch %d" % (i+1))
        model.fit(X, y, epochs=1, batch_size=n_batch, verbose=1, shuffle=False)

        if stateful:
            model.reset_states()

        # evaluate model on train data
        # make forecasts
        forecasts = make_lstm_forecasts(model, n_batch, train, n_lag, n_seq)
        # evaluate forecasts
        train_rmse.append(evaluate_forecasts(actual_train, forecasts, n_lag, n_seq, False))

        #if stateful:
        #    model.reset_states()

        # evaluate model on test data
        forecasts = make_lstm_forecasts(model, n_batch, test, n_lag, n_seq)
        # evaluate forecasts
        test_rmse.append(evaluate_forecasts(actual_test, forecasts, n_lag, n_seq, False))
        print(train_rmse[-1])
        print(test_rmse[-1])
        if stateful:
            model.reset_states()

    history = DataFrame()
    history['train'], history['test'] = train_rmse, test_rmse
    return model, history

def fit_nn(train, test, n_lag, n_seq, n_batch, nb_epoch, n_neurons, n_layers, series, scaler, differentiate, actual_test, actual_train):
    # reshape training into [samples, timesteps, features]
    X, y = train[:, 0:n_lag], train[:, n_lag:]
    #X = X.reshape(X.shape[0], 1, X.shape[1])
    X = X.reshape(X.shape[0], X.shape[1])

    # design network
    model = Sequential()
    if n_layers == 1:
        model.add(Dense(n_neurons, input_dim=X.shape[1], activation='relu', kernel_initializer='normal'))
    else:
        model.add(Dense(n_neurons, input_dim=X.shape[1], activation='relu', kernel_initializer='normal'))
        for i in range(0,n_layers-1):
            model.add(Dense(n_neurons, activation='relu', kernel_initializer='normal'))
    model.add(Dense(y.shape[1], kernel_initializer='normal'))
    model.compile(loss='mean_squared_error', optimizer='adam')

    x_test, y_test = test[:, 0:n_lag], test[:, n_lag:]
    history = model.fit(X, y, epochs=nb_epoch, batch_size=n_batch, verbose=1, shuffle=False, validation_data=(x_test, y_test))
    return model, history.history

# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    for i in range(n_seq):
        actual = [row[i] for row in test]
        predicted = [forecast[i] for forecast in forecasts]
        rmse = sqrt(mean_squared_error(actual, predicted))
        if debug:
            print('t+%d RMSE: %f' % ((i + 1), rmse))
        return rmse


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test-1, len(series)-1), forecasts, label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# Parameters
n_lag = 5
n_seq = 1
test_percentage = 0.2
output_file = "output_test.csv"
file_pattern = "data/*.csv"
DEBUG = False
differentiate = True
log_differentiation = False

df = read_file(file_pattern)

# Analysis of traffic and choose_series
choosen_series = perform_traffic_analysis_and_choose_series(df, DEBUG)
raw_values = choosen_series.values

# Check correlation of data
if DEBUG:
    check_correlation(choosen_series)
    is_stationary = stationary_tests(choosen_series.values)

# Transform data to a supervised problem
scaler, supervised_values, n_test = prepare_data(choosen_series, n_lag, n_seq, test_percentage, True)


# split into train and test sets
train, test = supervised_values[0:-n_test], supervised_values[-n_test:]


n_test = len(test)
n_train = len(train)

actual_test = [row[n_lag:] for row in test]
actual_train = [row[n_lag:] for row in train]

# Fit NN model
n_epochs = 1000
n_batch = 1
n_layers = [2]
list_neurons = [[5]]

i=0
for j in range(len(n_layers)):
    layers = n_layers[j]
    writer = open(output_file+"_nn_"+str(layers), 'w')

    for n_neurons in list_neurons[j]:
        if DEBUG:
            print("Training with %d neurons" % n_neurons)

        model, history = fit_nn(train, test, n_lag, n_seq, n_batch, n_epochs, n_neurons, layers, choosen_series, scaler, differentiate, actual_test, actual_train)
        #if DEBUG:
        print(history)
        writer.write(history.to_string()+"\n")

        i = i + 1

    writer.close()

# fit lstm model
n_epochs = 1000
stateful = True
n_batch = 1
n_layers = [2]
list_neurons = [[5]]

i=0
for j in range(len(n_layers)):
    layers = n_layers[j]
    writer = open(output_file+"_"+str(layers), 'w')

    for n_neurons in list_neurons[j]:
        if DEBUG:
            print("Training with %d neurons" % n_neurons)

        model, history = fit_lstm(train, test, n_lag, n_seq, n_batch, n_epochs, n_neurons, layers, choosen_series, scaler, differentiate, actual_test, actual_train, stateful=stateful)
        #if DEBUG:
        print(history)
        writer.write(history.to_string()+"\n")

        i = i + 1

    writer.close()
