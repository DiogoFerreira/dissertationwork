import glob
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt
from sklearn.metrics import mean_squared_error
from pandas import datetime


# Read data file
def read_file(file_pattern):
    # For all files in pattern, read them and append them into an array
    l = [pd.read_csv(filename, sep="|", header=[0], skipinitialspace=True, parse_dates=[0], index_col=0, date_parser=lambda x: datetime.strptime(x, "%Y/%m/%dT%H:%M:%S")).dropna(axis=1, how='all') for filename in sorted(glob.glob(file_pattern))]
    # Concatenate all time frames
    df = pd.concat(l, axis=0)
    # Add the values with the same timestamp in different files
    df = df.groupby('Date').sum()

    # Convert Bytes to Mbps
    df['Bytes'] = df['Bytes'].apply(lambda x: 8*x/1000000)
    df = df.rename(columns={"Bytes": "Mbps"})
    # Convert packets to Kpps
    df['Packets'] = df['Packets'].apply(lambda x: x/1000)
    df = df.rename(columns={"Packets": "KPackets"})

    # Convert to Japan Timestamp
    df.index = df.index.tz_localize('UTC').tz_convert('Asia/Tokyo')
    return df


# Perform traffic analysis and choose time series data
# Prints the traffic and the packets with different time-frames
def perform_traffic_analysis_and_choose_series(df, seconds_to_group=5, debug=True):

    if debug:
        print("Total number of Megabits: " + str(df['Mbps'].sum()))
        print("Total number of packets: " + str(1000*df['KPackets'].sum()))

    new_second_lag = df.resample(str(seconds_to_group)+'S').sum()
    new_second_lag['Mbps'] = new_second_lag['Mbps'].apply(lambda x: x/seconds_to_group)
    new_second_lag['KPackets'] = new_second_lag['KPackets'].apply(lambda x: x/seconds_to_group)

    # Drop the first and last row becaus their information is incomplete
    new_second_lag.drop(new_second_lag.head(1).index, inplace=True)
    new_second_lag.drop(new_second_lag.tail(1).index, inplace=True)

    if debug:
        mbps_plot = df.plot(y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        kpps_plot = df.plot(y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        new_second_lag.plot(ax=mbps_plot, y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        new_second_lag.plot(ax=kpps_plot, y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        mbps_plot.set_title("Traffic volume analysis")
        kpps_plot.set_title("Packet number")
        mbps_plot.legend(["1 second between samples", str(seconds_to_group)+" seconds between samples"])
        kpps_plot.legend(["1 second between samples", str(seconds_to_group)+" seconds between samples"])

        plt.show()

    return new_second_lag['Mbps']


def make_persistence_forecast(train, test, n_steps):
    forecasts = []
    for i in range(n_steps):
        forecasts.append(train[-(i+1):] + test[:-(i+1)])
    return forecasts


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    for i in range(len(forecasts)):
        plt.plot(range(len(series)-n_test, len(series)), forecasts[i], label="Predicted values t+"+str(i+1))
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_steps, debug=True):
    for i in range(n_steps):
        predicted = forecasts[i]
        mse = mean_squared_error(test, predicted)
        rmse = sqrt(mse)
        mape = mean_absolute_percentage_error(test, forecasts)
        if debug:
            print('t+%d RMSE: %f' % ((i + 1), rmse))
            print('t+%d MAPE: %f' % ((i + 1), mape))
    return rmse


# Parameters
file_pattern = "data*/*.csv"
DEBUG = True
seconds_to_group = 900
test_percentage = 0.2
n_steps = 1

df = read_file(file_pattern)

# Analysis of traffic and choose_series
choosen_series = perform_traffic_analysis_and_choose_series(df, seconds_to_group, False)
raw_values = choosen_series.values.tolist()

n_test = int(test_percentage*len(raw_values))
train, test = raw_values[:-n_test], raw_values[-n_test:]

forecasts = make_persistence_forecast(train, test, n_steps)
if DEBUG:
    plot_forecasts(choosen_series, forecasts, n_test, "Persistence model prediction", "Sample number", "Traffic volume")
evaluate_forecasts(test, forecasts, n_steps, True)
