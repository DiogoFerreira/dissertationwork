import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat
from keras.models import Sequential
from keras.layers import Dense, GRU
from math import sqrt
from pandas import Series


# Read data file
def read_file(file_pattern):
    # For all files in pattern, read them and append them into an array
    l = [pd.read_csv(filename, sep="|", header=[0], skipinitialspace=True, parse_dates=[0], index_col=0, date_parser=lambda x: datetime.strptime(x, "%Y/%m/%dT%H:%M:%S")).dropna(axis=1, how='all') for filename in sorted(glob.glob(file_pattern))]
    # Concatenate all time frames
    df = pd.concat(l, axis=0)
    # Add the values with the same timestamp in different files
    df = df.groupby('Date').sum()

    # Convert Bytes to Mbps
    df['Bytes'] = df['Bytes'].apply(lambda x: 8*x/1000000)
    df = df.rename(columns={"Bytes": "Mbps"})
    # Convert packets to Kpps
    df['Packets'] = df['Packets'].apply(lambda x: x/1000)
    df = df.rename(columns={"Packets": "KPackets"})

    # Convert to Japan Timestamp
    df.index = df.index.tz_localize('UTC').tz_convert('Asia/Tokyo')
    return df


# Perform traffic analysis and choose time series data
# Prints the traffic and the packets with different time-frames
def perform_traffic_analysis_and_choose_series(df, seconds_to_group=5, debug=True):

    if debug:
        print("Total number of Megabits: " + str(df['Mbps'].sum()))
        print("Total number of packets: " + str(1000*df['KPackets'].sum()))

    new_second_lag = df.resample(str(seconds_to_group)+'S').sum()
    new_second_lag['Mbps'] = new_second_lag['Mbps'].apply(lambda x: x/seconds_to_group)
    new_second_lag['KPackets'] = new_second_lag['KPackets'].apply(lambda x: x/seconds_to_group)

    # Drop the first and last row becaus their information is incomplete
    new_second_lag.drop(new_second_lag.head(1).index, inplace=True)
    new_second_lag.drop(new_second_lag.tail(1).index, inplace=True)

    if debug:
        mbps_plot = df.plot(y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        kpps_plot = df.plot(y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        new_second_lag.plot(ax=mbps_plot, y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        new_second_lag.plot(ax=kpps_plot, y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        mbps_plot.set_title("Traffic volume analysis")
        kpps_plot.set_title("Packet number")
        mbps_plot.legend(["1 second between samples", str(seconds_to_group)+" seconds between samples"])
        kpps_plot.legend(["1 second between samples", str(seconds_to_group)+" seconds between samples"])

        plt.show()

    return new_second_lag['Mbps']


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# evaluate the model
def make_gru_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_gru(model, X, n_batch, n_lag)
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# make one forecast with a GRU
def forecast_gru(model, X, n_batch, n_lag):
    # reshape input pattern to [samples, timesteps, features]
    X = X.reshape(1, n_lag, 1)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test, forecasts))
    mape = mean_absolute_percentage_error(test, forecasts)
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values t+1")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


def differentiate(series):
    return Series(np.diff(series))


def invert_difference(original_series, series):
    return Series([series[i]+original_series[i] for i in range(0, len(series))])


# Parameters
file_pattern = "data*/*.csv"
DEBUG = True
seconds_to_group = 900
test_percentage = 0.2
n_seq = 1
n_lag = 10
stateful = True
output_file = "GRU"
n_neurons = 200
n_layers = 2
n_batch = 1
n_epochs = 100
differentiation = True
normalize = True
number_of_tests = 5

df = read_file(file_pattern)

# Analysis of traffic and choose_series
original_series = perform_traffic_analysis_and_choose_series(df, seconds_to_group, False)
chosen_series = original_series

if differentiation:
    chosen_series = differentiate(chosen_series)

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(chosen_series.values.reshape(len(chosen_series.values), 1))
else:
    scaler = None
    scaled_values = chosen_series.values

scaled_values = scaled_values.reshape(len(scaled_values), 1)

# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values


n_test = int(test_percentage*len(supervised_values))

# split into train and test sets
train, test = supervised_values[:-n_test], supervised_values[-n_test:]

if normalize:
    original_train = invert_scaler([row[n_lag:] for row in train], scaler)
    original_test = invert_scaler([row[n_lag:] for row in test], scaler)
else:
    original_train = [row[n_lag:] for row in train]
    original_test = [row[n_lag:] for row in test]


if differentiation:
    original_train = invert_difference(original_series[n_lag:], original_train)
    original_test = invert_difference(original_series[-n_test-1:], original_test)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:n_lag], train[:, n_lag:]
X = X.reshape(X.shape[0], X.shape[1], 1)
test_X, test_y = test[:, 0:n_lag], test[:, n_lag:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1], 1)


lowest_error = 9999

# fit network
for test_number in range(0, number_of_tests):
    # design network
    model = Sequential()
    if n_layers == 1:
        model.add(GRU(n_neurons, batch_input_shape=(n_batch, X.shape[1], X.shape[2]), stateful=stateful))
    else:
        model.add(GRU(n_neurons, batch_input_shape=(n_batch, X.shape[1], X.shape[2]), return_sequences=True, stateful=stateful))
        for i in range(0,n_layers-2):
            model.add(GRU(n_neurons, return_sequences=True, stateful=True))
        model.add(GRU(n_neurons))
    model.add(Dense(y.shape[1]))

    model.compile(loss='mse', optimizer='adam')

    # fit network
    train_rmse, test_rmse = list(), list()
    for i in range(n_epochs):
        print(i)
        if (i + 1) % 100 == 0:
            print(i+1)
        history = model.fit(X, y, epochs=1, batch_size=n_batch, verbose=0, shuffle=False)

        if stateful:
            model.reset_states()

        forecasts = make_gru_forecasts(model, n_batch, train, n_lag, n_seq)
        if normalize:
            forecasts = invert_scaler(forecasts, scaler)
        else:
            forecasts = np.asarray(forecasts)
        if differentiation:
            forecasts = invert_difference(original_series[n_lag:], forecasts)
        error = evaluate_forecasts(original_train, forecasts, n_lag, n_seq, False)
        train_rmse.append(error)

        forecasts = make_gru_forecasts(model, n_batch, test, n_lag, n_seq)
        if normalize:
            forecasts = invert_scaler(forecasts, scaler)
        else:
            forecasts = np.asarray(forecasts)
        if differentiation:
            forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
        error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, False)
        test_rmse.append(error)

        if stateful:
            model.reset_states()

        if error < lowest_error:
            lowest_error = error

    history = DataFrame()
    history['train'], history['test'] = train_rmse, test_rmse
    writer = open("new_outputs/GRU/" + output_file + "_n_layers_" + str(n_layers) + "_neurons_" + str(n_neurons) +
                  "_differentiation_" + str(differentiation) + "_stateful_" +
                  str(stateful) + "_iteration_" + str(test_number) + "_lag_" + str(n_lag) + ".csv", 'w')
    writer.write(history.to_string() + "\n")
    writer.close()

    if DEBUG:
        if test_number == 0:
            plt.plot(train_rmse, "b", label="Training error")
            plt.plot(test_rmse, "orange", label="Test error")
        else:
            plt.plot(train_rmse, "b")
            plt.plot(test_rmse, "orange")

if DEBUG:
    plt.title("RMSE error")
    plt.legend()
    plt.show()

print(lowest_error)


forecasts = make_gru_forecasts(model, n_batch, train, n_lag, n_seq)

forecasts = make_gru_forecasts(model, n_batch, test, n_lag, n_seq)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)
if differentiation:
    forecasts = invert_difference(original_series[-n_test - 1:], forecasts)
error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)

if DEBUG:
    plot_forecasts(original_series, forecasts, n_test, "Stateful GRU model prediction", "Sample number", "Traffic volume")

