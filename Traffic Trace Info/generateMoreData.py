import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pandas import Series
from pandas import datetime
from statsmodels.tsa.stattools import adfuller
import statsmodels.tsa.api as smt
import statsmodels.api as sm
import scipy.stats as scs
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(file_pattern):
    # For all files in pattern, read them and append them into an array
    l = [pd.read_csv(filename, sep="|", header=[0], skipinitialspace=True, parse_dates=[0], index_col=0, date_parser=lambda x: datetime.strptime(x, "%Y/%m/%dT%H:%M:%S")).dropna(axis=1, how='all') for filename in sorted(glob.glob(file_pattern))]
    # Concatenate all time frames
    df = pd.concat(l, axis=0)
    # Add the values with the same timestamp in different files
    df = df.groupby('Date').sum()

    # Convert Bytes to Mbps
    df['Bytes'] = df['Bytes'].apply(lambda x: 8*x/1000000)
    df = df.rename(columns={"Bytes": "Mbps"})
    # Convert packets to Kpps
    df['Packets'] = df['Packets'].apply(lambda x: x/1000)
    df = df.rename(columns={"Packets": "KPackets"})

    df = df.resample('1S').sum()

    df.drop(df.tail(900).index, inplace=True)

    return df



def generateData(df):
    new_data = df.copy()
    df_copy = df.copy()
    
    for i in range(1, 2):
        indexes = df_copy.index
        df_copy.index = df.index.map(lambda x: x+pd.DateOffset(days=2*i))
        new_data = pd.concat([new_data, df_copy])

    return new_data

    
def perform_traffic_analysis_and_choose_series(df, seconds_to_group=5, debug=True):

    new_second_lag = df.resample(str(seconds_to_group)+'S').sum()
    new_second_lag['Mbps'] = new_second_lag['Mbps'].apply(lambda x: x/seconds_to_group)
    new_second_lag.drop(new_second_lag.tail(1).index, inplace=True)

    if debug:
        mbps_plot = new_second_lag.plot(y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)

        mbps_plot.set_title("Traffic volume analysis")
        mbps_plot.legend([str(seconds_to_group)+" seconds between samples"])

        plt.show()

    return new_second_lag['Mbps']

# Parameters
file_pattern = "data*/*.csv"

df = read_file(file_pattern)
df = generateData(df)
df = perform_traffic_analysis_and_choose_series(df, 900)
df.to_csv('generated_data.csv', header=["Mbps"])