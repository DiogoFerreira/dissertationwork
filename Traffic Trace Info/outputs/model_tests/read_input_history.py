import pandas as pd
from matplotlib import pyplot

lstm = pd.read_csv("stateful_lstm.csv", skipinitialspace=True, sep=" ")
stateless_lstm = pd.read_csv("stateless_lstm.csv", skipinitialspace=True, sep=" ")
nn = pd.read_csv("nn.csv", skipinitialspace=True, sep=" ")
gru = pd.read_csv("gru.csv", skipinitialspace=True, sep=" ")


fig, axes = pyplot.subplots(nrows=2, ncols=2)

axes[0, 0].plot(lstm.loc[:, "test"], color='blue', label='Test data')
axes[0, 0].plot(lstm.loc[:, "train"], color='orange', label='Training data')
axes[0, 0].set_title("Stateful LSTM")
axes[0, 0].legend()
axes[0, 0].set_ylim(60, 68)

axes[0, 1].plot(stateless_lstm.loc[:, "test"], color='blue', label='Test data')
axes[0, 1].plot(stateless_lstm.loc[:, "train"], color='orange', label='Training data')
axes[0, 1].set_title("Stateless LSTM")
axes[0, 1].legend()
axes[0, 1].set_ylim(60, 68)

axes[1, 0].plot(nn.loc[:, "test"], color='blue', label='Test data')
axes[1, 0].plot(nn.loc[:, "train"], color='orange', label='Training data')
axes[1, 0].set_title("Feed-forward Neural Network")
axes[1, 0].legend()
axes[1, 0].set_ylim(60, 68)

axes[1, 1].plot(gru.loc[:, "test"], color='blue', label='Test data')
axes[1, 1].plot(gru.loc[:, "train"], color='orange', label='Training data')
axes[1, 1].set_title("Gated Recurrent Unit")
axes[1, 1].legend()
axes[1, 1].set_ylim(60, 68)


fig.text(0.5, 0.04, 'Number of epochs', ha='center')
fig.text(0.04, 0.5, 'Root Mean Squared Error', va='center', rotation='vertical')
pyplot.show()


pyplot.plot(lstm.loc[:, "test"], color='blue', label='Stateful LSTM')
pyplot.plot(stateless_lstm.loc[:, "test"], color='orange', label='Stateless LSTM')
pyplot.plot(nn.loc[:, "test"], color='green', label='Feed-forward Neural Network')
pyplot.plot(gru.loc[:, "test"], color='red', label='Gated Recurrent Unit')
pyplot.title("Test data error with different models")
pyplot.legend()
pyplot.show()
