import pandas as pd
from matplotlib import pyplot

twenty_five_neurons = pd.read_csv("twenty_five_neurons.csv", skipinitialspace=True, sep=" ")
fifty_neurons = pd.read_csv("fifty_neurons.csv", skipinitialspace=True, sep=" ")
one_hundred_neurons = pd.read_csv("one_hundred_neurons.csv", skipinitialspace=True, sep=" ")
#two_hundred_neurons = pd.read_csv("two_hundred_neurons.csv", skipinitialspace=True, sep=" ")


fig, axes = pyplot.subplots(nrows=2, ncols=2)

axes[0, 0].plot(twenty_five_neurons.loc[:, "test"], color='blue', label='Test data')
axes[0, 0].plot(twenty_five_neurons.loc[:, "train"], color='orange', label='Training data')
axes[0, 0].set_title("25 neuron")
axes[0, 0].legend()
axes[0, 0].set_ylim(62, 70)

axes[0, 1].plot(fifty_neurons.loc[:, "test"], color='blue', label='Test data')
axes[0, 1].plot(fifty_neurons.loc[:, "train"], color='orange', label='Training data')
axes[0, 1].set_title("50 neurons")
axes[0, 1].legend()
axes[0, 1].set_ylim(62, 70)

axes[1, 0].plot(one_hundred_neurons.loc[:, "test"], color='blue', label='Test data')
axes[1, 0].plot(one_hundred_neurons.loc[:, "train"], color='orange', label='Training data')
axes[1, 0].set_title("100 neurons")
axes[1, 0].legend()
axes[1, 0].set_ylim(62, 70)

#axes[1, 1].plot(two_hundred_neurons.loc[:, "test"], color='blue', label='Test data')
#axes[1, 1].plot(two_hundred_neurons.loc[:, "train"], color='orange', label='Training data')
#axes[1, 1].set_title("200 neurons")
#axes[1, 1].legend()
#axes[1, 1].set_ylim(62, 68)

fig.text(0.5, 0.04, 'Number of epochs', ha='center')
fig.text(0.04, 0.5, 'Root Mean Squared Error', va='center', rotation='vertical')
pyplot.show()
