import pandas as pd
from matplotlib import pyplot

one_neuron = pd.read_csv("one_neuron.csv", skipinitialspace=True, sep=" ")
two_neurons = pd.read_csv("two_neurons.csv", skipinitialspace=True, sep=" ")
five_neurons = pd.read_csv("five_neurons.csv", skipinitialspace=True, sep=" ")
ten_neurons = pd.read_csv("ten_neurons.csv", skipinitialspace=True, sep=" ")
twenty_neurons = pd.read_csv("twenty_neurons.csv", skipinitialspace=True, sep=" ")


fig, axes = pyplot.subplots(nrows=3, ncols=2)

axes[0, 0].plot(one_neuron.loc[:, "test"], color='blue', label='Test data')
axes[0, 0].plot(one_neuron.loc[:, "train"], color='orange', label='Training data')
axes[0, 0].set_title("1 neuron")
axes[0, 0].legend()
axes[0, 0].set_ylim(64, 66.5)

axes[0, 1].plot(two_neurons.loc[:, "test"], color='blue', label='Test data')
axes[0, 1].plot(two_neurons.loc[:, "train"], color='orange', label='Training data')
axes[0, 1].set_title("2 neurons")
axes[0, 1].legend()
axes[0, 1].set_ylim(62, 68)

axes[1, 0].plot(five_neurons.loc[:, "test"], color='blue', label='Test data')
axes[1, 0].plot(five_neurons.loc[:, "train"], color='orange', label='Training data')
axes[1, 0].set_title("5 neurons")
axes[1, 0].legend()
axes[1, 0].set_ylim(62, 67)

axes[1, 1].plot(ten_neurons.loc[:, "test"], color='blue', label='Test data')
axes[1, 1].plot(ten_neurons.loc[:, "train"], color='orange', label='Training data')
axes[1, 1].set_title("10 neurons")
axes[1, 1].legend()
axes[1, 1].set_ylim(62, 68)

axes[2, 0].plot(twenty_neurons.loc[:, "test"], color='blue', label='Test data')
axes[2, 0].plot(twenty_neurons.loc[:, "train"], color='orange', label='Training data')
axes[2, 0].set_title("20 neurons")
axes[2, 0].legend()
axes[2, 0].set_ylim(62, 68)

fig.text(0.5, 0.04, 'Number of epochs', ha='center')
fig.text(0.04, 0.5, 'Root Mean Squared Error', va='center', rotation='vertical')
pyplot.show()
