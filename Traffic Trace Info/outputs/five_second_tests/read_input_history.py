import pandas as pd
from matplotlib import pyplot

nn = pd.read_csv("nn.csv", skipinitialspace=True, sep=" ")


fig, axes = pyplot.subplots(nrows=1, ncols=1)

axes.plot(nn.loc[:, "test"], color='blue', label='Test data')
axes.plot(nn.loc[:, "train"], color='orange', label='Training data')
axes.set_title("Neural Network")
axes.legend()
fig.text(0.5, 0.04, 'Number of epochs', ha='center')
fig.text(0.04, 0.5, 'Root Mean Squared Error', va='center', rotation='vertical')
pyplot.show()

