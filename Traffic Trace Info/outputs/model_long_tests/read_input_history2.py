import pandas as pd
from matplotlib import pyplot

lstm = pd.read_csv("lstm_tests_2.csv", skipinitialspace=True, sep=" ")
nn = pd.read_csv("nn_tests_2.csv", skipinitialspace=True, sep=" ")


fig, axes = pyplot.subplots(nrows=1, ncols=2)

axes[0].plot(lstm.loc[:, "test"], color='blue', label='Test data')
axes[0].plot(lstm.loc[:, "train"], color='orange', label='Training data')
axes[0].set_title("LSTM")
axes[0].legend()
axes[0].set_ylim(55, 70)

axes[1].plot(nn.loc[:, "test"], color='blue', label='Test data')
axes[1].plot(nn.loc[:, "train"], color='orange', label='Training data')
axes[1].set_title("Feed-forward Neural Network")
axes[1].legend()
axes[1].set_ylim(55, 70)


fig.text(0.5, 0.04, 'Number of epochs', ha='center')
fig.text(0.04, 0.5, 'Root Mean Squared Error', va='center', rotation='vertical')
pyplot.show()


pyplot.plot(lstm.loc[:, "test"], color='blue', label='LSTM')
pyplot.plot(nn.loc[:, "test"], color='green', label='Feed-forward Neural Network')
pyplot.ylim(60, 68)
pyplot.title("Test data error with different models")
pyplot.xlabel("Number of epochs")
pyplot.legend()
pyplot.show()
