import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pandas import Series
from pandas import datetime
from statsmodels.tsa.stattools import adfuller
import statsmodels.tsa.api as smt
import statsmodels.api as sm
import scipy.stats as scs
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(file_pattern):
    # For all files in pattern, read them and append them into an array
    l = [pd.read_csv(filename, sep="|", header=[0], skipinitialspace=True, parse_dates=[0], index_col=0, date_parser=lambda x: datetime.strptime(x, "%Y/%m/%dT%H:%M:%S")).dropna(axis=1, how='all') for filename in sorted(glob.glob(file_pattern))]
    # Concatenate all time frames
    df = pd.concat(l, axis=0)
    # Add the values with the same timestamp in different files
    df = df.groupby('Date').sum()

    # Convert Bytes to Mbps
    df['Bytes'] = df['Bytes'].apply(lambda x: 8*x/1000000)
    df = df.rename(columns={"Bytes": "Mbps"})
    # Convert packets to Kpps
    df['Packets'] = df['Packets'].apply(lambda x: x/1000)
    df = df.rename(columns={"Packets": "KPackets"})

    # Convert to Japan Timestamp
    df.index = df.index.tz_localize('UTC').tz_convert('Asia/Tokyo')
    return df


# Perform traffic analysis and choose time series data
# Prints the traffic and the packets with different time-frames
def perform_traffic_analysis_and_choose_series(df, seconds_to_group=5, debug=True):

    if debug:
        print("Total number of Megabits: " + str(df['Mbps'].sum()))
        print("Total number of packets: " + str(1000*df['KPackets'].sum()))

    new_second_lag = df.resample(str(seconds_to_group)+'S').sum()
    new_second_lag['Mbps'] = new_second_lag['Mbps'].apply(lambda x: x/seconds_to_group)
    new_second_lag['KPackets'] = new_second_lag['KPackets'].apply(lambda x: x/seconds_to_group)

    # Drop the first and last row becaus their information is incomplete
    new_second_lag.drop(new_second_lag.head(1).index, inplace=True)
    new_second_lag.drop(new_second_lag.tail(1).index, inplace=True)

    if debug:
        mbps_plot = df.plot(y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        kpps_plot = df.plot(y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        new_second_lag.plot(ax=mbps_plot, y='Mbps')
        plt.ylabel("Traffic (Mbps)")
        plt.ylim(ymin=0)
        new_second_lag.plot(ax=kpps_plot, y='KPackets')
        plt.ylabel("Number of packets (Kpps)")
        plt.ylim(ymin=0)

        mbps_plot.set_title("Traffic volume analysis")
        kpps_plot.set_title("Packet number")
        mbps_plot.legend(["1 second between samples", str(seconds_to_group)+" seconds between samples"])
        kpps_plot.legend(["1 second between samples", str(seconds_to_group)+" seconds between samples"])

        plt.show()

    return new_second_lag['Mbps']


def test_stationarity(timeseries):

    print("Mean: " + str(timeseries.mean()))
    print("Std: " + str(timeseries.std()))

    # Determing rolling statistics
    rolmean = timeseries.rolling(window=12).mean()
    rolstd = timeseries.rolling(window=12).std()

    # Plot rolling statistics:
    orig = plt.plot(timeseries, color='blue', label='Original')
    mean = plt.plot(rolmean, color='red', label='Rolling Mean')
    std = plt.plot(rolstd, color='black', label='Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show(block=True)

    # Perform Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    values = timeseries.values.reshape(len(timeseries.values))
    dftest = adfuller(values, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic', 'p-value', '#Lags Used', 'Number of Observations Used'])
    for key, value in dftest[4].items():
        dfoutput['Critical Value (%s)' % key] = value
    print(dfoutput)
    print("Autocorrelation lag+1: "+str(timeseries.autocorr(1)))


def tsplot(y, lags=None, figsize=(10, 8), style='bmh'):
    if not isinstance(y, pd.Series):
        y = pd.Series(y)
    with plt.style.context(style):
        fig = plt.figure(figsize=figsize)
        # mpl.rcParams['font.family'] = 'Ubuntu Mono'
        layout = (3, 2)
        ts_ax = plt.subplot2grid(layout, (0, 0), colspan=2)
        acf_ax = plt.subplot2grid(layout, (1, 0))
        pacf_ax = plt.subplot2grid(layout, (1, 1))
        qq_ax = plt.subplot2grid(layout, (2, 0))
        pp_ax = plt.subplot2grid(layout, (2, 1))

        y.plot(ax=ts_ax)
        ts_ax.set_title('Time Series Analysis Plots')
        smt.graphics.plot_acf(y, lags=lags, ax=acf_ax, alpha=0.5)
        smt.graphics.plot_pacf(y, lags=lags, ax=pacf_ax, alpha=0.5)
        sm.qqplot(y, line='s', ax=qq_ax)
        qq_ax.set_title('QQ Plot')
        scs.probplot(y, sparams=(y.mean(), y.std()), plot=pp_ax)

        plt.tight_layout()
        plt.show()


def analyze_data(series):

    print("Original Data")
    test_stationarity(series)
    tsplot(series, 100)

    print("One Differentiation")
    diff_series = Series(np.diff(series))
    test_stationarity(diff_series)
    tsplot(diff_series, 100)

    print("Two Differentiations")
    diff_series = Series(np.diff(diff_series))
    test_stationarity(diff_series)
    tsplot(diff_series, 100)

    print("Log of data")
    log_series = Series(np.log(series))
    test_stationarity(log_series)
    tsplot(log_series, 100)

    print("One differentiation of log of data")
    diff_log_series = Series(np.diff(log_series))
    test_stationarity(diff_log_series)
    tsplot(diff_log_series, 100)

    print("Moving average of log data")
    moving_avg = log_series.rolling(12).mean()
    ts_log_moving_avg_diff = log_series - moving_avg
    ts_log_moving_avg_diff.dropna(inplace=True)
    test_stationarity(ts_log_moving_avg_diff)
    tsplot(ts_log_moving_avg_diff, 100)

    print("Exponential weighted average of log data")
    expwighted_avg = log_series.ewm(halflife=12).mean()
    ts_log_ewma_diff = log_series - expwighted_avg
    test_stationarity(ts_log_ewma_diff)
    tsplot(ts_log_ewma_diff, 100)

    print("Seasonal decomposition of log data")
    decomposition = seasonal_decompose(log_series, freq=12)
    trend = decomposition.trend
    seasonal = decomposition.seasonal
    residual = decomposition.resid

    plt.subplot(411)
    plt.plot(log_series, label='Original')
    plt.legend(loc='best')
    plt.subplot(412)
    plt.plot(trend, label='Trend')
    plt.legend(loc='best')
    plt.subplot(413)
    plt.plot(seasonal, label='Seasonality')
    plt.legend(loc='best')
    plt.subplot(414)
    plt.plot(residual, label='Residuals')
    plt.legend(loc='best')
    plt.tight_layout()
    plt.show()

    ts_log_decompose = residual
    ts_log_decompose.dropna(inplace=True)
    test_stationarity(ts_log_decompose)
    tsplot(ts_log_decompose, 100)




# Parameters
file_pattern = "data*/*.csv"
seconds_to_group = 900

df = read_file(file_pattern)

# Analysis of traffic and choose_series
choosen_series = perform_traffic_analysis_and_choose_series(df, seconds_to_group, True)
raw_values = choosen_series.values.tolist()
analyze_data(choosen_series)