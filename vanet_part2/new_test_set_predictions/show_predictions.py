import pandas as pd
from matplotlib import pyplot

file = "ffnn_predictions_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()

file = "ffnn_predictions_mape_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "ffnn_predictions_ppm20_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "ffnn_predictions_ppm50_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "ffnn_predictions_ppm80_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "ffnn_predictions_jpm20_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "ffnn_predictions_jpm50_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "ffnn_predictions_jpm80_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])