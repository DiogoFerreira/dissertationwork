from matplotlib import pyplot
import pandas as pd

file = "lstm_predictions_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
idx = predictions.loc[:, "test"].idxmin()
print(idx)
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("RMSE value along the number of trained epochs")
pyplot.legend()
pyplot.xlabel("Number of epochs")
pyplot.ylabel("RMSE (number of sessions per hour)")
pyplot.show()

file = "lstm_predictions_mape_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[idx,"test"])

file = "lstm_predictions_ppm20_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[idx,"test"])

file = "lstm_predictions_ppm50_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[idx,"test"])

file = "lstm_predictions_ppm80_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[idx,"test"])

file = "lstm_predictions_jpm20_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[idx,"test"])

file = "lstm_predictions_jpm50_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[idx,"test"])

file = "lstm_predictions_jpm80_diff_1.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[idx,"test"])