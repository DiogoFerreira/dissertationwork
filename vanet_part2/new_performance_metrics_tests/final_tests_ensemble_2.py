import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, LSTM
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return np.asarray([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# make one forecast with a NN,
def forecast_lstm(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag, 1)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]
    

# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# evaluate the model
def make_lstm_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


def find_week_threshold(test, start_offset=0, end_offset=0):
    start_break = ((start_offset+23)//24)*24-(start_offset)

    first_window = test[:start_break]

    if end_offset == 0:
        inner_windows = test[start_break:]
        last_window = test[:0]
    else:
        inner_windows = test[start_break:-end_offset]
        last_window = test[-end_offset:]

    if len(first_window) != 0:
        first_window_max = np.max(first_window, axis=0, keepdims=True)
    else:
        first_window_max = np.zeros((0,))

    inner_windows_max = np.max(inner_windows.reshape(len(inner_windows)//24, 24), axis=1, keepdims=True)
    if len(last_window) != 0:
        last_window_max = np.max(last_window, axis=0, keepdims=True)
    else:
        last_window_max = np.zeros((0,))

    first_window_max_values = np.repeat(first_window_max, start_break, axis=0).flatten()
    inner_window_max_values = np.repeat(inner_windows_max, 24, axis=1).flatten()
    last_window_max_values = np.repeat(last_window_max, end_offset, axis=0).flatten()

    weeks_threshold = np.hstack((first_window_max_values, inner_window_max_values, last_window_max_values))
    return weeks_threshold


def peak_performance_metric(test, forecasts, sigma=0.8, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)*sigma

    test_above_sigma = test>weeks_threshold
    test_above_sigma_indices = np.where(test_above_sigma)

    if len(test_above_sigma_indices[0])>0:
        return sqrt(mean_squared_error(test[test_above_sigma_indices], forecasts[test_above_sigma_indices]))
    else:
        return 0.0


def jump_performance_metric(test, forecasts, sigma=0.2, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)*sigma

    week_difference = np.absolute(np.diff(test, 1))
    week_difference = np.insert(week_difference, 0, 0)

    jumps_above_sigma = week_difference>weeks_threshold
    jumps_above_sigma_indices = np.where(jumps_above_sigma)

    if len(jumps_above_sigma_indices[0])>0:
        return sqrt(mean_squared_error(test[jumps_above_sigma_indices], forecasts[jumps_above_sigma_indices]))
    else:
        return 0.0

def plot_performance_metrics(test, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)
    weeks_threshold_02 = weeks_threshold*0.2
    weeks_threshold_05 = weeks_threshold*0.5
    weeks_threshold_08 = weeks_threshold*0.8

    test_above_sigma_02 = test>weeks_threshold_02
    test_above_sigma_02_indices = np.where(test_above_sigma_02)


    test_above_sigma_05 = test>weeks_threshold_05
    test_above_sigma_05_indices = np.where(test_above_sigma_05)

    test_above_sigma_08 = test>weeks_threshold_08
    test_above_sigma_08_indices = np.where(test_above_sigma_08)

    plt.plot(test)
    plt.plot(weeks_threshold_02, label="Threshold value (sigma=0.2)", color="green")
    plt.plot(weeks_threshold_05, label="Threshold value (sigma=0.5)", color="orange")
    plt.plot(weeks_threshold_08, label="Threshold value (sigma=0.8)", color="red")
    y_values_masked_02 = np.ma.masked_where(test_above_sigma_02*test <= 0 , test_above_sigma_02*test)
    plt.plot(y_values_masked_02, 'o', color="green")
    y_values_masked_05 = np.ma.masked_where(test_above_sigma_05*test <= 0 , test_above_sigma_05*test)
    plt.plot(y_values_masked_05, 'o', color="orange")
    y_values_masked_08 = np.ma.masked_where(test_above_sigma_08*test <= 0 , test_above_sigma_08*test)
    plt.plot(y_values_masked_08, 'o', color="red")
    plt.title("Observations above the threshold value")
    plt.xlabel("Observation number")
    plt.ylabel("Number of sessions per hour")
    plt.legend()
    plt.show()

    week_difference = np.absolute(np.diff(test, 1))
    week_difference = np.insert(week_difference, 0, 0)

    jumps_above_sigma_02 = week_difference>weeks_threshold_02
    jumps_above_sigma_05 = week_difference>weeks_threshold_05
    jumps_above_sigma_08 = week_difference>weeks_threshold_08

    plt.plot(test)
    plt.plot(weeks_threshold_02, label="Threshold value (sigma=0.2)", color="green")
    plt.plot(weeks_threshold_05, label="Threshold value (sigma=0.5)", color="orange")
    plt.plot(weeks_threshold_08, label="Threshold value (sigma=0.8)", color="red")
    y_values_masked_02 = np.ma.masked_where(jumps_above_sigma_02*test <= 0 , jumps_above_sigma_02*test)
    plt.plot(y_values_masked_02, 'o', color="green")
    y_values_masked_05 = np.ma.masked_where(jumps_above_sigma_05*test <= 0 , jumps_above_sigma_05*test)
    plt.plot(y_values_masked_05, 'o', color="orange")
    y_values_masked_08 = np.ma.masked_where(jumps_above_sigma_08*test <= 0 , jumps_above_sigma_08*test)
    plt.plot(y_values_masked_08, 'o', color="red")
    plt.title("Absolute difference between consecutive observations above the threshold value")
    plt.xlabel("Observation number")
    plt.ylabel("Number of sessions per hour")
    plt.legend()
    plt.show()





# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True, initial_offset=0, final_offset=0):
    rmse = sqrt(mean_squared_error(test.values, forecasts))

    try:
        mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    except Exception as e:
        mape = 0.0
    
    peak_pm_20 = peak_performance_metric(test.values, forecasts, 0.2, initial_offset, final_offset)
    peak_pm_50 = peak_performance_metric(test.values, forecasts, 0.5, initial_offset, final_offset)
    peak_pm_80 = peak_performance_metric(test.values, forecasts, 0.8, initial_offset, final_offset)
    jumps_pm_20 = jump_performance_metric(test.values, forecasts, 0.2, initial_offset, final_offset)
    jumps_pm_50 = jump_performance_metric(test.values, forecasts, 0.5, initial_offset, final_offset)
    jumps_pm_80 = jump_performance_metric(test.values, forecasts, 0.8, initial_offset, final_offset)

    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
        print('t+%d PPM (0.2): %f' % (1, peak_pm_20))
        print('t+%d PPM (0.5): %f' % (1, peak_pm_50))
        print('t+%d PPM (0.8): %f' % (1, peak_pm_80))
        print('t+%d JPM (0.2): %f' % (1, jumps_pm_20))
        print('t+%d JPM (0.5): %f' % (1, jumps_pm_50))
        print('t+%d JPM (0.8): %f' % (1, jumps_pm_80))
        print()

    return rmse, mape, peak_pm_20, peak_pm_50, peak_pm_80, jumps_pm_20, jumps_pm_50, jumps_pm_80


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.ylim(0, 750)
    plt.xlim(1650, 1995)
    plt.subplots_adjust(left=0.04, right=0.99, top=0.97, bottom=0.05)
    plt.show()


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded



def get_max_values(values):
    return np.array([np.amax(value) for value in values])


def get_min_values(values):
    return np.array([np.amin(value) for value in values])


def get_average_values(values):
    return np.array([np.average(value) for value in values])


def get_std_values(values):
    return np.array([np.std(value) for value in values])


def get_above_mean_values(values):
    return np.array([np.where(value > np.average(value))[0].size for value in values])


def get_below_mean_values(values):
    return np.array([np.where(value < np.average(value))[0].size for value in values])


def get_median_values(values):
    return np.array([np.median(value) for value in values])


def get_quantile_values(values, a):
    return np.array([np.quantile(value, a) for value in values])


def get_sum_values(values):
    return np.array([np.sum(value) for value in values])


def get_range_count_values(values, min, max):
    return np.array([np.sum((value >= min) & (value < max)) for value in values])


def get_index_of_max_values(values):
    return np.array([np.argmax(value) for value in values])


def get_index_of_min_values(values):
    return np.array([np.argmin(value) for value in values])


# Parameters
file = "../counts.csv"
differentiation = True
second_differentiation = False

normalize = True

# Lag to differentiate the time series
differentiation_lag = 1

# Lag for second differentiation of the time series
second_differentiation_lag = 1

# Number of points available to predict the next ones
n_lag = 12

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2

# Number of epochs to train the model
n_epochs = 500

# Batch size
batch_size = 1

model_number = 3


df = read_file(file)

# Analysis of traffic and choose_series
series_values = get_series_values(df)
original_series_values = series_values

if differentiation:
    series_values = differentiate(series_values, differentiation_lag)
    if second_differentiation:
        second_series_values = series_values
        series_values = differentiate(series_values, second_differentiation_lag)
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
    else:
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
else:
    original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
    week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
    hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
else:
    scaler = None
    scaled_values = series_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)


# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values

work_day_feature = False
week_day_feature = True
hour_feature = False

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    number_inputs += 24

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1])
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

# design network
input_tensor = Input(shape=(X.shape[1],))
output = Dense(200, activation='relu')(input_tensor)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(y.shape[1])(output)
model = Model(inputs=input_tensor, outputs=output)
model.compile(loss='mean_squared_error', optimizer='adam')

model.load_weights('../models/new_loss_func_tests/FFNNmodelWeights_diff_1_08.h5')
#model.load_weights('../models/final_models/FFNNmodelWeights_diff_1_08_with_metrics.h5')

forecasts = make_nn_forecasts(model, batch_size, test, number_inputs, n_seq)


if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)


if second_differentiation and differentiation:
    forecasts = invert_difference(forecasts, second_series_values.values[second_differentiation_lag-len(test):], second_differentiation_lag)
    forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)
elif differentiation:
    forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)


forecasts[forecasts<0] = 0

#plot_performance_metrics(original_test.values)
print("FFNN prediction")
error, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True, 0, 0)

#plot_forecasts(original_series_values, forecasts, len(validation), "Feed-Forward NN model prediction", "Sample number", "Number of sessions per hour")


n_lag = 168
series_values = original_series_values
original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)

# transform into supervised learning problem X, y
supervised = series_to_supervised(series_values.values.reshape(len(series_values), 1), n_lag, n_seq)
supervised_values = supervised.values

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)

week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

week_max_value = get_max_values(supervised_values[:, :n_lag])
week_min_value = get_min_values(supervised_values[:, :n_lag])
week_average_value = get_average_values(supervised_values[:, :n_lag])
week_std_value = get_std_values(supervised_values[:, :n_lag])
week_above_mean_value = get_above_mean_values(supervised_values[:, :n_lag])
week_below_mean_value = get_below_mean_values(supervised_values[:, :n_lag])
week_median_value = get_median_values(supervised_values[:, :n_lag])
week_sum_value = get_sum_values(supervised_values[:, :n_lag])
week_quantile_value_25 = get_quantile_values(supervised_values[:, :n_lag], 0.25)
week_quantile_value_50 = get_quantile_values(supervised_values[:, :n_lag], 0.50)
week_quantile_value_75 = get_quantile_values(supervised_values[:, :n_lag], 0.75)
week_range_0_100_value = get_range_count_values(supervised_values[:, :n_lag], 0, 100)
week_range_100_300_value = get_range_count_values(supervised_values[:, :n_lag], 100, 300)
week_range_300_600_value = get_range_count_values(supervised_values[:, :n_lag], 300, 600)
week_range_600_700_value = get_range_count_values(supervised_values[:, :n_lag], 600, 700)
week_range_700_9999_value = get_range_count_values(supervised_values[:, :n_lag], 700, 9999)
week_index_max_values = get_index_of_max_values(supervised_values[:, :n_lag])
week_index_min_values = get_index_of_min_values(supervised_values[:, :n_lag])
last_week_value = supervised_values[:, n_lag-168]


day_max_value = get_max_values(supervised_values[:, n_lag-24:n_lag])
day_min_value = get_min_values(supervised_values[:, n_lag-24:n_lag])
day_average_value = get_average_values(supervised_values[:, n_lag-24:n_lag])
day_std_value = get_std_values(supervised_values[:, n_lag-24:n_lag])
day_above_mean_value = get_above_mean_values(supervised_values[:, n_lag-24:n_lag])
day_below_mean_value = get_below_mean_values(supervised_values[:, n_lag-24:n_lag])
day_median_value = get_median_values(supervised_values[:, n_lag-24:n_lag])
day_sum_value = get_sum_values(supervised_values[:, n_lag-24:n_lag])
day_quantile_value_25 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.25)
day_quantile_value_50 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.50)
day_quantile_value_75 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.75)
day_range_0_100_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 0, 100)
day_range_100_300_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 100, 300)
day_range_300_600_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 300, 600)
day_range_600_700_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 600, 700)
day_range_700_9999_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 700, 9999)
day_index_max_values = get_index_of_max_values(supervised_values[:, n_lag-24:n_lag])
day_index_min_values = get_index_of_min_values(supervised_values[:, n_lag-24:n_lag])
last_day_value = supervised_values[:, n_lag-24]

last_hour_value = supervised_values[:, n_lag-1]

features = np.c_ [last_hour_value, last_day_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
        week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value, day_quantile_value_25, day_quantile_value_50, day_quantile_value_75,
        week_quantile_value_25, week_quantile_value_50, week_quantile_value_75, day_sum_value, week_sum_value, 
        week_index_max_values, week_index_min_values, day_index_min_values, day_index_max_values]

number_inputs = features.shape[1]

if normalize:
    # rescale values
    scaler2 = StandardScaler()
    scaled_features = scaler2.fit_transform(features)
    scaled_labels = scaler2.fit_transform(supervised_values[:, -n_seq:])
else:
    scaler2 = None
    scaled_features = features
    scaled_labels = supervised_values[:, -n_seq:]


train, validation, test = train_validation_test_split(np.c_ [scaled_features, scaled_labels], 0, validation_hours, test_hours)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1])
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

rmse = 1000
while rmse>30:
    rfr = RandomForestRegressor()
    rfr = rfr.fit(X, y)
    forecasts_features = rfr.predict(test_X)
    forecasts_features = forecasts_features.reshape(len(forecasts_features), 1)
    if normalize:
        forecasts_features = invert_scaler(forecasts_features, scaler2)
    else:
        forecasts_features = np.asarray(forecasts_features)

    forecasts_features[forecasts_features<0] = 0

    rmse, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(original_test, forecasts_features, n_lag, n_seq, False, 0, 0)
    #plot_forecasts(original_series_values[:-len(test)], forecasts_features, len(validation), "Random Forest Regressor model prediction", "Sample number", "Number of sessions per hour")

print("Random Forest prediction")
rmse, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(original_test, forecasts_features, n_lag, n_seq, True, 0, 0)


series_values = original_series_values
n_lag = 12

original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
else:
    scaler = None
    scaled_values = series_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)

supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values

work_day_feature = True
week_day_feature = False
hour_feature = False

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    number_inputs += 24

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)

# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1], 1)
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1], 1)
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1], 1)

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

# design network
input_tensor = Input(shape=(X.shape[1],X.shape[2]))
x = LSTM(200, activation='tanh', recurrent_dropout=0.2)(input_tensor)
x = Dropout(0.2)(x)
output = Dense(y.shape[1])(x)
model = Model(inputs=input_tensor, outputs=output)
model.compile(loss='mean_squared_error', optimizer='adam')

model.load_weights('../models/new_loss_func_tests/LSTM_modelWeights_08.h5')
#model.load_weights('../models/final_models/LSTM_modelWeights_08_with_metrics.h5')

forecasts_lstm = make_lstm_forecasts(model, batch_size, test, number_inputs, n_seq)

if normalize:
    forecasts_lstm = invert_scaler(forecasts_lstm, scaler)
else:
    forecasts_lstm = np.asarray(forecasts_lstm)

forecasts_lstm[forecasts_lstm<0] = 0

print("LSTM prediction")
evaluate_forecasts(original_test, forecasts_lstm, number_inputs, n_seq, True)

plot_forecasts(original_series_values, forecasts_lstm, len(validation), "Recurrent NN model prediction", "Sample number", "Number of sessions per hour")


two_forecasts = (forecasts+forecasts_features)/2

print("Ensemble of two models")
evaluate_forecasts(original_test, two_forecasts, n_lag, n_seq, True)
plot_forecasts(original_series_values, two_forecasts, len(test), "Ensemble voting of two best models prediction", "Sample number", "Number of sessions per hour")


two_forecasts = (forecasts+forecasts_lstm)/2

print("Ensemble of two network models")
evaluate_forecasts(original_test, two_forecasts, n_lag, n_seq, True)
plot_forecasts(original_series_values, two_forecasts, len(test), "Ensemble voting of two models prediction", "Sample number", "Number of sessions per hour")


all_forecasts = (forecasts+forecasts_features+forecasts_lstm)/3

print("Ensemble of three models")
evaluate_forecasts(original_test, all_forecasts, n_lag, n_seq, True)
plot_forecasts(original_series_values, all_forecasts, len(test), "Ensemble voting of three best models prediction", "Sample number", "Number of sessions per hour")

weeks_threshold = find_week_threshold(original_test.values, 0, 0)*0.8
boost_forecast = []
for i in range(len(weeks_threshold)):
    if original_test[i]>weeks_threshold[i]:
        boost_forecast.append((forecasts_lstm.item(i)+forecasts.item(i))/2)
    else:
        boost_forecast.append(forecasts_features.item(i))

boost_forecast = np.array(boost_forecast)
evaluate_forecasts(original_test, boost_forecast, n_lag, n_seq, True)
plot_forecasts(original_series_values, boost_forecast, len(test), "Boosting forecast prediction", "Sample number", "Number of sessions per hour")


