import os
import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from tsfresh import extract_features
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.linear_model import LinearRegression, SGDRegressor, Ridge, Lasso, ElasticNet
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, AdaBoostRegressor
from xgboost import XGBRegressor


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return Series([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, debug=True):
    rmse = sqrt(mean_squared_error(test.values, forecasts))
    mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse, mape


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


def get_max_values(values):
    return np.array([np.amax(value) for value in values])


def get_min_values(values):
    return np.array([np.amin(value) for value in values])


def get_average_values(values):
    return np.array([np.average(value) for value in values])


def get_std_values(values):
    return np.array([np.std(value) for value in values])


def get_above_mean_values(values):
    return np.array([np.where(value > np.average(value))[0].size for value in values])


def get_below_mean_values(values):
    return np.array([np.where(value < np.average(value))[0].size for value in values])


def get_median_values(values):
    return np.array([np.median(value) for value in values])


def get_quantile_values(values, a):
    return np.array([np.quantile(value, a) for value in values])


def get_sum_values(values):
    return np.array([np.sum(value) for value in values])


def get_range_count_values(values, min, max):
    return np.array([np.sum((value >= min) & (value < max)) for value in values])


def get_index_of_max_values(values):
    return np.array([np.argmax(value) for value in values])


def get_index_of_min_values(values):
    return np.array([np.argmin(value) for value in values])


# Parameters
file = "counts.csv"

normalize = True

# Number of points available to predict the next ones
n_lag = 168

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2

n_epochs = 500

batch_size = 1


df = read_file(file)

series_values = get_series_values(df)
original_series_values = series_values

original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)

supervised = series_to_supervised(series_values.values.reshape(len(series_values), 1), n_lag, n_seq)
supervised_values = supervised.values
print(supervised_values.shape)
week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

week_max_value = get_max_values(supervised_values[:, :n_lag])
week_min_value = get_min_values(supervised_values[:, :n_lag])
week_average_value = get_average_values(supervised_values[:, :n_lag])
week_std_value = get_std_values(supervised_values[:, :n_lag])
week_above_mean_value = get_above_mean_values(supervised_values[:, :n_lag])
week_below_mean_value = get_below_mean_values(supervised_values[:, :n_lag])
week_median_value = get_median_values(supervised_values[:, :n_lag])
week_sum_value = get_sum_values(supervised_values[:, :n_lag])
week_quantile_value_25 = get_quantile_values(supervised_values[:, :n_lag], 0.25)
week_quantile_value_50 = get_quantile_values(supervised_values[:, :n_lag], 0.50)
week_quantile_value_75 = get_quantile_values(supervised_values[:, :n_lag], 0.75)
week_range_0_100_value = get_range_count_values(supervised_values[:, :n_lag], 0, 100)
week_range_100_300_value = get_range_count_values(supervised_values[:, :n_lag], 100, 300)
week_range_300_600_value = get_range_count_values(supervised_values[:, :n_lag], 300, 600)
week_range_600_700_value = get_range_count_values(supervised_values[:, :n_lag], 600, 700)
week_range_700_9999_value = get_range_count_values(supervised_values[:, :n_lag], 700, 9999)
week_index_max_values = get_index_of_max_values(supervised_values[:, :n_lag])
week_index_min_values = get_index_of_min_values(supervised_values[:, :n_lag])
last_week_value = supervised_values[:, n_lag-168]


day_max_value = get_max_values(supervised_values[:, n_lag-24:n_lag])
day_min_value = get_min_values(supervised_values[:, n_lag-24:n_lag])
day_average_value = get_average_values(supervised_values[:, n_lag-24:n_lag])
day_std_value = get_std_values(supervised_values[:, n_lag-24:n_lag])
day_above_mean_value = get_above_mean_values(supervised_values[:, n_lag-24:n_lag])
day_below_mean_value = get_below_mean_values(supervised_values[:, n_lag-24:n_lag])
day_median_value = get_median_values(supervised_values[:, n_lag-24:n_lag])
day_sum_value = get_sum_values(supervised_values[:, n_lag-24:n_lag])
day_quantile_value_25 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.25)
day_quantile_value_50 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.50)
day_quantile_value_75 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.75)
day_range_0_100_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 0, 100)
day_range_100_300_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 100, 300)
day_range_300_600_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 300, 600)
day_range_600_700_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 600, 700)
day_range_700_9999_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 700, 9999)
day_index_max_values = get_index_of_max_values(supervised_values[:, n_lag-24:n_lag])
day_index_min_values = get_index_of_min_values(supervised_values[:, n_lag-24:n_lag])
last_day_value = supervised_values[:, n_lag-24]

last_hour_value = supervised_values[:, n_lag-1]

for model_number in range(5,6):


    if model_number == 0:
        features = np.c_ [last_hour_value, last_day_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
        week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value, day_quantile_value_25, day_quantile_value_50, day_quantile_value_75,
        week_quantile_value_25, week_quantile_value_50, week_quantile_value_75, day_sum_value, week_sum_value, day_range_0_100_value, day_range_100_300_value,
        day_range_300_600_value, day_range_600_700_value, day_range_700_9999_value, week_range_0_100_value, week_range_100_300_value, week_range_300_600_value,
        week_range_600_700_value, week_range_700_9999_value, week_index_max_values, week_index_min_values, day_index_min_values, day_index_max_values]
    elif model_number == 1:
        features = np.c_ [last_hour_value, last_day_value, last_week_value, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
        week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value, day_quantile_value_25, day_quantile_value_50, day_quantile_value_75,
        week_quantile_value_25, week_quantile_value_50, week_quantile_value_75, day_sum_value, week_sum_value, day_range_0_100_value, day_range_100_300_value,
        day_range_300_600_value, day_range_600_700_value, day_range_700_9999_value, week_range_0_100_value, week_range_100_300_value, week_range_300_600_value,
        week_range_600_700_value, week_range_700_9999_value, week_index_max_values, week_index_min_values, day_index_min_values, day_index_max_values]
    elif model_number == 2:
        features = np.c_ [last_hour_value, last_day_value, week_day_array, work_day_array, hour_array, day_max_value, day_min_value,
        day_average_value, day_std_value, day_above_mean_value, day_below_mean_value, day_median_value, day_quantile_value_25, day_quantile_value_50,
        day_quantile_value_75, day_sum_value, day_range_0_100_value, day_range_100_300_value,day_range_300_600_value, day_range_600_700_value,
        day_range_700_9999_value, day_index_min_values, day_index_max_values]
    elif model_number == 3:
        features = np.c_ [last_hour_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, week_std_value, 
        week_above_mean_value, week_below_mean_value,  week_median_value, 
        week_quantile_value_25, week_quantile_value_50, week_quantile_value_75, week_sum_value, 
        week_range_0_100_value, week_range_100_300_value, week_range_300_600_value,
        week_range_600_700_value, week_range_700_9999_value, week_index_max_values, week_index_min_values]
    elif model_number == 4:
        features = np.c_ [week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
        week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value, day_quantile_value_25, day_quantile_value_50, day_quantile_value_75,
        week_quantile_value_25, week_quantile_value_50, week_quantile_value_75, day_sum_value, week_sum_value, day_range_0_100_value, day_range_100_300_value,
        day_range_300_600_value, day_range_600_700_value, day_range_700_9999_value, week_range_0_100_value, week_range_100_300_value, week_range_300_600_value,
        week_range_600_700_value, week_range_700_9999_value, week_index_max_values, week_index_min_values, day_index_min_values, day_index_max_values]
    elif model_number == 5:
        features = np.c_ [last_hour_value, last_day_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
        week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value,
        day_sum_value, week_sum_value, day_range_0_100_value, day_range_100_300_value,
        day_range_300_600_value, day_range_600_700_value, day_range_700_9999_value, week_range_0_100_value, week_range_100_300_value, week_range_300_600_value,
        week_range_600_700_value, week_range_700_9999_value, week_index_max_values, week_index_min_values, day_index_min_values, day_index_max_values]
    elif model_number == 6:
        features = np.c_ [last_hour_value, last_day_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
        week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value, day_quantile_value_25, day_quantile_value_50, day_quantile_value_75,
        week_quantile_value_25, week_quantile_value_50, week_quantile_value_75, day_sum_value, week_sum_value, 
        week_index_max_values, week_index_min_values, day_index_min_values, day_index_max_values]
    elif model_number == 7:
        features = np.c_ [last_hour_value, last_day_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
        week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value, day_quantile_value_25, day_quantile_value_50, day_quantile_value_75,
        week_quantile_value_25, week_quantile_value_50, week_quantile_value_75, day_sum_value, week_sum_value, day_range_0_100_value, day_range_100_300_value,
        day_range_300_600_value, day_range_600_700_value, day_range_700_9999_value, week_range_0_100_value, week_range_100_300_value, week_range_300_600_value,
        week_range_600_700_value, week_range_700_9999_value]
    elif model_number == 8:
        features = np.c_ [last_hour_value, last_day_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
        week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value,
        week_median_value, day_median_value, day_sum_value, week_sum_value]


    number_inputs = features.shape[1]

    if normalize:
        # rescale values
        scaler = StandardScaler()
        scaled_features = scaler.fit_transform(features)
        scaled_labels = scaler.fit_transform(supervised_values[:, -n_seq:])
    else:
        scaler = None
        scaled_features = features
        scaled_labels = supervised_values[:, -n_seq:]


    train, validation, test = train_validation_test_split(np.c_ [scaled_features, scaled_labels], 0, validation_hours, test_hours)


    # reshape training into [samples, timesteps, features]
    X, y = train[:, 0:number_inputs], train[:, number_inputs:]
    X = X.reshape(X.shape[0], X.shape[1])
    validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
    validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])


    # input_tensor = Input(shape=(X.shape[1],))
    # output = Dense(200, activation='relu')(input_tensor)
    # output = Dropout(0.2)(output)
    # output = Dense(200, activation='relu')(output)
    # output = Dropout(0.2)(output)
    # output = Dense(200, activation='relu')(output)
    # output = Dropout(0.2)(output)
    # output = Dense(200, activation='relu')(output)
    # output = Dropout(0.2)(output)
    # output = Dense(y.shape[1])(output)
    # model = Model(inputs=input_tensor, outputs=output)
    # model.compile(loss='mean_squared_error', optimizer='adam')

    # train_rmse, test_rmse = list(), list()
    # train_mape, test_mape = list(), list()
    # lowest_error = 9999
    # best_model = None

    # for i in range(n_epochs):
    #     print("Epoch "+str(i+1))

    #     history = model.fit(X, y, epochs=1, batch_size=batch_size, verbose=0, shuffle=False)
    #     forecasts = make_nn_forecasts(model, batch_size, train, number_inputs, n_seq)
        
    #     if normalize:
    #         forecasts = invert_scaler(forecasts, scaler)
    #     else:
    #         forecasts = np.asarray(forecasts)

    #     forecasts = np.resize(forecasts, (len(forecasts),))
            
    #     forecasts[forecasts<0] = 0
    #     error, mape = evaluate_forecasts(original_train, forecasts)
    #     train_rmse.append(error)
    #     train_mape.append(mape)
        
    #     forecasts = make_nn_forecasts(model, batch_size, validation, number_inputs, n_seq)
    #     if normalize:
    #         forecasts = invert_scaler(forecasts, scaler)
    #     else:
    #         forecasts = np.asarray(forecasts)

    #     forecasts = np.resize(forecasts, (len(forecasts),))

    #     forecasts[forecasts<0] = 0
    #     error, mape = evaluate_forecasts(original_validation, forecasts)
    #     test_rmse.append(error)
    #     test_mape.append(mape)
        
    #     if error < lowest_error:
    #         lowest_error = error
    #         best_model = model
    #         best_model.save_weights("models/feature_tests/FFNN_model_weights"+str(model_number)+".h5")

    # history = DataFrame()
    # history['train'], history['test'] = train_rmse, test_rmse
    # writer = open("feature_predictions/ffnn_predictions"+str(model_number)+".csv", 'w')
    # writer.write(history.to_string()+"\n")
    # writer.close()

    # history_mape = DataFrame()
    # history_mape['train'], history_mape['test'] = train_mape, test_mape
    # writer = open("feature_predictions/ffnn_predictions_mape_"+str(model_number)+".csv", 'w')
    # writer.write(history_mape.to_string()+"\n")
    # writer.close()


    print(str(model_number)+"\n")

    sgd = SGDRegressor()
    sgd = sgd.fit(X, y)
    forecasts = sgd.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)

    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)


    svr = svm.SVR()
    svr = svr.fit(X, y)
    forecasts = svr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)


    lr = LinearRegression()
    lr = lr.fit(X, y)
    forecasts = lr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)


    ridge = Ridge()
    ridge = ridge.fit(X, y)
    forecasts = lr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)


    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)


    lasso = Lasso()
    lasso = lasso.fit(X, y)
    forecasts = lasso.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)


    elasticNet = ElasticNet()
    elasticNet = elasticNet.fit(X, y)
    forecasts = elasticNet.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)


    rfr = RandomForestRegressor()
    rfr = rfr.fit(X, y)
    forecasts = rfr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)
    if rmse<40:
        plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Random Forest Regressor model prediction", "Sample number", "Number of sessions")


    gbr = GradientBoostingRegressor()
    gbr = gbr.fit(X, y)
    forecasts = gbr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)


    abr = AdaBoostRegressor()
    abr = abr.fit(X, y)
    forecasts = abr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)


    xgb = XGBRegressor()
    xgb = xgb.fit(X, y)
    forecasts = xgb.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts)

