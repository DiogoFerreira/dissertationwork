import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, LSTM
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.ensemble import RandomForestRegressor


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"], df.loc['2018-12-12 08:00:00':'2019-01-11 13:00:00']["values"]


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return np.asarray([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_test_split(values, training_hours, test_hours):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# make one forecast with a NN,
def forecast_lstm(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag, 1)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# evaluate the model
def make_lstm_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


def find_week_threshold(test, start_offset=0, end_offset=0):
    start_break = ((start_offset+23)//24)*24-(start_offset)

    first_window = test[:start_break]

    if end_offset == 0:
        inner_windows = test[start_break:]
        last_window = test[:0]
    else:
        inner_windows = test[start_break:-end_offset]
        last_window = test[-end_offset:]


    first_window_max = np.max(first_window, axis=0, keepdims=True)
    inner_windows_max = np.max(inner_windows.reshape(len(inner_windows)//24, 24), axis=1, keepdims=True)
    if len(last_window) != 0:
        last_window_max = np.max(last_window, axis=0, keepdims=True)
    else:
        last_window_max = np.zeros((0,))

    first_window_max_values = np.repeat(first_window_max, start_break, axis=0).flatten()
    inner_window_max_values = np.repeat(inner_windows_max, 24, axis=1).flatten()
    last_window_max_values = np.repeat(last_window_max, end_offset, axis=0).flatten()

    weeks_threshold = np.hstack((first_window_max_values, inner_window_max_values, last_window_max_values))
    return weeks_threshold

def plot_performance_metrics(test, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)
    weeks_threshold_02 = weeks_threshold*0.2
    weeks_threshold_05 = weeks_threshold*0.5
    weeks_threshold_08 = weeks_threshold*0.8

    test_above_sigma_02 = test>weeks_threshold_02
    test_above_sigma_02_indices = np.where(test_above_sigma_02)


    test_above_sigma_05 = test>weeks_threshold_05
    test_above_sigma_05_indices = np.where(test_above_sigma_05)

    test_above_sigma_08 = test>weeks_threshold_08
    test_above_sigma_08_indices = np.where(test_above_sigma_08)

    plt.plot(test)
    plt.plot(weeks_threshold_02, label="Threshold value (sigma=0.2)", color="green")
    plt.plot(weeks_threshold_05, label="Threshold value (sigma=0.5)", color="orange")
    plt.plot(weeks_threshold_08, label="Threshold value (sigma=0.8)", color="red")
    y_values_masked_02 = np.ma.masked_where(test_above_sigma_02*test <= 0 , test_above_sigma_02*test)
    plt.plot(y_values_masked_02, 'o', color="green")
    y_values_masked_05 = np.ma.masked_where(test_above_sigma_05*test <= 0 , test_above_sigma_05*test)
    plt.plot(y_values_masked_05, 'o', color="orange")
    y_values_masked_08 = np.ma.masked_where(test_above_sigma_08*test <= 0 , test_above_sigma_08*test)
    plt.plot(y_values_masked_08, 'o', color="red")
    plt.title("Observations above the threshold value")
    plt.xlabel("Observation number")
    plt.ylabel("Number of sessions per hour")
    plt.legend()
    plt.show()

    week_difference = np.absolute(np.diff(test, 1))
    week_difference = np.insert(week_difference, 0, 0)

    jumps_above_sigma_02 = week_difference>weeks_threshold_02
    jumps_above_sigma_05 = week_difference>weeks_threshold_05
    jumps_above_sigma_08 = week_difference>weeks_threshold_08

    plt.plot(test)
    plt.plot(weeks_threshold_02, label="Threshold value (sigma=0.2)", color="green")
    plt.plot(weeks_threshold_05, label="Threshold value (sigma=0.5)", color="orange")
    plt.plot(weeks_threshold_08, label="Threshold value (sigma=0.8)", color="red")
    y_values_masked_02 = np.ma.masked_where(jumps_above_sigma_02*test <= 0 , jumps_above_sigma_02*test)
    plt.plot(y_values_masked_02, 'o', color="green")
    y_values_masked_05 = np.ma.masked_where(jumps_above_sigma_05*test <= 0 , jumps_above_sigma_05*test)
    plt.plot(y_values_masked_05, 'o', color="orange")
    y_values_masked_08 = np.ma.masked_where(jumps_above_sigma_08*test <= 0 , jumps_above_sigma_08*test)
    plt.plot(y_values_masked_08, 'o', color="red")
    plt.title("Absolute difference between consecutive observations above the threshold value")
    plt.xlabel("Observation number")
    plt.ylabel("Number of sessions per hour")
    plt.legend()
    plt.show()


def peak_performance_metric(test, forecasts, sigma=0.8, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)*sigma

    test_above_sigma = test>weeks_threshold
    test_above_sigma_indices = np.where(test_above_sigma)

    # plt.plot(test)
    # plt.plot(weeks_threshold)
    # plt.plot(test_above_sigma*test, 'bo')
    # plt.show()

    if len(test_above_sigma_indices[0])>0:
        return sqrt(mean_squared_error(test[test_above_sigma_indices], forecasts[test_above_sigma_indices]))
    else:
        return 0.0


def jump_performance_metric(test, forecasts, sigma=0.2, start_offset=0, end_offset=0):
    weeks_threshold = find_week_threshold(test, start_offset, end_offset)*sigma

    week_difference = np.absolute(np.diff(test, 1))
    week_difference = np.insert(week_difference, 0, 0)

    jumps_above_sigma = week_difference>weeks_threshold
    jumps_above_sigma_indices = np.where(jumps_above_sigma)

    # plt.plot(test)
    # plt.plot(weeks_threshold)
    # plt.plot(jumps_above_sigma*test, 'bo')
    # plt.show()

    if len(jumps_above_sigma_indices[0])>0:
        return sqrt(mean_squared_error(test[jumps_above_sigma_indices], forecasts[jumps_above_sigma_indices]))
    else:
        return 0.0


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True, initial_offset=0, final_offset=0):
    rmse = sqrt(mean_squared_error(test.values, forecasts))

    try:
        mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    except Exception as e:
        mape = 0.0
    
    peak_pm_20 = peak_performance_metric(test.values, forecasts, 0.2, initial_offset+n_lag, final_offset)
    peak_pm_50 = peak_performance_metric(test.values, forecasts, 0.5, initial_offset+n_lag, final_offset)
    peak_pm_80 = peak_performance_metric(test.values, forecasts, 0.8, initial_offset+n_lag, final_offset)
    jumps_pm_20 = jump_performance_metric(test.values, forecasts, 0.2, initial_offset+n_lag, final_offset)
    jumps_pm_50 = jump_performance_metric(test.values, forecasts, 0.5, initial_offset+n_lag, final_offset)
    jumps_pm_80 = jump_performance_metric(test.values, forecasts, 0.8, initial_offset+n_lag, final_offset)

    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
        print('t+%d PPM (0.2): %f' % (1, peak_pm_20))
        print('t+%d PPM (0.5): %f' % (1, peak_pm_50))
        print('t+%d PPM (0.8): %f' % (1, peak_pm_80))
        print('t+%d JPM (0.2): %f' % (1, jumps_pm_20))
        print('t+%d JPM (0.5): %f' % (1, jumps_pm_50))
        print('t+%d JPM (0.8): %f' % (1, jumps_pm_80))
        print()

    return rmse, mape, peak_pm_20, peak_pm_50, peak_pm_80, jumps_pm_20, jumps_pm_50, jumps_pm_80


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values t+1")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


def get_max_values(values):
    return np.array([np.amax(value) for value in values])


def get_min_values(values):
    return np.array([np.amin(value) for value in values])


def get_average_values(values):
    return np.array([np.average(value) for value in values])


def get_std_values(values):
    return np.array([np.std(value) for value in values])


def get_above_mean_values(values):
    return np.array([np.where(value > np.average(value))[0].size for value in values])


def get_below_mean_values(values):
    return np.array([np.where(value < np.average(value))[0].size for value in values])


def get_median_values(values):
    return np.array([np.median(value) for value in values])


def get_quantile_values(values, a):
    return np.array([np.quantile(value, a) for value in values])


def get_sum_values(values):
    return np.array([np.sum(value) for value in values])


def get_range_count_values(values, min, max):
    return np.array([np.sum((value >= min) & (value < max)) for value in values])


def get_index_of_max_values(values):
    return np.array([np.argmax(value) for value in values])


def get_index_of_min_values(values):
    return np.array([np.argmin(value) for value in values])


# Parameters
file = "../counts.csv"
differentiation = False

normalize = True

# Lag to differentiate the time series
differentiation_lag = 1

# Number of points available to predict the next ones
n_lag = 12

# Number of next points to predict
n_seq = 1

# Number of epochs to train the model
n_epochs = 500

# Batch size
batch_size = 1

# Enable or disable features
week_day_feature = True
work_day_feature = False
hour_feature = False

df = read_file(file)

# Analysis of traffic and choose_series
train_set, test_set = get_series_values(df)


if differentiation:
    train_values = differentiate(train_set, differentiation_lag)
    week_day_array = np.array([date_to_week_day(index) for index in train_set.index[n_lag+differentiation_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in train_set.index[n_lag+differentiation_lag:]])
    hour_array = np.array([date_to_hour(index) for index in train_set.index[n_lag+differentiation_lag:]])

    test_values = differentiate(test_set, differentiation_lag)
    week_day_array_test = np.array([date_to_week_day(index) for index in test_set.index[n_lag+differentiation_lag:]])
    work_day_array_test = np.array([[date_to_work_day(index)] for index in test_set.index[n_lag+differentiation_lag:]])
    hour_array_test = np.array([date_to_hour(index) for index in test_set.index[n_lag+differentiation_lag:]])
else:
    train_values = train_set
    week_day_array = np.array([date_to_week_day(index) for index in train_set.index[n_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in train_set.index[n_lag:]])
    hour_array = np.array([date_to_hour(index) for index in train_set.index[n_lag:]])

    test_values = test_set
    week_day_array_test = np.array([date_to_week_day(index) for index in test_set.index[n_lag:]])
    work_day_array_test = np.array([[date_to_work_day(index)] for index in test_set.index[n_lag:]])
    hour_array_test = np.array([date_to_hour(index) for index in test_set.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(train_values.values.reshape(len(train_values.values), 1))
    scaled_values_test = scaler.transform(test_values.values.reshape(len(test_values.values), 1))
else:
    scaler = None
    scaled_values = train_values.values
    scaled_values_test = test_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)
scaled_values_test = scaled_values_test.reshape(len(scaled_values_test), 1)


# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_test = series_to_supervised(scaled_values_test, n_lag, n_seq)

supervised_values = supervised.values
supervised_values_test = supervised_test.values

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    supervised_values_test = np.c_ [supervised_values_test[:, :-n_seq], work_day_array_test, supervised_values_test[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    supervised_values_test = np.c_[supervised_values_test[:, :-n_seq], week_day_array_test, supervised_values_test[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    supervised_values_test = np.c_ [supervised_values_test[:, :-n_seq], hour_array_test, supervised_values_test[:, -n_seq:]]
    number_inputs += 24


# reshape training into [samples, timesteps, features]
X, y = supervised_values[:, 0:number_inputs], supervised_values[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1])
test_X, test_y = supervised_values_test[:, 0:number_inputs], supervised_values_test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])

# design network
input_tensor = Input(shape=(X.shape[1],))    
output = Dense(200, activation='relu')(input_tensor)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(200, activation='relu')(output)
output = Dropout(0.2)(output)
output = Dense(y.shape[1])(output)
model = Model(inputs=input_tensor, outputs=output)
model.compile(loss='mean_squared_error', optimizer='adam')

model.load_weights('../models/new_test_set/FFNNmodelWeights.h5')

forecasts = make_nn_forecasts(model, batch_size, supervised_values_test, number_inputs, n_seq)

if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

if differentiation:
    forecasts = invert_difference(forecasts, test_set.values[differentiation_lag:], differentiation_lag)


plot_performance_metrics(test_set.values, 8, 14)
error = evaluate_forecasts(test_set[n_lag:], forecasts, n_lag, n_seq, True, 8, 14)

plot_forecasts(test_set[n_lag:], forecasts, len(test_set[n_lag:]), "Feed-Forward NN model prediction", "Sample number", "Number of sessions per hour")

differentiation = False
week_day_feature = False
work_day_feature = True
hour_feature = False
stateful = False

if differentiation:
    train_values = differentiate(train_set, differentiation_lag)
    week_day_array = np.array([date_to_week_day(index) for index in train_set.index[n_lag+differentiation_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in train_set.index[n_lag+differentiation_lag:]])
    hour_array = np.array([date_to_hour(index) for index in train_set.index[n_lag+differentiation_lag:]])

    test_values = differentiate(test_set, differentiation_lag)
    week_day_array_test = np.array([date_to_week_day(index) for index in test_set.index[n_lag+differentiation_lag:]])
    work_day_array_test = np.array([[date_to_work_day(index)] for index in test_set.index[n_lag+differentiation_lag:]])
    hour_array_test = np.array([date_to_hour(index) for index in test_set.index[n_lag+differentiation_lag:]])
else:
    train_values = train_set
    week_day_array = np.array([date_to_week_day(index) for index in train_set.index[n_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in train_set.index[n_lag:]])
    hour_array = np.array([date_to_hour(index) for index in train_set.index[n_lag:]])

    test_values = test_set
    week_day_array_test = np.array([date_to_week_day(index) for index in test_set.index[n_lag:]])
    work_day_array_test = np.array([[date_to_work_day(index)] for index in test_set.index[n_lag:]])
    hour_array_test = np.array([date_to_hour(index) for index in test_set.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(train_values.values.reshape(len(train_values.values), 1))
    scaled_values_test = scaler.transform(test_values.values.reshape(len(test_values.values), 1))
else:
    scaler = None
    scaled_values = train_values.values
    scaled_values_test = test_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)
scaled_values_test = scaled_values_test.reshape(len(scaled_values_test), 1)


# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_test = series_to_supervised(scaled_values_test, n_lag, n_seq)

supervised_values = supervised.values
supervised_values_test = supervised_test.values


number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    supervised_values_test = np.c_ [supervised_values_test[:, :-n_seq], work_day_array_test, supervised_values_test[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    supervised_values_test = np.c_[supervised_values_test[:, :-n_seq], week_day_array_test, supervised_values_test[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    supervised_values_test = np.c_ [supervised_values_test[:, :-n_seq], hour_array_test, supervised_values_test[:, -n_seq:]]
    number_inputs += 24


# reshape training into [samples, timesteps, features]
X, y = supervised_values[:, 0:number_inputs], supervised_values[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1], 1)
test_X, test_y = supervised_values_test[:, 0:number_inputs], supervised_values_test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1], 1)

# design network
input_tensor = Input(shape=(X.shape[1], X.shape[2]), batch_shape=(batch_size,X.shape[1],X.shape[2]))
output = LSTM(200, activation='tanh', stateful=stateful, recurrent_dropout=0.2)(input_tensor)
output = Dropout(0.2)(output)
output = Dense(y.shape[1])(output)
model = Model(inputs=input_tensor, outputs=output)
model.compile(loss='mse', optimizer='adam')

model.load_weights('../models/new_test_set/LSTM_model_weights.h5')

forecasts_lstm = make_lstm_forecasts(model, batch_size, supervised_values_test, number_inputs, n_seq)
if normalize:
    forecasts_lstm = invert_scaler(forecasts_lstm, scaler)
else:
    forecasts_lstm = np.asarray(forecasts_lstm)

if differentiation:
    forecasts_lstm = invert_difference(forecasts_lstm, test_set.values[-differentiation_lag:], differentiation_lag)

forecasts_lstm[forecasts_lstm<0] = 0

error, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(test_set[n_lag:], forecasts_lstm, n_lag, n_seq, True, 8, 14)

plot_forecasts(test_set[n_lag:], forecasts_lstm, len(test_set[n_lag:]), "Recurrent model prediction", "Sample number", "Number of sessions per hour")

n_lag = 168

train_set_reshaped = train_set.values.reshape(len(train_set), 1)
test_set_reshaped = test_set.values.reshape(len(test_set), 1)

# transform into supervised learning problem X, y
supervised = series_to_supervised(train_set_reshaped, n_lag, n_seq)
supervised_test = series_to_supervised(test_set_reshaped, n_lag, n_seq)

supervised_values = supervised.values
supervised_values_test = supervised_test.values

week_day_array = np.array([date_to_week_day(index) for index in train_set.index[n_lag:]])
work_day_array = np.array([[date_to_work_day(index)] for index in train_set.index[n_lag:]])
hour_array = np.array([date_to_hour(index) for index in train_set.index[n_lag:]])

week_day_array_test = np.array([date_to_week_day(index) for index in test_set.index[n_lag:]])
work_day_array_test = np.array([[date_to_work_day(index)] for index in test_set.index[n_lag:]])
hour_array_test = np.array([date_to_hour(index) for index in test_set.index[n_lag:]])


week_max_value = get_max_values(supervised_values[:, :n_lag])
week_min_value = get_min_values(supervised_values[:, :n_lag])
week_average_value = get_average_values(supervised_values[:, :n_lag])
week_std_value = get_std_values(supervised_values[:, :n_lag])
week_above_mean_value = get_above_mean_values(supervised_values[:, :n_lag])
week_below_mean_value = get_below_mean_values(supervised_values[:, :n_lag])
week_median_value = get_median_values(supervised_values[:, :n_lag])
week_sum_value = get_sum_values(supervised_values[:, :n_lag])
week_quantile_value_25 = get_quantile_values(supervised_values[:, :n_lag], 0.25)
week_quantile_value_50 = get_quantile_values(supervised_values[:, :n_lag], 0.50)
week_quantile_value_75 = get_quantile_values(supervised_values[:, :n_lag], 0.75)
week_range_0_100_value = get_range_count_values(supervised_values[:, :n_lag], 0, 100)
week_range_100_300_value = get_range_count_values(supervised_values[:, :n_lag], 100, 300)
week_range_300_600_value = get_range_count_values(supervised_values[:, :n_lag], 300, 600)
week_range_600_700_value = get_range_count_values(supervised_values[:, :n_lag], 600, 700)
week_range_700_9999_value = get_range_count_values(supervised_values[:, :n_lag], 700, 9999)
week_index_max_values = get_index_of_max_values(supervised_values[:, :n_lag])
week_index_min_values = get_index_of_min_values(supervised_values[:, :n_lag])
last_week_value = supervised_values[:, n_lag-168]


day_max_value = get_max_values(supervised_values[:, n_lag-24:n_lag])
day_min_value = get_min_values(supervised_values[:, n_lag-24:n_lag])
day_average_value = get_average_values(supervised_values[:, n_lag-24:n_lag])
day_std_value = get_std_values(supervised_values[:, n_lag-24:n_lag])
day_above_mean_value = get_above_mean_values(supervised_values[:, n_lag-24:n_lag])
day_below_mean_value = get_below_mean_values(supervised_values[:, n_lag-24:n_lag])
day_median_value = get_median_values(supervised_values[:, n_lag-24:n_lag])
day_sum_value = get_sum_values(supervised_values[:, n_lag-24:n_lag])
day_quantile_value_25 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.25)
day_quantile_value_50 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.50)
day_quantile_value_75 = get_quantile_values(supervised_values[:, n_lag-24:n_lag], 0.75)
day_range_0_100_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 0, 100)
day_range_100_300_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 100, 300)
day_range_300_600_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 300, 600)
day_range_600_700_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 600, 700)
day_range_700_9999_value = get_range_count_values(supervised_values[:, n_lag-24:n_lag], 700, 9999)
day_index_max_values = get_index_of_max_values(supervised_values[:, n_lag-24:n_lag])
day_index_min_values = get_index_of_min_values(supervised_values[:, n_lag-24:n_lag])
last_day_value = supervised_values[:, n_lag-24]

last_hour_value = supervised_values[:, n_lag-1]

week_max_value_test = get_max_values(supervised_values_test[:, :n_lag])
week_min_value_test = get_min_values(supervised_values_test[:, :n_lag])
week_average_value_test = get_average_values(supervised_values_test[:, :n_lag])
week_std_value_test = get_std_values(supervised_values_test[:, :n_lag])
week_above_mean_value_test = get_above_mean_values(supervised_values_test[:, :n_lag])
week_below_mean_value_test = get_below_mean_values(supervised_values_test[:, :n_lag])
week_median_value_test = get_median_values(supervised_values_test[:, :n_lag])
week_sum_value_test = get_sum_values(supervised_values_test[:, :n_lag])
week_quantile_value_25_test = get_quantile_values(supervised_values_test[:, :n_lag], 0.25)
week_quantile_value_50_test = get_quantile_values(supervised_values_test[:, :n_lag], 0.50)
week_quantile_value_75_test = get_quantile_values(supervised_values_test[:, :n_lag], 0.75)
week_range_0_100_value_test = get_range_count_values(supervised_values_test[:, :n_lag], 0, 100)
week_range_100_300_value_test = get_range_count_values(supervised_values_test[:, :n_lag], 100, 300)
week_range_300_600_value_test = get_range_count_values(supervised_values_test[:, :n_lag], 300, 600)
week_range_600_700_value_test = get_range_count_values(supervised_values_test[:, :n_lag], 600, 700)
week_range_700_9999_value_test = get_range_count_values(supervised_values_test[:, :n_lag], 700, 9999)
week_index_max_values_test = get_index_of_max_values(supervised_values_test[:, :n_lag])
week_index_min_values_test = get_index_of_min_values(supervised_values_test[:, :n_lag])
last_week_value_test = supervised_values_test[:, n_lag-168]


day_max_value_test = get_max_values(supervised_values_test[:, n_lag-24:n_lag])
day_min_value_test = get_min_values(supervised_values_test[:, n_lag-24:n_lag])
day_average_value_test = get_average_values(supervised_values_test[:, n_lag-24:n_lag])
day_std_value_test = get_std_values(supervised_values_test[:, n_lag-24:n_lag])
day_above_mean_value_test = get_above_mean_values(supervised_values_test[:, n_lag-24:n_lag])
day_below_mean_value_test = get_below_mean_values(supervised_values_test[:, n_lag-24:n_lag])
day_median_value_test = get_median_values(supervised_values_test[:, n_lag-24:n_lag])
day_sum_value_test = get_sum_values(supervised_values_test[:, n_lag-24:n_lag])
day_quantile_value_25_test = get_quantile_values(supervised_values_test[:, n_lag-24:n_lag], 0.25)
day_quantile_value_50_test = get_quantile_values(supervised_values_test[:, n_lag-24:n_lag], 0.50)
day_quantile_value_75_test = get_quantile_values(supervised_values_test[:, n_lag-24:n_lag], 0.75)
day_range_0_100_value_test = get_range_count_values(supervised_values_test[:, n_lag-24:n_lag], 0, 100)
day_range_100_300_value_test = get_range_count_values(supervised_values_test[:, n_lag-24:n_lag], 100, 300)
day_range_300_600_value_test = get_range_count_values(supervised_values_test[:, n_lag-24:n_lag], 300, 600)
day_range_600_700_value_test = get_range_count_values(supervised_values_test[:, n_lag-24:n_lag], 600, 700)
day_range_700_9999_value_test = get_range_count_values(supervised_values_test[:, n_lag-24:n_lag], 700, 9999)
day_index_max_values_test = get_index_of_max_values(supervised_values_test[:, n_lag-24:n_lag])
day_index_min_values_test = get_index_of_min_values(supervised_values_test[:, n_lag-24:n_lag])
last_day_value_test = supervised_values_test[:, n_lag-24]

last_hour_value_test = supervised_values_test[:, n_lag-1]

features = np.c_ [last_hour_value, last_day_value, last_week_value, week_day_array, work_day_array, hour_array, week_max_value, week_min_value,
    week_average_value, day_max_value, day_min_value, day_average_value, day_std_value, week_std_value, day_above_mean_value, day_below_mean_value,
    week_above_mean_value, week_below_mean_value,  week_median_value, day_median_value,
    day_sum_value, week_sum_value, day_range_0_100_value, day_range_100_300_value,
    day_range_300_600_value, day_range_600_700_value, day_range_700_9999_value, week_range_0_100_value, week_range_100_300_value, week_range_300_600_value,
    week_range_600_700_value, week_range_700_9999_value, week_index_max_values, week_index_min_values, day_index_min_values, day_index_max_values]

features_test = np.c_ [last_hour_value_test, last_day_value_test, last_week_value_test, week_day_array_test, work_day_array_test, hour_array_test, week_max_value_test, week_min_value_test,
    week_average_value_test, day_max_value_test, day_min_value_test, day_average_value_test, day_std_value_test, week_std_value_test, day_above_mean_value_test, day_below_mean_value_test,
    week_above_mean_value_test, week_below_mean_value_test, week_median_value_test, day_median_value_test,
    day_sum_value_test, week_sum_value_test, day_range_0_100_value_test, day_range_100_300_value_test,
    day_range_300_600_value_test, day_range_600_700_value_test, day_range_700_9999_value_test, week_range_0_100_value_test, week_range_100_300_value_test, week_range_300_600_value_test,
    week_range_600_700_value_test, week_range_700_9999_value_test, week_index_max_values_test, week_index_min_values_test, day_index_min_values_test, day_index_max_values_test]

number_inputs = features.shape[1]

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_features = scaler.fit_transform(features)
    scaled_labels = scaler.fit_transform(supervised_values[:, -n_seq:])
    scaled_features_test = scaler.transform(features_test)
    scaled_labels_test = scaler.transform(supervised_values_test[:, -n_seq:])
else:
    scaler = None
    scaled_features = features
    scaled_labels = supervised_values[:, -n_seq:]
    scaled_features_test = features_test
    scaled_labels_test = supervised_values_test[:, -n_seq:]


# reshape training into [samples, timesteps, features]
X, y = scaled_features, scaled_labels
X = X.reshape(X.shape[0], X.shape[1])

test_X, test_y = scaled_features_test, scaled_labels_test
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1])

rmse = 9999
forecasts_rf = None
while rmse>55:
    rfr = RandomForestRegressor()
    rfr = rfr.fit(X, y)
    forecasts_rf = rfr.predict(test_X)
    forecasts_rf = forecasts_rf.reshape(len(forecasts_rf), 1)
    if normalize:
        forecasts_rf = invert_scaler(forecasts_rf, scaler)
    else:
        forecasts_rf = np.asarray(forecasts_rf)

    forecasts_rf[forecasts_rf<0] = 0
    rmse, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(test_set[n_lag:], forecasts_rf, n_lag, n_seq, False, 8, 14)

rmse, mape, ppm_20, ppm_50, ppm_80, jpm_20, jpm_50, jpm_80 = evaluate_forecasts(test_set[n_lag:], forecasts_rf, n_lag, n_seq, True, 8, 14)
plot_forecasts(test_set[n_lag:], forecasts_rf, len(test_set[n_lag:]), "Random Forest Regressor model prediction", "Sample number", "Number of sessions")



error = evaluate_forecasts(test_set[n_lag:], forecasts[-len(forecasts_rf):], n_lag, n_seq, True, 8, 14)
plot_forecasts(test_set[n_lag:], forecasts[-len(forecasts_rf):], len(test_set[n_lag:]), "Feed-Forward NN model prediction", "Sample number", "Number of sessions per hour")

error = evaluate_forecasts(test_set[n_lag:], forecasts_lstm[-len(forecasts_rf):], n_lag, n_seq, True, 8, 14)
plot_forecasts(test_set[n_lag:], forecasts_lstm[-len(forecasts_rf):], len(test_set[n_lag:]), "Recurrent NN model prediction", "Sample number", "Number of sessions per hour")


two_best_forecasts = (forecasts[-len(forecasts_rf):]+forecasts_rf)/2
error = evaluate_forecasts(test_set[n_lag:], two_best_forecasts, n_lag, n_seq, True, 8, 14)
plot_forecasts(test_set[n_lag:], two_best_forecasts, len(test_set[n_lag:]), "Ensemble voting of two best models prediction", "Sample number", "Number of sessions per hour")


all_forecasts = (forecasts[-len(forecasts_rf):]+forecasts_lstm[-len(forecasts_rf):]+forecasts_rf)/3
error = evaluate_forecasts(test_set[n_lag:], all_forecasts, n_lag, n_seq, True, 8, 14)
plot_forecasts(test_set[n_lag:], all_forecasts, len(test_set[n_lag:]), "Ensemble voting of three best models prediction", "Sample number", "Number of sessions per hour")
