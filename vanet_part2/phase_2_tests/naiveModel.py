import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from math import sqrt
from pandas import datetime
from sklearn.metrics import mean_squared_error


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"], df.loc['2018-12-12 08:00:00':'2019-01-11 13:00:00']["values"]


# Make a forecast of the next value be equal to the previous one
def make_persistence_forecast(train, test, n_steps=1, n_lag=1):
    return [train[-(i+n_lag):] + test[:-(i+n_lag)]for i in range(n_steps)]


# Calculate the mean absolute precentage error
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_steps, debug=True):
    for i in range(n_steps):
        predicted = forecasts[i]
        mse = mean_squared_error(test, predicted)
        rmse = sqrt(mse)
        mape = mean_absolute_percentage_error(test, np.resize(forecasts, (len(forecasts[0]),)))
        if debug:
            print('t+%d RMSE: %f' % ((i + 1), rmse))
            print('t+%d MAPE: %f' % ((i + 1), mape))
    return rmse


# Parameters
file = "../counts.csv"
DEBUG = True
n_steps = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2

# Number of hours to lag from the current prediction
n_lag = 1

df = read_file(file)
train_values, test_values = get_series_values(df)

train_values = train_values.values.tolist()
test_values = test_values.values.tolist()

forecasts = make_persistence_forecast(train_values, test_values, n_steps, n_lag)

evaluate_forecasts(test_values, forecasts, n_steps, True)
