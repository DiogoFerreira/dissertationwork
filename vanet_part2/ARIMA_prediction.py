import glob
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return Series([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# Print the model summary and residual errors from the arima model
def print_arima_errors(model):
    print(model.summary())
    residuals = DataFrame(model.resid)
    residuals.plot()
    plt.show()
    residuals.plot(kind='kde')
    plt.show()
    print(residuals.describe())


# Fit an arima model and predict one step
def fit_and_predict_arima_step(data, order, debug=False):
    model = ARIMA(data, order=order)
    model_fit = model.fit(disp=0)
    
    if debug:
        print_arima_errors(model_fit)
    
    output = model_fit.forecast()
    return output[0]


# Fit and predict the ARIMA model, with train and test dataset
def fit_and_predict_arima(train_data, test_data, order):
    history = train_data
    predictions = []
    i=0
    for elem in test_data:
        history = np.concatenate((history, [elem]))
        predictions.append([fit_and_predict_arima_step(history, order)[0]])
        i += 1
        print(i)
    return predictions


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test.values, forecasts))
    mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse, mape


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# Parameters
file = "counts.csv"
differentiation = True
second_differentiation = False

normalize = True

# Lag to differentiate the time series
differentiation_lag = 24*7

# Lag for second differentiation of the time series
second_differentiation_lag = 24*7

# Number of points available to predict the next ones
n_lag = 1

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2

# ARIMA order to train
order = (0,0,0)

p = [3]
q = [0]

for i in p:
    for j in q:
        order = (i,0,j)
        if order != (0,0,3) and order != (0,0,4) and order != (0,0,5):
            try:
                df = read_file(file)

                # Analysis of traffic and choose_series
                series_values = get_series_values(df)
                original_series_values = series_values

                if differentiation:
                    series_values = differentiate(series_values, differentiation_lag)
                    if second_differentiation:
                        second_series_values = series_values
                        series_values = differentiate(series_values, second_differentiation_lag)
                        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
                    else:
                        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
                else:
                    original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)

                if normalize:
                    # rescale values
                    scaler = StandardScaler()
                    scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
                else:
                    scaler = None
                    scaled_values = series_values.values


                scaled_values = scaled_values.reshape(len(scaled_values), 1)


                # transform into supervised learning problem X, y
                supervised = series_to_supervised(scaled_values, n_lag, n_seq)
                supervised_values = supervised.values

                train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)
                
                forecasts = fit_and_predict_arima(train[:, n_lag - 1], validation[:, n_lag-1], order)

                if normalize:
                    forecasts = invert_scaler(forecasts, scaler)
                else:
                    forecasts = np.asarray(forecasts)


                if second_differentiation and differentiation:
                    forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
                    forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
                elif differentiation:
                    forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)


                forecasts[forecasts<0] = 0
                rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
                plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "ARIMA model prediction", "Sample number", "Number of sessions per hour")

                diff_title = str(differentiation_lag)+"_"+str(second_differentiation_lag)+"_" if second_differentiation and differentiation else str(differentiation_lag)+"_" if differentiation else "not_differenced_"
                with open("arima_predictions/"+diff_title+str(order)+"-results.txt", 'w') as f:
                    for elem in forecasts:
                        f.write("%s\n" % elem)
                    f.write("RMSE: %s\n" % rmse)
                    f.write("MAPE: %s\n" % mape)

            except Exception as e:
                print(e)

