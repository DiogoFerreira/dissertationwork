import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from keras.utils import plot_model
from keras.optimizers import Adam
from keras.models import Sequential, Input, Model
from keras.layers import Dense, LSTM, Dropout
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return Series([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag, 1)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test, forecasts))

    try:
        mape = mean_absolute_percentage_error(test, np.resize(forecasts, (len(forecasts),)))
    except Exception as e:
        mape = 0.0

    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse, mape


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values t+1")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


def get_model(i, batch_size, shape_x, shape_y, stateful):
    input_tensor = Input(shape=(shape_x, shape_y), batch_shape=(batch_size,shape_x,shape_y))
    output = LSTM(200, activation='tanh', stateful=stateful, recurrent_dropout=0.2)(input_tensor)
    output = Dropout(0.2)(output)
    output = Dense(y.shape[1])(output)
    model = Model(inputs=input_tensor, outputs=output)
    return model


def get_first_diff(i):
    return False, 0


def get_lag(i):
    return 12


def get_stateful(i):
    return False
    

def get_features(i):
    if i<1:
        return True, False, False
    elif i<2:
        return False, True, False   
    elif i<3:
        return False, False, True
    elif i<4:
        return True, True, False
    elif i<5:
        return True, False, True
    elif i<6:
        return False, True, True
    else:
        return True, True, True


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


# Parameters
file = "counts.csv"
differentiation = False
second_differentiation = False

normalize = True

# Lag to differentiate the time series
differentiation_lag = 1

# Lag for second differentiation of the time series
second_differentiation_lag = 24*7

# Number of points available to predict the next ones
n_lag = 12

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2

# Number of epochs to train the model
n_epochs = 500

# Batch size
batch_size = 1

# Set if the model is stateful or not
stateful = False


model_number = 1
week_day_feature, work_day_feature, hour_feature = get_features(model_number)
differentiation, differentiation_lag = get_first_diff(model_number)
n_lag = get_lag(model_number)
stateful = get_stateful(model_number)


df = read_file(file)

# Analysis of traffic and choose_series
series_values = get_series_values(df)
original_series_values = series_values

if differentiation:
    series_values = differentiate(series_values, differentiation_lag)
    if second_differentiation:
        second_series_values = series_values
        series_values = differentiate(series_values, second_differentiation_lag)
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
    else:
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
else:
    original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
    week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
    work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
    hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

if normalize:
    # rescale values
    scaler = StandardScaler()
    scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
else:
    scaler = None
    scaled_values = series_values.values


scaled_values = scaled_values.reshape(len(scaled_values), 1)


# transform into supervised learning problem X, y
supervised = series_to_supervised(scaled_values, n_lag, n_seq)
supervised_values = supervised.values

number_inputs = n_lag
if work_day_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 1

if week_day_feature:
    supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
    number_inputs += 7

if hour_feature:
    supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
    number_inputs += 24

train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)


# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1], 1)
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1], 1)
test_X, test_y = test[:, 0:number_inputs], test[:, number_inputs:]
test_X = test_X.reshape(test_X.shape[0], test_X.shape[1], 1)

X = np.vstack((X, validation_X))
y = np.vstack((y, validation_y))

train = np.vstack((train, validation))

original_train = np.hstack((original_train, original_validation))

# design network
model = get_model(model_number, batch_size, X.shape[1], X.shape[2], stateful)
model.compile(loss='mse', optimizer='adam')


train_rmse, test_rmse = list(), list()
train_mape, test_mape = list(), list()
lowest_error = 9999
best_model = None

for i in range(n_epochs):
    print("Epoch "+str(i+1))
    history = model.fit(X, y, epochs=1, batch_size=batch_size, verbose=0, shuffle=False)
        
    if stateful:
        model.reset_states()

    forecasts = make_nn_forecasts(model, batch_size, train, number_inputs, n_seq)

    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[n_lag:-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[second_differentiation_lag+n_lag:-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[n_lag:-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    error, mape = evaluate_forecasts(original_train, forecasts, n_lag, n_seq, True)
    train_rmse.append(error)
    train_mape.append(mape)
        
    forecasts = make_nn_forecasts(model, batch_size, test, number_inputs, n_seq)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[second_differentiation_lag-len(test):], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)

    forecasts[forecasts<0] = 0
    error, mape = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)
    test_rmse.append(error)
    test_mape.append(mape)

    if stateful:
        model.reset_states()

    if error < lowest_error:
        lowest_error = error
        best_model = model
        best_model.save_weights("models/final_models/LSTM_model_weights"+str(model_number)+".h5")

history = DataFrame()
history['train'], history['test'] = train_rmse, test_rmse
writer = open("final_predictions/lstm_predictions"+str(model_number)+".csv", 'w')
writer.write(history.to_string()+"\n")
writer.close()

history_mape = DataFrame()
history_mape['train'], history_mape['test'] = train_mape, test_mape
writer = open("final_predictions/lstm_predictions_mape_"+str(model_number)+".csv", 'w')
writer.write(history_mape.to_string()+"\n")
writer.close()


# plt.title("RMSE error")
# plt.plot(train_rmse, "b", label="Training error")
# plt.plot(test_rmse, "orange", label="Test error")
# plt.legend()
# plt.show()


# forecasts = make_nn_forecasts(best_model, batch_size, test, number_inputs, n_seq)

# if normalize:
#     forecasts = invert_scaler(forecasts, scaler)
# else:
#     forecasts = np.asarray(forecasts)

# if second_differentiation and differentiation:
#     forecasts = invert_difference(forecasts, second_series_values.values[-second_differentiation_lag-len(test):], second_differentiation_lag)
#     forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)
# elif differentiation:
#     forecasts = invert_difference(forecasts, original_series_values.values[-differentiation_lag-len(test):], differentiation_lag)

# forecasts[forecasts<0] = 0

# error = evaluate_forecasts(original_test, forecasts, n_lag, n_seq, True)

# plot_forecasts(original_series_values.values, forecasts, len(test), "Recurrent NN model prediction", "Sample number", "Number of sessions per hour")

