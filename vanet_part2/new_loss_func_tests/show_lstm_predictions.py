import pandas as pd
from matplotlib import pyplot

file = "lstm_predictions.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()

file = "lstm_predictions_mape.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "lstm_predictions_ppm20.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "lstm_predictions_ppm50.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "lstm_predictions_ppm80.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "lstm_predictions_jpm20.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "lstm_predictions_jpm50.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])

file = "lstm_predictions_jpm80.csv"

predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
print(predictions.loc[:, "test"].idxmin())
print(predictions.loc[:, "test"].min())

pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
pyplot.title("Predictions")
pyplot.legend()
pyplot.show()
print(predictions.loc[301,"test"])