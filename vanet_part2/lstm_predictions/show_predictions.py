from matplotlib import pyplot
import pandas as pd

for model_number in range(6, 7):
	file = "feature_tests/lstm_predictions"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	print("model number: "+str(model_number))
	print(predictions.loc[:, "test"].idxmin())
	print(predictions.loc[:, "test"].min())

	# pyplot.plot(predictions.loc[:, "test"], color='blue', label='Test data')
	# pyplot.plot(predictions.loc[:, "train"], color='orange', label='Training data')
	# pyplot.title("Predictions")
	# pyplot.legend()
	# pyplot.show()

