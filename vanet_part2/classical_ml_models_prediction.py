import os
import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn import svm
from sklearn.linear_model import LinearRegression, SGDRegressor, Ridge, Lasso, ElasticNet
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, AdaBoostRegressor
from xgboost import XGBRegressor


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return Series([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test.values, forecasts))
    mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse, mape


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


def write_to_file(classifier_name, rmse):
    filename = "classical_ml_predictions/"+classifier_name+"_"+str(work_day_feature)+"_"+str(week_day_feature)+"_"+str(hour_feature)+".csv"
    if os.path.exists(filename):
        append_write = 'a' # append if already exists
    else:
        append_write = 'w' # make a new file if not

    writer = open(filename, append_write)
    writer.write(str(differentiation_lag)+";"+str(n_lag)+";"+str(rmse)+"\n")
    writer.close()


def get_diff_lag(i):
    if i%4 == 0:
        return 0
    elif i%4 == 1:
        return 1
    elif i%4 == 2:
        return 24
    else:
        return 168


def get_lag_number(i):
    if (i//4)%5 == 0:
        return 1
    elif (i//4)%5 == 1:
        return 12
    elif (i//4)%5 == 2:
        return 24
    elif (i//4)%5 == 3:
        return 48
    else:
        return 168


def get_features(i):
    if (i//(4*5)) % 7 == 0:
        return True, False, False
    elif (i//(4*5)) % 7 == 1:
        return False, True, False   
    elif (i//(4*5)) % 7 == 2:
        return False, False, True
    elif (i//(4*5)) % 7 == 3:
        return True, True, False
    elif (i//(4*5)) % 7 == 4:
        return True, False, True
    elif (i//(4*5)) % 7 == 5:
        return False, True, True
    else:
        return True, True, True


# Parameters
file = "counts.csv"
differentiation = False
second_differentiation = False
week_day_feature = True
work_day_feature = True
hour_feature = True

normalize = True

# Lag to differentiate the time series
differentiation_lag = 24*7

# Lag for second differentiation of the time series
second_differentiation_lag = 24*7

# Number of points available to predict the next ones
n_lag = 24

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2


df = read_file(file)

for i in range(0,4*5*7):

    differentiation_lag = get_diff_lag(i)
    if differentiation_lag == 0:
        differentiation = False
    else:
        differentiation = True

    n_lag = get_lag_number(i)

    #week_day_feature, work_day_feature, hour_feature = get_features(i)
    week_day_feature, work_day_feature, hour_feature = True, True, True
    differentiation = False
    n_lag = 24
    # Analysis of traffic and choose_series
    series_values = get_series_values(df)
    original_series_values = series_values

    if differentiation:
        series_values = differentiate(series_values, differentiation_lag)
        if second_differentiation:
            second_series_values = series_values
            series_values = differentiate(series_values, second_differentiation_lag)
            original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
            week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
            work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
            hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
        else:
            original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
            week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
            work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag:]])
            hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
    else:
        original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
        week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
        work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
        hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

    if normalize:
        # rescale values
        scaler = StandardScaler()
        scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
    else:
        scaler = None
        scaled_values = series_values.values


    scaled_values = scaled_values.reshape(len(scaled_values), 1)


    # transform into supervised learning problem X, y
    supervised = series_to_supervised(scaled_values, n_lag, n_seq)
    supervised_values = supervised.values

    number_inputs = n_lag
    if work_day_feature:
        supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
        number_inputs += 1

    if week_day_feature:
        supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
        number_inputs += 7

    if hour_feature:
        supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
        number_inputs += 24

    train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)

    # reshape training into [samples, timesteps, features]
    X, y = train[:, 0:number_inputs], train[:, number_inputs:]
    X = X.reshape(X.shape[0], X.shape[1])
    validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
    validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])



    sgd = SGDRegressor()
    sgd = sgd.fit(X, y)
    forecasts = sgd.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)

    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Stochastic Gradient Descent Regressor model prediction", "Sample number", "Number of sessions")
    #write_to_file("SGDRegressor", rmse)


    svr = svm.SVR()
    svr = svr.fit(X, y)
    forecasts = svr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Support Vector Regressor model prediction", "Sample number", "Number of sessions")
    #write_to_file("SVR", rmse)


    lr = LinearRegression()
    lr = lr.fit(X, y)
    forecasts = lr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Linear Regression model prediction", "Sample number", "Number of sessions")
    #write_to_file("Linear Regression", rmse)


    ridge = Ridge()
    ridge = ridge.fit(X, y)
    forecasts = lr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)


    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Ridge Regression model prediction", "Sample number", "Number of sessions")
    #write_to_file("Ridge", rmse)


    lasso = Lasso()
    lasso = lasso.fit(X, y)
    forecasts = lasso.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Lasso Regression model prediction", "Sample number", "Number of sessions")
    #write_to_file("Lasso", rmse)


    elasticNet = ElasticNet()
    elasticNet = elasticNet.fit(X, y)
    forecasts = elasticNet.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "ElasticNet Regression model prediction", "Sample number", "Number of sessions")
    #write_to_file("ElasticNet", rmse)


    rfr = RandomForestRegressor()
    rfr = rfr.fit(X, y)
    forecasts = rfr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Random Forest Regressor model prediction", "Sample number", "Number of sessions per hour")
    #write_to_file("Random Forest", rmse)


    gbr = GradientBoostingRegressor()
    gbr = gbr.fit(X, y)
    forecasts = gbr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Gradient Boosting Regressor model prediction", "Sample number", "Number of sessions")

    #write_to_file("Gradient Boosting", rmse)


    abr = AdaBoostRegressor()
    abr = abr.fit(X, y)
    forecasts = abr.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "AdaBoost Regressor model prediction", "Sample number", "Number of sessions")
    #write_to_file("AdaBoost", rmse)


    xgb = XGBRegressor()
    xgb = xgb.fit(X, y)
    forecasts = xgb.predict(validation_X)
    forecasts = forecasts.reshape(len(forecasts), 1)
    if normalize:
        forecasts = invert_scaler(forecasts, scaler)
    else:
        forecasts = np.asarray(forecasts)

    if second_differentiation and differentiation:
        forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
    elif differentiation:
        forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

    forecasts[forecasts<0] = 0
    rmse, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
    plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "XGBoost model prediction", "Sample number", "Number of sessions per hour")
    #write_to_file("XGBRegressor", rmse)