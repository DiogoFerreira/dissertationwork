import holidays
import numpy as np
import pandas as pd
from math import sqrt
import matplotlib.pyplot as plt
from tsfresh import extract_features
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat, Series
from tsfresh.feature_extraction import MinimalFCParameters, EfficientFCParameters
from sklearn.feature_selection import VarianceThreshold
from tsfresh.utilities.dataframe_functions import impute
from tsfresh import select_features
from sklearn import svm
from sklearn.linear_model import LinearRegression, SGDRegressor, Ridge, Lasso, ElasticNet
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, AdaBoostRegressor
from xgboost import XGBRegressor


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, debug=True):
    rmse = sqrt(mean_squared_error(test.values, forecasts))
    mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse, mape


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


# Parameters
file = "counts.csv"

normalize = True

# Number of points available to predict the next ones
n_lag = 168

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2



df = read_file(file)

series_values = get_series_values(df)

original_train, original_validation, original_test = train_validation_test_split(series_values, n_lag, validation_hours, test_hours)

values = series_values.values.reshape(len(series_values), 1)

supervised = series_to_supervised(values, n_lag, n_seq)
supervised_values = supervised.values

id_list = []
values_list = []
time_list = []

idx = 0
for ts in supervised_values[:, :-n_seq]:
	time = 0
	for value in ts:
		id_list.append(idx)
		values_list.append(value)
		time_list.append(time)
		time += 1
	idx += 1


dataframe = DataFrame(data={'id': id_list, 'values': values_list, 'time':time_list})

#X = extract_features(dataframe, column_id='id', column_value='values', column_sort='time', default_fc_parameters=MinimalFCParameters())
X = extract_features(dataframe, column_id='id', column_value='values', column_sort='time')

# Remove Nan and Infs
X = impute(X)

y = supervised_values[:, -n_seq:]
y = np.resize(y, len(y))

filtered_features = X
#filtered_features = select_features(X, y, ml_task='regression')
y = y.reshape(-1, 1)


if normalize:
    scaler = MinMaxScaler()
    scaled_features = scaler.fit_transform(filtered_features)
    scaled_label = scaler.fit_transform(y)
else:
    scaler = None
    scaled_features = filtered_features
    scaled_label = y

week_day_array = np.array([date_to_week_day(index) for index in series_values.index[n_lag:]])
work_day_array = np.array([[date_to_work_day(index)] for index in series_values.index[n_lag:]])
hour_array = np.array([date_to_hour(index) for index in series_values.index[n_lag:]])


scaled_data = np.c_[scaled_features, week_day_array, work_day_array, hour_array, scaled_label]
number_inputs = filtered_features.shape[1]+24+7+1

sel = VarianceThreshold(threshold=0.9)
print(filtered_features.shape)
filtered_features = sel.fit_transform(filtered_features)
print(filtered_features.shape)

exit(0)

import seaborn as sns
corrmat = X.corr()
top_corr_features = corrmat.index
plt.figure(figsize=(20,20))
#plot heat map
g=sns.heatmap(X[top_corr_features].corr(),annot=True,cmap="RdYlGn")
plt.show()
exit(0)

train, validation, test = train_validation_test_split(scaled_data, 0, validation_hours, test_hours)

# reshape training into [samples, timesteps, features]
X, y = train[:, 0:number_inputs], train[:, number_inputs:]
X = X.reshape(X.shape[0], X.shape[1])
validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])


sgd = SGDRegressor()
sgd = sgd.fit(X, y)
forecasts = sgd.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)

if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "Stochastic Gradient Descent Regressor model prediction", "Sample number", "Number of sessions")


svr = svm.SVR()
svr = svr.fit(X, y)
forecasts = svr.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "SVR Regressor model prediction", "Sample number", "Number of sessions")


lr = LinearRegression()
lr = lr.fit(X, y)
forecasts = lr.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "Linear Regression model prediction", "Sample number", "Number of sessions")


ridge = Ridge()
ridge = ridge.fit(X, y)
forecasts = lr.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)


forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "Ridge model prediction", "Sample number", "Number of sessions")


lasso = Lasso()
lasso = lasso.fit(X, y)
forecasts = lasso.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "Lasso model prediction", "Sample number", "Number of sessions")


elasticNet = ElasticNet()
elasticNet = elasticNet.fit(X, y)
forecasts = elasticNet.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "ElasticNet model prediction", "Sample number", "Number of sessions")


rfr = RandomForestRegressor()
rfr = rfr.fit(X, y)
forecasts = rfr.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "Random Forest model prediction", "Sample number", "Number of sessions")


gbr = GradientBoostingRegressor()
gbr = gbr.fit(X, y)
forecasts = gbr.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "Gradient Boosting model prediction", "Sample number", "Number of sessions")


abr = AdaBoostRegressor()
abr = abr.fit(X, y)
forecasts = abr.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "AdaBoost model prediction", "Sample number", "Number of sessions")

xgb = XGBRegressor()
xgb = xgb.fit(X, y)
forecasts = xgb.predict(validation_X)
forecasts = forecasts.reshape(len(forecasts), 1)
if normalize:
    forecasts = invert_scaler(forecasts, scaler)
else:
    forecasts = np.asarray(forecasts)

forecasts[forecasts<0] = 0
rmse, mape = evaluate_forecasts(original_validation, forecasts)
#plot_forecasts(series_values[:-len(test)], forecasts, len(validation), "XGBoost model prediction", "Sample number", "Number of sessions")

