import glob
import pylab
import pandas as pd
import numpy as np
import scipy.stats as scs
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.tsa.api as smt
from pandas.plotting import lag_plot
from pandas import Series, datetime, DataFrame
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))

# Parse time-series data interval
def choose_series(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


def test_stationarity(timeseries, window=24):

    print("Mean: " + str(timeseries.mean()))
    print("Std: " + str(timeseries.std()))

    # Determing rolling statistics
    rolmean = timeseries.rolling(window=window).mean()
    rolstd = timeseries.rolling(window=window).std()

    # Plot rolling statistics:
    fig5 = plt.figure()
    orig = plt.plot(timeseries, color='blue', label='Original data')
    mean = plt.plot(rolmean, color='red', label='Rolling Mean')
    std = plt.plot(rolstd, color='black', label='Rolling Std')
    plt.legend(loc='best')
    plt.ylabel("Number of sessions per hour")
    plt.xlabel("Timestamp")
    plt.title('Rolling Mean & Standard Deviation of time-series data')

    # Perform Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    values = timeseries.values.reshape(len(timeseries.values))
    dftest = adfuller(values, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic', 'p-value', '#Lags Used', 'Number of Observations Used'])
    for key, value in dftest[4].items():
        dfoutput['Critical Value (%s)' % key] = value
    print(dfoutput)
    print("Autocorrelation lag+1: "+str(timeseries.autocorr(1)))
    print("Autocorrelation lag day: "+str(timeseries.autocorr(24)))
    print("Autocorrelation lag week: "+str(timeseries.autocorr(24*7)))
    print()
    print()


def draw_lag_plot(series):
    hour_dict = {i:[] for i in range(0, 24)}
    last = None
    for idx, s in series.iteritems():
        if last == None:
            last = s
        else:
            hour_dict[idx.hour].append([last,s])
            last = s

    # Generate colors
    NUM_COLORS = 24

    cm = pylab.get_cmap('nipy_spectral')

    cgen = [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)]

    fig = plt.figure()
    for key, values in hour_dict.items():
        plt.scatter([value[0] for value in values], [value[1] for value in values], color=cgen[key], s=20, label=str(key)+":00 to "+str(key+1)+":00")
    plt.legend()
    plt.xlabel("Number of sessions at time t")
    plt.ylabel("Number of sessions at time t+1")
    plt.title("Number of sessions in consecutive timestamps in different periods of day")
    plt.show()


def tsplot(y, lags=None, figsize=(10, 8), style='bmh'):
    if not isinstance(y, pd.Series):
        y = pd.Series(y)
    with plt.style.context(style):

        smt.graphics.plot_acf(y, lags=lags, alpha=0.5)
        plt.title("Autocorrelation function")
        plt.xlabel("Number of lags (1 hour per lag)")
        plt.ylabel("Autocorrelation")
        plt.show()

        smt.graphics.plot_pacf(y, lags=lags, alpha=0.5)
        plt.title("Partial Autocorrelation function")
        plt.xlabel("Number of lags (1 hour per lag)")
        plt.ylabel("Partial Autocorrelation")
        plt.show()

        y.hist(bins = 20)
        plt.title("Time-series data histogram")
        plt.xlabel("Number of sessions per hour")
        plt.ylabel("Frequency")
        plt.show()

        
        fig = plt.figure(figsize=figsize)
        
        layout = (3, 2)
        ts_ax = plt.subplot2grid(layout, (0, 0), colspan=2)
        acf_ax = plt.subplot2grid(layout, (1, 0))
        pacf_ax = plt.subplot2grid(layout, (1, 1))
        qq_ax = plt.subplot2grid(layout, (2, 0))
        pp_ax = plt.subplot2grid(layout, (2, 1))

        y.plot(ax=ts_ax)
        ts_ax.set_title('Time Series Analysis Plots')
        smt.graphics.plot_acf(y, lags=lags, ax=acf_ax, alpha=0.5)
        smt.graphics.plot_pacf(y, lags=lags, ax=pacf_ax, alpha=0.5)
        sm.qqplot(y, line='s', ax=qq_ax)
        qq_ax.set_title('QQ Plot')
        scs.probplot(y, sparams=(y.mean(), y.std()), plot=pp_ax)

        plt.tight_layout()

        fig2 = plt.figure()
        y.hist()

        decomposition = seasonal_decompose(y, freq=168, model="additive")
        trend = decomposition.trend
        seasonal = decomposition.seasonal
        residual = decomposition.resid

        fig4 = plt.figure()
        fig4.text(0.5, 0.98, 'Seasonal decomposition with additive model and frequency of one week', ha='center')
        fig4.text(0.5, 0.02, 'Timestamp', ha='center')
        fig4.text(0.04, 0.5, 'Number of sessions per hour', va='center', rotation='vertical')
        plt.subplot(411)
        plt.plot(y, label='Original')
        plt.legend(loc='best')
        plt.subplot(412)
        plt.plot(trend, label='Trend')
        plt.legend(loc='best')
        plt.subplot(413)
        plt.plot(seasonal, label='Seasonality')
        plt.legend(loc='best')
        plt.subplot(414)
        plt.plot(residual, label='Residuals')
        plt.legend(loc='best')
        plt.tight_layout()
        
        fig3 = plt.figure()
        lag_plot(y)
        plt.show()


def analyze_data(series):

    # Differentiate the time-series in lag time-steps
    def differentiate_ratio(series, lag=1):
        return Series([series[i]/series[i-lag] if series[i-lag]>1.0 else series[i] for i in range(lag, len(series))])

    diff_series = differentiate_ratio(chosen_series)
    diff_series.plot()
    plt.xlabel("Sample Number")
    plt.title("Sessions per hour in the training set - Multiplicative transformation")
    plt.ylabel("Number of sessions per hour ratio")
    plt.show()

    series.plot()
    plt.xlabel("Timestamp")
    plt.title("Time-series data train, validation and test split")
    plt.ylabel("Number of sessions per hour")
    plt.show()

    print(series)
    print(len(series))
    print((series==0).astype(int).sum())

    print("Original Data")
    test_stationarity(series, window=24)
    tsplot(series, 350)
    draw_lag_plot(series)

    print("One Differentiation")
    diff_series = Series(np.diff(series))
    diff_series.plot()
    plt.xlabel("Sample Number")
    plt.title("Sessions per hour in the training set - One-lag differentiation")
    plt.ylabel("Number of sessions per hour variation")
    plt.show()
    test_stationarity(diff_series)
    tsplot(diff_series, 350)

    # print("Two Differentiations")
    # diff_two_series = Series(np.diff(diff_series))
    # test_stationarity(diff_two_series)
    # tsplot(diff_two_series, 350)

    day_diff = []
    for i in range(24, len(series)):
        value = series[i]-series[i-24]
        day_diff.append(value)

    day_diff = Series(day_diff)
    print("Day differentiation")
    test_stationarity(day_diff)
    tsplot(day_diff, 350)

    week_diff = []
    for i in range(24*7, len(series)):
        value = series[i]-series[i-24*7]
        week_diff.append(value)

    week_diff = Series(week_diff)
    print("Week differentiation")
    test_stationarity(week_diff)
    tsplot(week_diff, 350)

    # day_diff = []
    # for i in range(24, len(diff_series)):
    #     value = diff_series[i]-diff_series[i-24]
    #     day_diff.append(value)

    # day_diff = Series(day_diff)
    # print("Day and one degree differentiation")
    # test_stationarity(day_diff)
    # tsplot(day_diff, 350)

    # week_diff = []
    # for i in range(24*7, len(diff_series)):
    #     value = diff_series[i]-diff_series[i-24*7]
    #     week_diff.append(value)

    # week_diff = Series(week_diff)
    # print("Week and one degree differentiation")
    # test_stationarity(week_diff)
    # tsplot(week_diff, 350)

    # print("Moving average of data")
    # moving_avg = series.rolling(12).mean()
    # ts_log_moving_avg_diff = series - moving_avg
    # ts_log_moving_avg_diff.dropna(inplace=True)
    # test_stationarity(ts_log_moving_avg_diff)
    # tsplot(ts_log_moving_avg_diff, 100)

    # print("Exponential weighted average of data")
    # expwighted_avg = series.ewm(halflife=12).mean()
    # ts_log_ewma_diff = series - expwighted_avg
    # test_stationarity(ts_log_ewma_diff)
    # tsplot(ts_log_ewma_diff, 100)


# Parameters
file = "counts.csv"
DEBUG = True

df = read_file(file)
chosen_series = choose_series(df)
analyze_data(chosen_series)
