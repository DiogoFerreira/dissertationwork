from matplotlib import pyplot
import pandas as pd

for model_number in range(8, 9):
	file = "ffnn_predictions"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	print("model number: "+str(model_number))
	print(predictions.loc[:, "test"].idxmin())
	print(predictions.loc[:, "test"].min())

	# pyplot.plot(predictions.loc[:, "train"], color='orange', label='Train RMSE')
	# pyplot.plot(predictions.l38.450918oc[:, "test"], color='blue', label='Cross-validation RMSE')
	# pyplot.title("Prediction error")
	# pyplot.xlabel("Number of epochs")
	# pyplot.ylabel("RMSE")
	# pyplot.legend()
	# pyplot.show()
