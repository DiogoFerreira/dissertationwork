from matplotlib import pyplot
import pandas as pd

for model_number in range(15, 16):
	file = "batch_tests_features/Repetition of best result/ffnn_predictions2_"+str(model_number)+".csv"

	predictions = pd.read_csv(file, skipinitialspace=True, sep=" ")
	print("model number: "+str(model_number))
	print(predictions.loc[:, "test"].idxmin())
	print(predictions.loc[:, "test"].min())

	pyplot.plot(predictions.loc[:, "train"], color='orange', label='Train RMSE')
	pyplot.plot(predictions.loc[:, "test"], color='blue', label='Cross-validation RMSE')
	pyplot.title("Prediction error")
	pyplot.xlabel("Number of epochs")
	pyplot.ylabel("RMSE")
	pyplot.legend()
	pyplot.show()
