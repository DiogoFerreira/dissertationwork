import pandas as pd
from pandas import datetime
import matplotlib.pyplot as plt

# No data from 29/august to 13 September
# Many connections in 14/august
# Sessions with very large duration
# Sessions with negative duration
# After Christmas, the number of connections are very different


filename = "session_data.csv"

# Read all session logs and store the timestamp and the session duration
data = pd.read_csv(filename, usecols=["ts", "duration"], parse_dates=[0])

# Replace rows with duration Nan to 0
data["duration"].fillna(0, inplace=True)

# Create the column "start", with the start pf each session
data["duration"] = data["duration"].apply(lambda duration: pd.Timedelta(seconds=duration))
data["start"] = data["ts"]
data.columns = ["start", "duration", "end"]
data["start"] = data["start"] - data["duration"]
print(data.head())
print(data["duration"].describe())

data["duration"].astype('timedelta64[m]').plot()
plt.title("Session duration")
plt.ylabel("Duration of session (minutes)")
plt.show()

# Delete outliers (Sessions with negative duration and sessions with very long duration)
total_number_sessions = len(data)
data = data[data.duration < pd.Timedelta(weeks=100)]
number_sessions_too_long = total_number_sessions - len(data)

number_sessions_without_long_sessions = len(data)
data = data[data.duration > pd.Timedelta(seconds=0)]
number_negative_sessions = number_sessions_without_long_sessions - len(data)

# Outlier statistics
print("Total number of sessions: "+str(total_number_sessions))
print("Sessions too long: "+str(number_sessions_too_long)+" ("+str(number_sessions_too_long*100/total_number_sessions)+")%")
print("Sessions with negative duration: "+str(number_negative_sessions)+" ("+str(number_negative_sessions*100/total_number_sessions)+"%)")
print("Actual number of sessions: "+str(len(data))+" ("+str(len(data)*100/total_number_sessions)+"%)")

# Histogram of durations and duration statistics
print(data["duration"].describe())

plot_data = data
plot_data["duration"] = plot_data["duration"].astype('timedelta64[m]')

plot_data.plot(x='start', y='duration')
plt.title("Sessions duration")
plt.xlabel("Timestamp")
plt.ylabel("Duration of session (minutes)")
plt.show()

data["duration"].astype('timedelta64[m]').plot()
plt.xlabel("Timestamp")
plt.title("Session duration")
plt.ylabel("Duration of session (minutes)")
plt.show()


# Count the number of sessions that have started and ended at each timestamp
start_count = data["start"].value_counts()
end_count = data["end"].value_counts()
data2 = pd.concat([start_count, end_count], axis=1, keys=["start", "end"])
data2.fillna(0, inplace=True)

# Count the number of sessions that at active at each hour
data2["diff"] = data2["start"] - data2["end"]
counts = data2["diff"].resample("1h").sum().fillna(0).cumsum()

counts.plot()
plt.title("Number of sessions/hour")
plt.xlabel("Timestamp")
plt.ylabel("Number of sessions/hour")
plt.show()

counts.loc['2018-09-15':].plot()
plt.title("Number of sessions/hour since 15-09")
plt.xlabel("Timestamp")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2018-09-01':'2018-09-30'].plot()
plt.title("Number of sessions/hour in September")
plt.xlabel("Timestamp")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2018-10-01':'2018-10-31'].plot()
plt.title("Number of sessions/hour in October")
plt.xlabel("Timestamp")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2018-11-01':'2018-11-30'].plot()
plt.title("Number of sessions/hour in November")
plt.xlabel("Timestamp")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2018-12-01':'2018-12-31'].plot()
plt.title("Number of sessions/hour in December")
plt.xlabel("Timestamp")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2019-01-01':'2019-01-31'].plot()
plt.title("Number of sessions/hour in January")
plt.xlabel("Timestamp")
plt.ylabel("Number of sessions/hour")
plt.show()
counts.loc['2019-02-01':'2019-02-28'].plot()
plt.title("Number of sessions/hour in February")
plt.xlabel("Timestamp")
plt.ylabel("Number of sessions/hour")
plt.show()

counts.loc['2018-11-25':'2018-12-08'].plot()
plt.title("Test set", fontsize=30)
plt.xlabel("Timestamp", fontsize=30)
plt.ylabel("Number of sessions/hour", fontsize=30)
plt.ylim(0, 750)
plt.subplots_adjust(left=0.06, right=0.99, top=0.95, bottom=0.10)
plt.tick_params(labelsize = 20, which='both')
plt.show()

counts.to_csv('counts.csv', header=["values"])

