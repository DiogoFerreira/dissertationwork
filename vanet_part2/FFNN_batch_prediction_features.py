import glob
import holidays
import numpy as np
import pandas as pd
from math import sqrt
from numpy import array
import matplotlib.pyplot as plt
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input
from sklearn.metrics import mean_squared_error
from pandas import datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler, StandardScaler


# Read data file
def read_file(filename):
    return pd.read_csv(filename, sep=",", parse_dates=[0], index_col=0,
                       date_parser=lambda x: datetime.strptime(x, "%Y-%m-%d %H:%M:%S"))


# Get all series values
def get_series_values(df):
    return df.loc['2018-09-17':'2018-12-08']["values"]


# Differentiate the time-series in lag time-steps
def differentiate(series, lag=1):
    return Series([series[i]-series[i-lag] for i in range(lag, len(series))])


# Invert the differentiation
def invert_difference(series, original_series, lag=1):
    return np.asarray([original_series[i-lag] + series[i-lag] for i in range(lag, len(original_series))])


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# Parse time-series data interval for train, validation and test
def train_validation_test_split(values, offset=0, validation_hours=168, test_hours = 168):
    return values[offset:-test_hours-validation_hours], values[-test_hours-validation_hours:-test_hours], values[-test_hours:]


# make one forecast with a NN,
def forecast_nn(model, X, n_batch, n_lag):
    # reshape input pattern
    X = X.reshape(1, n_lag)
    # make forecast
    forecast = model.predict(X, batch_size=n_batch)
    # convert to array
    return [x for x in forecast[0, :]]


# evaluate the model
def make_nn_forecasts(model, n_batch, test, n_lag, n_seq):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_nn(model, X, n_batch, n_lag)
        # Uncomment the line below for testing purposes
        # forecast = y
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# Invert the scaler operation
def invert_scaler(forecasts, scaler):
    return scaler.inverse_transform(forecasts)


# Calculate the MAPE metric
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq, debug=True):
    rmse = sqrt(mean_squared_error(test.values, forecasts))
    try:
        mape = mean_absolute_percentage_error(test.values, np.resize(forecasts, (len(forecasts),)))
    except Exception as e:
        mape = 0.0
    if debug:
        print('t+%d RMSE: %f' % (1, rmse))
        print('t+%d MAPE: %f' % (1, mape))
    return rmse, mape


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test, title, xlabel, ylabel):
    # plot the entire dataset in blue
    plt.plot(series.values, label="Real values")
    plt.plot(range(len(series)-n_test, len(series)), forecasts, label="Predicted values t+1")
    # show the plot
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()


def get_model(i, shape):
    input_tensor = Input(shape=(shape,))
    if (i%10) == 0 or (i%10) == 3:
        output = Dense(200, activation='relu')(input_tensor)
        output = Dropout(0.2)(output)
        output = Dense(200, activation='relu')(output)
        output = Dropout(0.2)(output)
        output = Dense(200, activation='relu')(output)
        output = Dropout(0.2)(output)
        output = Dense(200, activation='relu')(output)
        output = Dropout(0.2)(output)
        output = Dense(y.shape[1])(output)
    elif (i%10) == 1 or (i%10) == 4:
        output = Dense(200, activation='relu')(input_tensor)
        output = Dense(200, activation='relu')(output)
        output = Dense(y.shape[1])(output)
    elif (i%10) == 2:
        output = Dense(200, activation='relu')(input_tensor)
        output = Dropout(0.2)(output)
        output = Dense(200, activation='relu')(output)
        output = Dropout(0.2)(output)
        output = Dense(200, activation='relu')(output)
        output = Dropout(0.2)(output)
        output = Dense(y.shape[1])(output)
    elif (i%10) == 5:
        output = Dense(200, activation='relu')(input_tensor)
        output = Dense(200, activation='relu')(output)
        output = Dense(200, activation='relu')(output)
        output = Dense(y.shape[1])(output)
    elif (i%10) == 6:
        output = Dense(20, activation='relu')(input_tensor)
        output = Dense(20, activation='relu')(output)
        output = Dense(20, activation='relu')(output)
        output = Dense(y.shape[1])(output)
    elif (i%10) == 7:
        output = Dense(20, activation='relu')(input_tensor)
        output = Dense(20, activation='relu')(output)
        output = Dense(y.shape[1])(output)
    elif (i%10) == 8:
        output = Dense(20, activation='relu')(input_tensor)
        output = Dropout(0.2)(output)
        output = Dense(20, activation='relu')(output)
        output = Dropout(0.2)(output)
        output = Dense(y.shape[1])(output)
    elif (i%10) == 9:
        output = Dense(20, activation='relu')(input_tensor)
        output = Dense(20, activation='relu')(output)
        output = Dense(20, activation='relu')(output)
        output = Dense(20, activation='relu')(output)
        output = Dense(y.shape[1])(output)        

    model = Model(inputs=input_tensor, outputs=output)
    return model


def get_first_diff(i):
    if (i%10) == 0 or (i%10) == 2 or (i%10) == 3 or (i%10) == 8:
        return True, 1
    else:
        return False, 0


def get_lag(i):
    if (i%10) == 0 or (i%10) == 2 or (i%10) == 4 or (i%10) == 6 or (i%10) == 7 or (i%10) == 9:
        return 12
    else:
        return 24


def get_features(i):
    if i<10:
        return True, False, False
    elif i<20:
        return False, True, False   
    elif i<30:
        return False, False, True
    elif i<40:
        return True, True, False
    elif i<50:
        return True, False, True
    elif i<60:
        return False, True, True
    else:
        return True, True, True


# Returns an array with one-hot encoded of week day
def date_to_week_day(date):
    week_day_encoded = [0]*7
    week_day_encoded[date.weekday()] = 1
    return week_day_encoded


# Returns True if it is work day, False otherwise
def date_to_work_day(date):
    pt_holidays = holidays.PT()
    return True if date.weekday() <= 4 and date not in pt_holidays else False


# Returns an array with one-hot encoded of the hour of the day
def date_to_hour(date):
    hour_encoded = [0]*24
    hour_encoded[date.hour] = 1
    return hour_encoded


# Parameters
file = "counts.csv"
differentiation = True
second_differentiation = False

normalize = True

# Lag to differentiate the time series
differentiation_lag = 24

# Lag for second differentiation of the time series
second_differentiation_lag = 1

# Number of points available to predict the next ones
n_lag = 12

# Number of next points to predict
n_seq = 1

# Number of hours to include in the validation set
validation_hours = 24*7*2

# Number of hours to include in the vest set
test_hours = 24*7*2

# Number of epochs to train the model
n_epochs = 500

# Batch size
batch_size = 1

model_number = 0

i = 0
lowest_error = 9999

while lowest_error>30:
    for model_number in range(2, 3):
        differentiation, differentiation_lag = get_first_diff(model_number)
        n_lag = get_lag(model_number)
        week_day_feature, work_day_feature, hour_feature = get_features(model_number)

        df = read_file(file)

        # Analysis of traffic and choose_series
        series_values = get_series_values(df)
        original_series_values = series_values

        if differentiation:
            series_values = differentiate(series_values, differentiation_lag)
            if second_differentiation:
                second_series_values = series_values
                series_values = differentiate(series_values, second_differentiation_lag)
                original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+second_differentiation_lag+n_lag, validation_hours, test_hours)
                week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
                work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
                hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag+second_differentiation_lag:]])
            else:
                original_train, original_validation, original_test = train_validation_test_split(original_series_values, differentiation_lag+n_lag, validation_hours, test_hours)
                week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
                work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag+differentiation_lag:]])
                hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag+differentiation_lag:]])
        else:
            original_train, original_validation, original_test = train_validation_test_split(original_series_values, n_lag, validation_hours, test_hours)
            week_day_array = np.array([date_to_week_day(index) for index in original_series_values.index[n_lag:]])
            work_day_array = np.array([[date_to_work_day(index)] for index in original_series_values.index[n_lag:]])
            hour_array = np.array([date_to_hour(index) for index in original_series_values.index[n_lag:]])

        if normalize:
            # rescale values
            scaler = StandardScaler()
            scaled_values = scaler.fit_transform(series_values.values.reshape(len(series_values.values), 1))
        else:
            scaler = None
            scaled_values = series_values.values


        scaled_values = scaled_values.reshape(len(scaled_values), 1)


        # transform into supervised learning problem X, y
        supervised = series_to_supervised(scaled_values, n_lag, n_seq)
        supervised_values = supervised.values

        number_inputs = n_lag
        if work_day_feature:
            supervised_values = np.c_ [supervised_values[:, :-n_seq], work_day_array, supervised_values[:, -n_seq:]]
            number_inputs += 1

        if week_day_feature:
            supervised_values = np.c_[supervised_values[:, :-n_seq], week_day_array, supervised_values[:, -n_seq:]]
            number_inputs += 7

        if hour_feature:
            supervised_values = np.c_ [supervised_values[:, :-n_seq], hour_array, supervised_values[:, -n_seq:]]
            number_inputs += 24

        train, validation, test = train_validation_test_split(supervised_values, 0, validation_hours, test_hours)


        # reshape training into [samples, timesteps, features]
        X, y = train[:, 0:number_inputs], train[:, number_inputs:]
        X = X.reshape(X.shape[0], X.shape[1])
        validation_X, validation_y = validation[:, 0:number_inputs], validation[:, number_inputs:]
        validation_X = validation_X.reshape(validation_X.shape[0], validation_X.shape[1])

        # design network
        model = get_model(model_number, X.shape[1])
        model.compile(loss='mean_squared_error', optimizer='adam')

        train_rmse, test_rmse = list(), list()
        train_mape, test_mape = list(), list()
        lowest_error = 9999
        best_model = None

        for i in range(n_epochs):
            print("Epoch "+str(i+1))

            history = model.fit(X, y, epochs=1, batch_size=batch_size, verbose=0, shuffle=False)
            forecasts = make_nn_forecasts(model, batch_size, train, number_inputs, n_seq)
            
            if normalize:
                forecasts = invert_scaler(forecasts, scaler)
            else:
                forecasts = np.asarray(forecasts)

            forecasts = np.resize(forecasts, (len(forecasts),))
            
            if second_differentiation and differentiation:
                forecasts = invert_difference(forecasts, second_series_values.values[n_lag:-len(test)-len(validation)], second_differentiation_lag)
                forecasts = invert_difference(forecasts, original_series_values.values[second_differentiation_lag+n_lag:-len(test)-len(validation)], differentiation_lag)
            elif differentiation:
                forecasts = invert_difference(forecasts, original_series_values.values[n_lag:-len(test)-len(validation)], differentiation_lag)
            
            
            forecasts[forecasts<0] = 0
            error, mape = evaluate_forecasts(original_train, forecasts, n_lag, n_seq, True)
            train_rmse.append(error)
            train_mape.append(mape)

            forecasts = make_nn_forecasts(model, batch_size, validation, number_inputs, n_seq)
            if normalize:
                forecasts = invert_scaler(forecasts, scaler)
            else:
                forecasts = np.asarray(forecasts)

            forecasts = np.resize(forecasts, (len(forecasts),))

            if second_differentiation and differentiation:
                forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
                forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
            elif differentiation:
                forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

            forecasts[forecasts<0] = 0
            error, mape = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)
            test_rmse.append(error)
            test_mape.append(mape)

            if error < lowest_error:
                lowest_error = error
                best_model = model
                best_model.save_weights("models/ffnn_features_tests/FFNNmodelWeights"+str(model_number)+"_"+str(i)+".h5")

        history = DataFrame()
        history['train'], history['test'] = train_rmse, test_rmse
        writer = open("ffnn_predictions/batch_tests_features/ffnn_predictions"+str(model_number)+"_"+str(i)+".csv", 'w')
        writer.write(history.to_string()+"\n")
        writer.close()

        history_mape = DataFrame()
        history_mape['train'], history_mape['test'] = train_mape, test_mape
        writer = open("ffnn_predictions/batch_tests_features/ffnn_predictions_mape_"+str(model_number)+"_"+str(i)+".csv", 'w')
        writer.write(history_mape.to_string()+"\n")
        writer.close()

        i += 1

        #plt.title("RMSE error")
        #plt.plot(train_rmse, "b", label="Training error")
        #plt.plot(test_rmse, "orange", label="Test error")
        #plt.legend()
        #plt.show()


        # forecasts = make_nn_forecasts(best_model, batch_size, validation, number_inputs, n_seq)

        # if normalize:
        #     forecasts = invert_scaler(forecasts, scaler)
        # else:
        #     forecasts = np.asarray(forecasts)

        # if second_differentiation and differentiation:
        #     forecasts = invert_difference(forecasts, second_series_values.values[-len(validation)-second_differentiation_lag-len(test):-len(test)], second_differentiation_lag)
        #     forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)
        # elif differentiation:
        #     forecasts = invert_difference(forecasts, original_series_values.values[-len(validation)-differentiation_lag-len(test):-len(test)], differentiation_lag)

        # error = evaluate_forecasts(original_validation, forecasts, n_lag, n_seq, True)

        #plot_forecasts(original_series_values[:-len(test)], forecasts, len(validation), "Feed-Forward NN model prediction", "Sample number", "Traffic volume")
        #best_model.save_weights("models/batch_tests_features/FFNNmodelWeights"+str(model_number)+".h5")


